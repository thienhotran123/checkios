import React from 'react';
import {Storage} from './src/Redux/Storage';
import {Provider} from 'react-redux';
import DefaultLayoutScreen from './src/DefaultLayout/DefaultLayoutScreen';
const App = () => {
  return (
    <Provider store={Storage}>
      <DefaultLayoutScreen/>
    </Provider>
  );
};
export default App;
