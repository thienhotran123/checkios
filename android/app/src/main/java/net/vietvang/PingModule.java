package net.vietvang;

import com.facebook.react.bridge.NativeModule;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import java.util.Map;
import java.util.HashMap;
import java.net.DatagramSocket;
import com.facebook.react.bridge.Callback;
import java.net.InetAddress;
import java.io.IOException;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.net.Socket;
import java.net.InetSocketAddress;
public class PingModule extends ReactContextBaseJavaModule {
    public PingModule(ReactApplicationContext context){
        super(context);
    }
    @Override 
    public String getName()
    {
        return "PingModule";
    }
    @ReactMethod
    public void PingAddress(String valueIp,Callback callback) {
         int port = 9100;
         int timeout = 3000;
         final Executor executor = Executors.newSingleThreadExecutor();
         executor.execute(new Runnable() {
             @Override
             public void run() {
                 Socket socket = new Socket();
                 try {
                    socket.connect(new InetSocketAddress(valueIp, port), timeout);
                    socket.close();
                    callback.invoke("Success");
                 } catch (Exception e) {
                    callback.invoke("Failure");
                 }
             }
         });
    }
}