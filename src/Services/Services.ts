import {ProductItem} from '../Utils/interface/interface';

const TotalQuantity = (Array: ProductItem[]) => {
  return Array.reduce((init: number, cur: ProductItem) => {
    if(cur.qtyStatus !==undefined)
    {
      return (init += cur.quantity + cur.qtyStatus);
    }
    else{
      return (init += cur.quantity);
    }
  }, 0);
};
const TotalBeforeTax = (Array: ProductItem[]) => {
  return Array.reduce((init: number, cur: ProductItem) => {
    if(cur.qtyStatus !==undefined)
    {
      return (init += Math.floor(
        (cur.price / (1 + cur.vat / 100)) * (cur.quantity + cur.qtyStatus)
      ));
    }
    else{
      return (init += Math.floor(
        (cur.price / (1 + cur.vat / 100)) * (cur.quantity),
      ));
    }
  }, 0);
};
const TotalVat = (Array: ProductItem[]) => {
  return Array.reduce((init: number, cur: ProductItem) => {
    if(cur.qtyStatus !==undefined)
    {
      return (init += cur.price * (cur.vat / 100) * cur.quantity + cur.qtyStatus);
    }
    else{
      return (init += cur.price * (cur.vat / 100) * cur.quantity);
    }
  }, 0);
};
const TotalMoney = (Array: ProductItem[]) => {
  return Array.reduce((init: number, cur: ProductItem) => {
    if(cur.qtyStatus !==undefined)
    {
      return (init += cur.price * (cur.quantity +cur.qtyStatus));
    }
    else{
      return (init += cur.price * (cur.quantity));
    }
  }, 0);
};
const TotalMoneyVat = (Array: ProductItem[]) => {
  return Array.reduce((init: number, cur: ProductItem) => {
    if(cur.qtyStatus !==undefined)
    {
      return (
        (init += cur.price * (cur.quantity + cur.qtyStatus)) +  cur.price * (cur.vat / 100) * cur.quantity + cur.qtyStatus
      );
    }
    else{
      return (
        (init += cur.price * (cur.quantity)) + cur.price * (cur.vat / 100) * cur.quantity
      );
    }
  }, 0);
};
const TotalMoneyVatItemProduct = (price: number, vat: number) => {
  return price + (price * vat) / 100;
};
const TotalMoneyWithVat =(item:ProductItem)=>{
  if(item.qtyStatus !==undefined)
  {
    return item.price * (item.quantity + item.qtyStatus) + ((item.price * item.vat) / 100) * (item.quantity + item.qtyStatus)
  }
  else{
    return item.price * (item.quantity) + ((item.price * item.vat) / 100) * (item.quantity)
  }
}
const RoundOff = (ValueNumber: number | string) => {
  const numberConvert = Number(ValueNumber);
  return (
    numberConvert -
    (numberConvert % 1000) +
    Math.round((numberConvert % 1000) / 100) * 100
  )
    .toString()
    .replace(/\B(?=(\d{3})+(?!\d))/g, ',');
};
const CommaSeparatedValues = (ValueNumber: number | string) => {
  return ValueNumber.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
};
const CheckProductItemPrinted = (Array: ProductItem[]) => {
  return Array.find(item => item.status == 2);
};
const ProductChecked = (Array: ProductItem[]) => {
  return Array.filter(item => item.check == 1);
};
const ProductNotChecked = (Array: ProductItem[]) => {
  return Array.filter(item => item.check == 0);
};
export {
  TotalQuantity,
  TotalBeforeTax,
  TotalVat,
  TotalMoney,
  TotalMoneyVat,
  TotalMoneyVatItemProduct,
  TotalMoneyWithVat,
  RoundOff,
  CommaSeparatedValues,
  CheckProductItemPrinted,
  ProductChecked,
  ProductNotChecked
};
