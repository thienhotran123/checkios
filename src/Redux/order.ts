import {PayloadAction, createSlice} from '@reduxjs/toolkit';
import { ValueInitialStateOrder } from '../Utils/interface/interface';
const initialState :ValueInitialStateOrder = {
  DataSetting: {PrintIp: '192.168.1.122'},
  showBtnActions: false,
  DataFooter: {
    content: '',
    image: null,
  },
  XmlProperties:null ,
  checkCallGetXml :true,
  ConnectNetwork:{show:false , connected:false}
};

export const OrderSlice = createSlice({
  name: 'item',
  initialState,
  reducers: {
    ResetState :()=>(initialState),
    SetDataSettings: (state, actions:PayloadAction<{PrintIp:string}>) => {
      state.DataSetting = actions.payload;
    },
    SetDataDataFooter: (state, actions:PayloadAction<{content:string ,image:string | null }>) => {
      state.DataFooter = actions.payload;
    },
    SetXmlProperties :(state ,actions:PayloadAction<{isCheck :boolean , data:any}>)=>{
      state.XmlProperties = actions.payload.data;
      state.checkCallGetXml=actions.payload.isCheck
    },
    SetConnectNetwork : (state,actions:PayloadAction<{show:boolean , connected:boolean}> )=>{
      state.ConnectNetwork=actions.payload
    }
  },
});
export const {
  SetConnectNetwork,
  ResetState,
  SetXmlProperties,
  SetDataDataFooter,
  SetDataSettings,
} = OrderSlice.actions;

export default OrderSlice.reducer;
