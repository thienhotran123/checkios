import {createSlice} from '@reduxjs/toolkit';
import {CategoryItem, ProductItem,Table,TableItem,ValueInitialState,userName} from '../Utils/interface/interface';
import {PayloadAction} from '@reduxjs/toolkit';
const initialState: ValueInitialState = {
  showTable: false,
  ShowTableDetail: false,
  ShowCreateModal : false,
  isNotification: false,
  ShowDetailPaymentHistory: false,
  ShowMOdalListTable: false,
  isLoading: false,
  renderTable: true,
  ShowOrderTamDetail: false,
  indexItem: -1,
  NameModal: '',
  ModalPosition: '',
  dataDeleteItem: {indexDelete: -1, indexParent: -1},
  UserName: {},
  Table: [],
  productItem: [],
  CategoryItem: [],
  TableItem: {listitem: []},
  DataPayment: {item: [] , CheckValue : '' , DiscountPayment : 0 , Reason:'' , valueNumber:{phone:'' , name:'', point:"", valid:false}},
  DetailItemPaymentOld: { tableName:'',valuetotal:0, item: [] , discountPayment:0},
  initialValueModal : { description : '' , showValue : false , functionConfirm : '', functionCancel :'' , valueBtnConfirm :'' , valueBtnCancel :''}
};
export const counterSlice = createSlice({
  name: 'counter',
  initialState,
  reducers: {
    ResetState :()=>(initialState),
    SetShowModalCreateCustomer :(state,actions:PayloadAction<boolean>)=>{
      state.ShowCreateModal = actions.payload
    },
    SetDataItemBill: (state,actions: PayloadAction<{ name: string; id: number | string;data: ProductItem[]; }>) => {
      switch (actions.payload.name) {
        case 'TableBill':
          const indexItemTable = state.Table.findIndex(item => item.id == actions.payload.id);
          let NewArray = [...state.TableItem.listitem, ...actions.payload.data];
          let Array: any[] = [];
          for (let i = 0; i < NewArray.length; i++) {
            const indexArrayItem = Array.findIndex(
              item => item.id == NewArray[i].id && item.status !== 2,
            );
            if (indexArrayItem !== -1) {
              Array[indexArrayItem].quantity += NewArray[i].quantity;
            } else {
              Array.push(NewArray[i]);
            }
          }
          state.TableItem.listitem = Array;
          if (indexItemTable !== -1) {
            state.Table[indexItemTable].listitem = Array;
          }
          return;
      }
    },
    SetModalPosition: (state, actions: PayloadAction<string>) => {
      state.ModalPosition = actions.payload;
    },
    SetChooseAllData: (state, actions: PayloadAction<{ChooseScreen: string}> ) => {
      if (actions.payload.ChooseScreen == 'Table') {
        const EveryAllTrue = state.TableItem.listitem.every(item => item.check == 1);
        state.TableItem.listitem = state.TableItem.listitem.map(item => {
          return {
            vat: item.vat,
            category_id: item.category_id,
            check: EveryAllTrue ? 0 : 1,
            id: item.id,
            image: item.image,
            price: item.price,
            qtyStatus: item.qtyStatus,
            quantity: item.quantity,
            status: item.status,
            title: item.title,
          };
        });
        return;
      }
    },
    SetDataTableCheckIitem: (state,actions: PayloadAction<{  ItemProductTable: Table; IndexProductTable: number }>) => {
      const IndexTableChangeCheck = state.Table.findIndex(item => item.id == state.TableItem.id);
      if (IndexTableChangeCheck !== -1) {
        if (
          state.Table[IndexTableChangeCheck].listitem[ actions.payload.IndexProductTable] !== undefined &&
          state.TableItem.listitem[actions.payload.IndexProductTable] !== undefined) {
          state.TableItem.listitem[actions.payload.IndexProductTable].check == 1? (state.TableItem.listitem[actions.payload.IndexProductTable].check = 0)
            : (state.TableItem.listitem[actions.payload.IndexProductTable].check = 1);
          state.Table[IndexTableChangeCheck].listitem[actions.payload.IndexProductTable].check == 1
            ? (state.Table[IndexTableChangeCheck].listitem[actions.payload.IndexProductTable].check = 0)
            : (state.Table[IndexTableChangeCheck].listitem[actions.payload.IndexProductTable].check = 1);
        }
      } else {
        (state.ShowTableDetail = false), (state.ModalPosition = '');
        state.TableItem = {listitem: []};
      }
    },
    SetUserName: (state, actions: PayloadAction<userName>) => {
      state.UserName = actions.payload;
    },
    SetShowMOdalListTable: ( state,actions: PayloadAction<{show: boolean; NameModal: string}>) => {
      state.ShowMOdalListTable = actions.payload.show;
      state.NameModal = actions.payload.NameModal;
      state.isNotification = false;
    },
    SetClearDataProductTable: (state) => {
      const IndexTable = state.Table.findIndex(item => item.id == state.TableItem.id,);
      const itemList = state.TableItem.listitem.filter(item => item.check !== 1);
      state.TableItem.listitem = itemList;
      if (IndexTable !== -1) {
        state.Table[IndexTable].listitem = itemList;
      }
    },
    SetUpdateTableCancel: (state,actions: PayloadAction<{NotCheck: ProductItem[]}>) => {
      const IndexTable = state.Table.findIndex(item => item.id == state.TableItem.id,);
      state.TableItem.listitem = actions.payload.NotCheck;
      if (IndexTable !== -1) {
        state.Table[IndexTable].listitem = actions.payload.NotCheck;
      }
    },
    SetTableCancel: state => {
      const TableIndex = state.Table.findIndex(item => item.id == state.TableItem.id);
      if (TableIndex !== -1) {
        state.Table[TableIndex].listitem = [];
        state.Table[TableIndex].status = 1;
      }
      state.ShowTableDetail = false;
      state.TableItem = {listitem: []};
    },
    SetReloadTable: state => {
      state.renderTable = !state.renderTable;
    },
    SetStatusTable: (state, action: PayloadAction<{index: number; status: number}> ) => {
      if (state.Table[action.payload.index] !== undefined) {
        state.Table[action.payload.index].status = action.payload.status;
      }
    },
    setProductAndCategory: (state, actions: PayloadAction<{   dataCategory: CategoryItem[]; dataProductItem: ProductItem[]; }>,) => {
      const dataCategory = actions.payload.dataCategory.filter(item => item.status !== -1);
      state.CategoryItem = dataCategory;
      state.productItem = actions.payload.dataProductItem;
    },
    SetTable: (state, actions: PayloadAction<Table[]>) => {
      state.Table = actions.payload;
      if (state.TableItem.id !== undefined) {
        const indexTable = state.Table.findIndex(item => item.id == state.TableItem.id);
        if (indexTable !== -1) {
          state.Table[indexTable].listitem = state.TableItem.listitem;
        }
      }
    },
    SetShowLoading: (state, actions: PayloadAction<boolean>) => {
      state.isLoading = actions.payload;
    },
    SetShowTableDetail: (state,actions: PayloadAction<{  dataDetailTable: TableItem;   dataListProductTable: ProductItem[];}>) => {
      state.ShowTableDetail = !state.ShowTableDetail;
      state.TableItem = actions.payload.dataDetailTable;
      const indexTable = state.Table.findIndex(item => item.id == actions.payload.dataDetailTable.id,);
      if (indexTable !== -1) {
        state.Table[indexTable].listitem = actions.payload.dataListProductTable;
        state.Table[indexTable].status = actions.payload.dataDetailTable.status;
      }
    },
    SetDataPaymentForModal: (state, actions:PayloadAction<{valueNumber:{name: string, phone: string, point: string, valid: boolean}, CheckValue:string , item:ProductItem[] , DiscountPayment : number , Reason : string}>) => {
      state.DataPayment = {
        item :actions.payload.item,
        CheckValue:actions.payload.CheckValue ,
        DiscountPayment : actions.payload.DiscountPayment,
        Reason : actions.payload.Reason ,
        valueNumber:actions.payload.valueNumber
      }
    },
    SetItemTableQty: (state,actions: PayloadAction<{value: string; index: number}> ) => {
      const index = actions.payload.index;
      const indexTable = state.Table.findIndex(item => item.tableName == state.TableItem.tableName);
      switch (actions.payload.value) {
        case 'increase':
          if (state.TableItem.listitem[index].status == 2) {
            state.TableItem.listitem[index].qtyStatus += 1;
            if (indexTable !== -1 && state.Table[indexTable] !== undefined) {
              state.Table[indexTable].listitem = state.TableItem.listitem;
            }
            return;
          }
          if (state.TableItem.listitem[index].status == 1) {
            state.TableItem.listitem[index].quantity += 1;
            if (indexTable !== -1 && state.Table[indexTable] !== undefined) {
              state.Table[indexTable].listitem = state.TableItem.listitem;
            }
          }
          return;
        case 'reduce':
          if (state.TableItem.listitem[index].status == 2) {
            if (state.TableItem.listitem[index].qtyStatus > 0) {
              state.TableItem.listitem[index].qtyStatus -= 1;
              if (indexTable !== -1 && state.Table[indexTable] !== undefined) {
                state.Table[indexTable].listitem = state.TableItem.listitem;
              }
            }
            return;
          }
          if (state.TableItem.listitem[index].status == 1) {
            if (state.TableItem.listitem[index].quantity == 1) {
              state.isNotification = true;
              state.dataDeleteItem = {indexDelete: index,indexParent: indexTable};
              return;
            }
            state.TableItem.listitem[index].quantity -= 1;
            if (indexTable !== -1 && state.Table[indexTable] !== undefined) {
              state.Table[indexTable].listitem = state.TableItem.listitem;
            }
            return;
          }
          return;
      }
    },
    SetUpdateTable: (state, actions) => {
      const TableIndex = state.Table.findIndex(item => item.id == actions.payload.idTable);
      if (TableIndex !== -1) {
        if (actions.payload.CheckValue == 'All') {
          state.TableItem = {listitem: []};
          state.Table[TableIndex].listitem = actions.payload.dataUpdate;
          state.Table[TableIndex].status = 1;
          state.ShowTableDetail = false;
          return;
        }
        if (actions.payload.CheckValue == 'NotAll') {
          state.Table[TableIndex].listitem = actions.payload.dataUpdate;
          state.TableItem.listitem = actions.payload.dataUpdate;
          return;
        }
      } else {
        state.Table = state.Table.filter(
          item => item.id !== actions.payload.idTable,
        );
        state.TableItem = {listitem: []};
        state.ShowTableDetail = false;
      }
    },
    SetDeleteItemListMenu: state => {
      if (state.TableItem.listitem[state.dataDeleteItem.indexDelete] !== undefined && state.dataDeleteItem.indexDelete !== -1) {
        state.TableItem.listitem.splice(state.dataDeleteItem.indexDelete, 1);
        if (
          state.dataDeleteItem.indexParent !== -1 &&
          state.Table[state.dataDeleteItem.indexParent] !== undefined &&
          state.Table[state.dataDeleteItem.indexParent].listitem[
            state.dataDeleteItem.indexDelete
          ] !== undefined
        ) {
          state.Table[state.dataDeleteItem.indexParent].listitem.splice(state.dataDeleteItem.indexDelete, 1);
        }
      }
      state.dataDeleteItem = {indexDelete: -1, indexParent: -1};
    },
    SetPrintTableDetail: (state, actions: PayloadAction<ProductItem[]>) => {
      const indexTableItem = state.Table.findIndex(item => item.tableName == state.TableItem.tableName);
      state.TableItem.listitem = actions.payload;
      if (indexTableItem !== -1) {
        state.Table[indexTableItem].listitem = actions.payload;
      }
    },
    SetShowDetailPaymentHistory: (state, actions:PayloadAction<{show:boolean , tableName: number|string, valuetotal: number,  item: ProductItem[] ,discountPayment : number}>) => {
      const payload = actions.payload;
      state.DetailItemPaymentOld = {
        tableName:payload.tableName,
        valuetotal:  payload.valuetotal,
        item:payload.item ,
        discountPayment:payload.discountPayment
      };
      state.ShowDetailPaymentHistory = actions.payload.show;
    },
    SetCloseDetailPaymentHistory: (state)=>{
      state.DetailItemPaymentOld={  tableName:'',valuetotal:0, item: [] , discountPayment:0},
      state.ShowDetailPaymentHistory = false;
    },
    SetUpdateTableChange :(state , actions :PayloadAction<{ChangeValue:string , ArrayTableStanDing:ProductItem[] ,ArrayTableMove:ProductItem[] ,IDTableMove:number | string ,IDTableStanDing:number|string }>)=>{
      const indexTableStanding = state.Table.findIndex(item=>item.id == actions.payload.IDTableStanDing)
      const indexTableMove = state.Table.findIndex(item=> item.id == actions.payload.IDTableMove)
      if(indexTableStanding !==-1)
      {
        state.Table[indexTableStanding].listitem = actions.payload.ArrayTableStanDing
        if(actions.payload.ChangeValue == 'All')
        {
          state.Table[indexTableStanding].status = 1
        }
      }
      if(indexTableMove !==-1)
      {
        state.Table[indexTableMove].listitem = actions.payload.ArrayTableMove
      }
      if(actions.payload.ChangeValue == 'All')
      {
        state.TableItem = {listitem:[]}
        state.ShowTableDetail = false
      }
      else{
        state.TableItem.listitem = actions.payload.ArrayTableStanDing
      }
    },
    SetRemoveTableNotExist :(state,actions:PayloadAction<number|string | undefined>)=>{
      state.Table  = state.Table.filter(item=>item.id  !== actions.payload)
      if(state.TableItem.id ==  actions.payload)
      {
        state.ShowTableDetail = false
        state.TableItem = {listitem: []}
      }
    },
    SetShowModalIsNotification : (state,actions:PayloadAction<{ description : string , showValue : boolean , functionConfirm : string ,functionCancel :string ,valueBtnConfirm:string , valueBtnCancel : string}>)=>{
      state.initialValueModal = actions.payload
      state.isNotification = actions.payload.showValue
    },
    SetCloseModalIsNotification : (state)=>{
      state.initialValueModal = {description : '' , showValue : false , functionConfirm : '' ,functionCancel :'' ,valueBtnConfirm:'' , valueBtnCancel : ''}
      state.isNotification =false
    },
    SetCloseDetailTable : (state)=>{
      const indexTable = state.Table.findIndex(item => item.id == state.TableItem.id)
      if(indexTable!==-1)
      {
        state.Table[indexTable].status =1
      }
      state.ShowTableDetail = false
      state.TableItem ={listitem:[]};
    }
  },
});
export const {
  SetCloseDetailPaymentHistory,
  SetShowModalCreateCustomer,
  ResetState,
  SetCloseDetailTable,
  SetCloseModalIsNotification,
  SetShowModalIsNotification,
  SetRemoveTableNotExist,
  SetUpdateTableChange,
  SetUpdateTable,
  SetTableCancel,
  SetUpdateTableCancel,
  SetDataItemBill,
  SetModalPosition,
  SetChooseAllData,
  SetDataTableCheckIitem,
  SetUserName,
  SetShowMOdalListTable,
  SetClearDataProductTable,
  SetReloadTable,
  SetShowLoading,
  SetShowTableDetail,
  SetDataPaymentForModal,
  SetItemTableQty,
  SetDeleteItemListMenu,
  SetPrintTableDetail,
  SetTable,
  SetShowDetailPaymentHistory,
  setProductAndCategory,
  SetStatusTable,
} = counterSlice.actions;

export default counterSlice.reducer;
