import { configureStore } from '@reduxjs/toolkit';
import counterReducer from './Slide';
import OrderReducer from './order';

export const Storage = configureStore({
  reducer: {
    counter: counterReducer,
    item: OrderReducer,
  },
});
export type RootState=ReturnType<typeof Storage.getState>
export type AppDisPatch=typeof Storage.dispatch
