import {StyleSheet} from 'react-native';
import {verticalScale} from '../../../Utils/Reponsive';
import {colors, widthScreen} from '../../../Utils/Styles';
export const ProductScreenCss = StyleSheet.create({
  container: {
    flex: 1,
  },
  headerContainer: {
    backgroundColor:'#fff',
    paddingVertical:8,
    paddingHorizontal:10
  },
  headerContainerSearch: {
    width: '100%',
    display: 'flex',
    alignItems: 'center',
    flexDirection: 'row',
  },
  headerContainerBack: {
    marginRight: 18,
    display: 'flex',
    alignItems: 'center',
    flexDirection: 'row',
  },
  headerContainerSearchBtn: {
    flex: 1,
  },
  headerContainerCaterGory: {
    marginTop: 16,
  },
  SearchContainer: {
    justifyContent: 'flex-end',
    alignItems: 'center',
    display: 'flex',
    flexDirection: 'row',
    backgroundColor: '#DDDDDD',
    borderRadius: 20,
    height:verticalScale(54)
  },
  InputContainer: {
    flex: 1,
    backgroundColor: '#DDDDDD',
    borderWidth: 1,
    borderColor: '#DDDDDD',
    height:verticalScale(54),
    fontSize:widthScreen*0.04,
    paddingHorizontal: 16,
    color:'#666666',
    borderRadius: 20,
    width: '100%',
  },
  ScrollVerticalBtn: {
    backgroundColor: '#fff',
    paddingVertical: 12,
    paddingHorizontal: 16,
    borderRadius: 20,
    marginRight: 10,
  },
  ScrollVerticalText: {
    color: '#333',
    fontWeight: '700',
    fontSize: widthScreen * 0.036,
  },
  ScrollVerticalBtnValid: {
    backgroundColor: '#0066CC',
    paddingVertical: 12,
    paddingHorizontal: 16,
    borderRadius: 20,
    marginRight: 10,
  },
  ScrollVerticalTextValid: {
    color: '#fff',
    fontWeight: '700',
    fontSize: widthScreen * 0.036,
  },
  BodyContainer: {
    flex: 1,
    marginTop: 20,
    backgroundColor:'#F5F6F8'
  },
  BodyContainerScrollView: {
    flex: 1,
  },
  footer: {
    padding: 10,
    width: '100%',
    display: 'flex',
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  footerBtn: {
    backgroundColor: '#0066CC',
    width: '46%',
    height: verticalScale(50),
    borderRadius: 6,
    justifyContent: 'center',
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
  },
  footerText: {
    color: '#fff',
    fontWeight: '700',
    marginRight: 10,
    fontSize: widthScreen * 0.04,
  },
  ScrollProductItemContainer: {
    marginBottom: 10,
    borderRadius: 8,
    width: '100%',
    height: verticalScale(90),
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
  },
  ScrollBodyImg: {
    alignItems: 'center',
    justifyContent: 'center',
    marginLeft: 10,
    width: '20%',
    height: '100%',
  },
  ItemText: {flex: 1, marginLeft: 10},
  ItemTextName: {
    color: colors.Textcolor,
    fontWeight: '700',
    fontSize: widthScreen * 0.048,
  },
  ItemTextPrice: {
    color: '#333',
    fontWeight: '700',
    fontSize: widthScreen * 0.032,
  },
  BtnItemFunc: {
    display: 'flex',
    alignItems: 'center',
    flexDirection: 'row',
    marginRight: 10,
  },
  BtnItemFuncText: {
    fontSize: widthScreen * 0.04,
    fontWeight: '700',
    marginRight: 10,
  },
  TotalItem:{
    color:'#fff',
    fontSize:widthScreen*0.04
  },
  Loadding:{
    position: 'absolute',
    left: 0,
    right: 0,
    bottom: 0,
    top: 0,
    backgroundColor: 'rgba(0,0,0,0.6)',
  }
});
