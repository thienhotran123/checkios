import {
  Text,
  View,
  SafeAreaView,
  TouchableOpacity,
  Keyboard,
  Alert,
  TextInput,
  ScrollView,
} from 'react-native';
import React, {useCallback, useState, useMemo} from 'react';
import {ProductScreenCss} from './ProductScreenCss';
import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import {widthScreen} from '../../../Utils/Styles';
import {useDispatch, useSelector} from 'react-redux';
import {
  AuthenticationError,
  ChangeString,
  xmlChangeText,
} from '../../../Utils/Helper';
import {
  SetCloseDetailTable,
  SetCloseModalIsNotification,
  SetDataItemBill,
  SetModalPosition,
  SetRemoveTableNotExist,
  SetShowLoading,
  SetShowModalIsNotification,
} from '../../../Redux/Slide';
import {CheckOutTable} from '../../../Utils/GetApi';
import {RootState} from '../../../Redux/Storage';
import {ResponsiveMessage} from '../../../Utils/MessageApi';
import {
  LoadingComponent,
  ModalNotification,
  ItemProduct,
} from '../../../Components/Components';
import {AuthContext} from '../../../DefaultLayout/DefaultLayoutScreen';
import {Success} from '../../../Utils/Enums';
import {
  LoginAgain,
  TableStandingNotExist,
  errorNotification,
} from '../../../Utils/MessageNotification';
interface itemValue {
  id: number;
  price: number;
  check: number;
  vat: number;
  quantity: number;
  image: string | null;
  title: string;
  category_id: number;
  status: number;
  qtyStatus: number;
}
const ProductScreen = ({navigation, route}) => {
  const {signOut} = React.useContext(AuthContext);
  const {nameOrder, TableID, indexTable, tableName, TakeOutBillID} =
    route.params;
  const dispatch = useDispatch();
  const ProductItem = useSelector(
    (state: RootState) => state.counter.productItem,
  );
  const XmlProperties = useSelector(
    (state: RootState) => state.item.XmlProperties,
  );
  const ProductXml = XmlProperties !== null ? XmlProperties[3] : null;
  let DataProduct = JSON.parse(JSON.stringify(ProductItem));
  const UserName = useSelector((state: RootState) => state.counter.UserName);
  const CategoryItem = useSelector(
    (state: RootState) => state.counter.CategoryItem,
  );
  const [selectCategory, setSelectCategory] = useState<{
    value: string;
    catergoryID: number | null;
  }>({value: 'Tất Cả', catergoryID: 0});
  const TableItem = useSelector((state: RootState) => state.counter.TableItem);
  const isNotification = useSelector(
    (state: RootState) => state.counter.isNotification,
  );
  const isLoading = useSelector((state: RootState) => state.counter.isLoading);
  const [Search, setSearch] = useState<string>('');
  const [products, setProducts] = useState([...DataProduct]);
  const handleIncrease = useCallback((index: number, itemValue: itemValue) => {
    const dataItem = [...products];
    const indexProduct = dataItem.findIndex(item => item.id == itemValue.id);
    dataItem[indexProduct] = {
      ...dataItem[indexProduct],
      quantity: (dataItem[indexProduct].quantity += 1),
    };
    setProducts(dataItem);
  }, []);
  const handleReduceItem = useCallback(
    (index: number, itemValue: itemValue) => {
      const dataItem = [...products];
      const indexProduct = dataItem.findIndex(item => item.id == itemValue.id);
      dataItem[indexProduct] = {
        ...dataItem[indexProduct],
        quantity: (dataItem[indexProduct].quantity -= 1),
      };
      setProducts(dataItem);
    },
    [],
  );
  const handleSearch = (value: string) => {
    setSearch(value);
  };
  const handleAddItemOrder = async () => {
    const dataNewArray = products.filter(item => item.quantity > 0);
    if (nameOrder == 'TableBill') {
      dispatch(
        SetDataItemBill({id: TableID, name: nameOrder, data: dataNewArray}),
      );
      dispatch(SetModalPosition('TableDetail'));
      navigation.goBack();
      return;
    }
  };
  const handleSetSelectCategory = (value: {
    name: string;
    id: number | null;
  }) => {
    setSelectCategory({value: value.name, catergoryID: value.id});
  };
  const handleClearSearch = () => {
    setSearch('');
    Keyboard.dismiss();
  };
  const handleRefresh = useCallback(() => {
    const dataItem = [...products];
    for (let i = 0; i < dataItem.length; i++) {
      if (dataItem[i].quantity > 0) {
        dataItem[i] = {...dataItem[i], quantity: (dataItem[i].quantity = 0)};
      }
    }
    setProducts(dataItem);
  }, []);
  const TotalProduct = useMemo(() => {
    return products.reduce((init: number, cur) => {
      return (init += cur.quantity);
    }, 0);
  }, [products]);
  const handleDeleteQtyItem = useCallback(
    (index: number, itemValue: itemValue) => {
      if (itemValue.quantity > 0) {
        const dataItem = [...products];
        const indexProduct = dataItem.findIndex(
          item => item.id == itemValue.id,
        );
        Alert.alert('Delta Pos', 'Bạn muốn làm mới món được chọn?', [
          {
            text: 'Đồng ý',
            onPress: () => {
              dataItem[indexProduct] = {
                ...dataItem[indexProduct],
                quantity: (dataItem[indexProduct].quantity = 0),
              };
              setProducts(dataItem);
            },
          },
          {
            text: 'Huỷ',
            style: 'cancel',
          },
        ]);
      }
    },
    [],
  );
  const handleGoBack = () => {
    if (nameOrder == 'TableBill') {
      if (TableItem.listitem.length <= 0) {
        dispatch(SetCloseModalIsNotification());
        dispatch(SetShowLoading(true));
        CheckOutTable({
          token: UserName.token,
          data: {
            id: TableItem.id,
            user_id: UserName.id,
          },
        })
          .then(data => {
            if (AuthenticationError(data.message, data.status_code)) {
              signOut(LoginAgain);
              return;
            }
            if (data.message == ResponsiveMessage.TableDontExist) {
              navigation.goBack();
              dispatch(SetModalPosition(''));
              dispatch(SetRemoveTableNotExist(TableItem.id));
              dispatch(
                SetShowModalIsNotification({
                  description: TableStandingNotExist,
                  showValue: true,
                  functionConfirm: '',
                  functionCancel: 'handleCloseModalNotification',
                  valueBtnConfirm: '',
                  valueBtnCancel: '',
                }),
              );
              return;
            }
            if (data.status_code == Success) {
              navigation.goBack();
              dispatch(SetModalPosition(''));
              dispatch(SetCloseDetailTable());
              dispatch(SetCloseModalIsNotification());
            } else {
              dispatch(
                SetShowModalIsNotification({
                  description: data.message,
                  showValue: true,
                  functionConfirm: '',
                  functionCancel: 'handleCloseModalNotification',
                  valueBtnConfirm: '',
                  valueBtnCancel: '',
                }),
              );
            }
          })
          .catch(e => {
            dispatch(
              SetShowModalIsNotification({
                description: errorNotification,
                showValue: true,
                functionConfirm: '',
                functionCancel: 'handleCloseModalNotification',
                valueBtnConfirm: '',
                valueBtnCancel: '',
              }),
            );
          })
          .finally(() => dispatch(SetShowLoading(false)));
      } else {
        navigation.goBack();
      }
      return;
    } else {
      navigation.goBack();
    }
  };
  return (
    <SafeAreaView style={{flex: 1, backgroundColor: '#fff'}}>
      <View style={ProductScreenCss.container}>
        <View style={ProductScreenCss.headerContainer}>
          <View style={ProductScreenCss.headerContainerSearch}>
            <View style={ProductScreenCss.headerContainerBack}>
              <TouchableOpacity onPress={handleGoBack}>
                <Ionicons
                  name={xmlChangeText(
                    ProductXml,
                    'IconNavigation',
                    'chevron-back-outline',
                  )}
                  size={widthScreen * 0.1}
                  color={xmlChangeText(
                    ProductXml,
                    'IconNavigationColor',
                    '#0066CC',
                  )}
                />
              </TouchableOpacity>
            </View>
            <View style={ProductScreenCss.headerContainerSearchBtn}>
              <View style={ProductScreenCss.SearchContainer}>
                <TextInput
                  style={[
                    ProductScreenCss.InputContainer,
                    {
                      backgroundColor: xmlChangeText(
                        ProductXml,
                        'BackgroundSearchInput',
                        '#DDDDDD',
                      ),
                    },
                  ]}
                  placeholder="Tìm kiếm sản phẩm"
                  value={Search}
                  onChangeText={value => handleSearch(value)}
                  placeholderTextColor="#888888"
                />
                <TouchableOpacity
                  style={{marginRight: 16}}
                  onPress={handleClearSearch}>
                  <Ionicons
                    name="close"
                    size={widthScreen * 0.06}
                    color="#333"
                  />
                </TouchableOpacity>
              </View>
            </View>
          </View>
          <View>
            <ScrollView
              showsHorizontalScrollIndicator={false}
              showsVerticalScrollIndicator={false}
              style={ProductScreenCss.headerContainerCaterGory}
              horizontal>
              <TouchableOpacity
                onPress={() =>
                  handleSetSelectCategory({name: 'Tất Cả', id: null})
                }
                style={
                  selectCategory.value == 'Tất Cả'
                    ? ProductScreenCss.ScrollVerticalBtnValid
                    : ProductScreenCss.ScrollVerticalBtn
                }>
                <Text
                  style={
                    selectCategory.value == 'Tất Cả'
                      ? ProductScreenCss.ScrollVerticalTextValid
                      : ProductScreenCss.ScrollVerticalText
                  }>
                  Tất cả
                </Text>
              </TouchableOpacity>
              {CategoryItem.map((item, index) => {
                return (
                  <TouchableOpacity
                    onPress={() =>
                      handleSetSelectCategory({
                        name: item.category_name,
                        id: item.id,
                      })
                    }
                    style={
                      selectCategory.value == item.category_name
                        ? ProductScreenCss.ScrollVerticalBtnValid
                        : ProductScreenCss.ScrollVerticalBtn
                    }
                    key={index}>
                    <Text
                      style={
                        selectCategory.value == item.category_name
                          ? ProductScreenCss.ScrollVerticalTextValid
                          : ProductScreenCss.ScrollVerticalText
                      }>
                      {item.category_name}
                    </Text>
                  </TouchableOpacity>
                );
              })}
            </ScrollView>
          </View>
        </View>
        <View style={ProductScreenCss.BodyContainer}>
          <Text
            numberOfLines={1}
            style={{
              fontSize: widthScreen * 0.04,
              color: '#333',
              fontWeight: '700',
              padding: 10,
            }}>
            {nameOrder == 'TableBill'
              ? `Bàn ${tableName}`
              : TakeOutBillID !== undefined
              ? TakeOutBillID
              : 'Đang tạo hoá đơn mang về'}
          </Text>
          <ScrollView
            style={ProductScreenCss.BodyContainerScrollView}
            showsHorizontalScrollIndicator={false}
            showsVerticalScrollIndicator={false}>
            {Search == ''
              ? products !== null
                ? products.map((item, index: number) => {
                    if (selectCategory.value == 'Tất Cả') {
                      return (
                        <ItemProduct
                          item={item}
                          index={index}
                          key={index}
                          onIncrease={handleIncrease}
                          onReduceItem={handleReduceItem}
                          onDeleteQtyItem={handleDeleteQtyItem}
                        />
                      );
                    } else if (item.category_id == selectCategory.catergoryID) {
                      return (
                        <ItemProduct
                          item={item}
                          index={index}
                          key={index}
                          onIncrease={handleIncrease}
                          onReduceItem={handleReduceItem}
                          onDeleteQtyItem={handleDeleteQtyItem}
                        />
                      );
                    }
                  })
                : ''
              : ChangeString(Search, products !== null ? products : []).map(
                  (item, index: number) => {
                    return (
                      <ItemProduct
                        item={item}
                        index={index}
                        key={index}
                        onReduceItem={handleReduceItem}
                        onIncrease={handleIncrease}
                        onDeleteQtyItem={handleDeleteQtyItem}
                      />
                    );
                  },
                )}
          </ScrollView>
        </View>
        {TotalProduct > 0 ? (
          <View style={ProductScreenCss.footer}>
            <TouchableOpacity
              style={ProductScreenCss.footerBtn}
              onPress={handleRefresh}>
              <Text style={ProductScreenCss.footerText}>Làm mới</Text>
              <MaterialIcons
                name="refresh"
                color="#fff"
                size={widthScreen * 0.07}
              />
            </TouchableOpacity>
            <TouchableOpacity
              onPress={handleAddItemOrder}
              style={ProductScreenCss.footerBtn}>
              <Text style={ProductScreenCss.footerText}>Thêm vào đơn</Text>
              <Text style={ProductScreenCss.TotalItem}>{TotalProduct}</Text>
            </TouchableOpacity>
          </View>
        ) : (
          ''
        )}
      </View>
      {isNotification ? <ModalNotification navigation={navigation} /> : ''}
      {isLoading ? <LoadingComponent /> : ''}
    </SafeAreaView>
  );
};

export default React.memo(ProductScreen);
