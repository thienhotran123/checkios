import {
  SafeAreaView,
  Text,
  TouchableOpacity,
  View,
  LogBox,
  TextInput,
  ScrollView,
  KeyboardAvoidingView,
  Platform,
} from 'react-native';
import React, {useEffect, useState} from 'react';
import {colors, widthScreen} from '../../../Utils/Styles';
import Ionicons from 'react-native-vector-icons/Ionicons';
import {PaymentScreenStyleCss} from './PaymentScreenStyle';
import {useDispatch, useSelector} from 'react-redux';
import {useMemo} from 'react';
import {GetAllCustomer} from '../../../Utils/GetApi';
import {
  SetDataPaymentForModal,
  SetShowModalCreateCustomer,
  SetShowModalIsNotification,
} from '../../../Redux/Slide';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import {RootState} from '../../../Redux/Storage';
import {
  AuthenticationError,
  RegexNotNumber,
  RegexReason,
  xmlChangeText,
} from '../../../Utils/Helper';
import {
  LoadingComponent,
  CustomerModal,
  ModalNotification,
  CreateCustomer,
} from '../../../Components/Components';
import {Success} from '../../../Utils/Enums';
import {LoginAgain, PaymentOrder} from '../../../Utils/MessageNotification';
import {AuthContext} from '../../../DefaultLayout/DefaultLayoutScreen';
import {
  RoundOff,
  TotalMoney,
  TotalQuantity,
  TotalVat,
} from '../../../Services/Services';
import {verticalScale} from '../../../Utils/Reponsive';
LogBox.ignoreLogs(['Warning: ...']);
LogBox.ignoreAllLogs();
const PaymentScreen = ({navigation, route}) => {
  let paramsData = route.params;
  const {signOut} = React.useContext(AuthContext);
  const UserName = useSelector((state: RootState) => state.counter.UserName);
  const [dataNumber, setDataNumber] = useState<{name: string; phone: string; point: string}[]>([]);
  const [modalDataNumber, setShowModalDataNumber] = useState<boolean>(false);
  const [showTextInput, setShowTextInput] = useState<boolean>(false);
  const [discount, setDiscount] = useState<number>(0);
  const isLoading = useSelector((state: RootState) => state.counter.isLoading);
  const TableItem = useSelector((state: RootState) => state.counter.TableItem);
  const XmlProperties = useSelector((state: RootState) => state.item.XmlProperties);
  const PaymentXml = XmlProperties !== null ? XmlProperties[4] : null;
  const [Reason , setReason] = useState<string>('');
  const isNotification = useSelector(
    (state: RootState) => state.counter.isNotification,
  );
  const ShowCreateModal = useSelector(
    (state: RootState) => state.counter.ShowCreateModal,
  );
  const [valueNumber, setValueNumber] = useState<{
    name: string;
    phone: string;
    point: string;
    valid: boolean;
  }>({name: '', phone: '', point: '', valid: false});
  const dispatch = useDispatch();
  useEffect(() => {
    const FetchApi = () => {
      GetAllCustomer({token: UserName.token})
        .then(data => {
          if (AuthenticationError(data.message, data.status_code)) {
            signOut(LoginAgain);
            return;
          }
          if (data.status_code !== Success) {
            setDataNumber([]);
          } else {
            const DataCustomer = data.customer.map(item => {
              return {
                name: item.name,
                phone: item.phone,
                point: item.point,
              };
            });
            setDataNumber(DataCustomer);
            setValueNumber({name: '', phone: '', point: '', valid: false});
          }
        })
        .catch(() => setDataNumber([]));
    };
    FetchApi();
  }, []);
  const DataFilterItemCustomer = useMemo(() => {
    return dataNumber.map((item, index) => {
      if (item.phone.includes(valueNumber.phone) && valueNumber.phone !== '') {
        return (
          <TouchableOpacity
            key={index}
            onPress={() => handleSetNumberPhone(item)}
            style={PaymentScreenStyleCss.btnCustomer}>
            <Text style={PaymentScreenStyleCss.btnCustomerPhone}>
              {item.phone}
            </Text>
            <Text
              style={PaymentScreenStyleCss.btnCustomerName}
              numberOfLines={1}>
              -{item.name}
            </Text>
          </TouchableOpacity>
        );
      } else if (valueNumber.phone == '') {
        return (
          <TouchableOpacity
            key={index}
            onPress={() => handleSetNumberPhone(item)}
            style={PaymentScreenStyleCss.btnCustomer}>
            <Text style={PaymentScreenStyleCss.btnCustomerPhone}>
              {item.phone}
            </Text>
            <Text
              style={PaymentScreenStyleCss.btnCustomerName}
              numberOfLines={1}>
              -{item.name}
            </Text>
          </TouchableOpacity>
        );
      }
    });
  }, [valueNumber]);
  const total = useMemo(() => {
    return TotalMoney(paramsData.item);
  }, []);
  const totalVat = useMemo(() => TotalVat(paramsData.item), []);
  const ValueTotal = useMemo(() => {
    return TotalMoney(paramsData.item) - discount + totalVat;
  }, [discount]);
  const totalQuantity = useMemo(() => {
    return TotalQuantity(paramsData.item);
  }, []);
  const handleDiscount = (value: string) => {
    const numberOnly = value.replace(RegexNotNumber, '');
    if (Number(numberOnly) > Number(total) + Number(totalVat)) {
      setDiscount(0);
      return;
    }
    setDiscount(Number(numberOnly));
  };
  const handleSetReason = (value: string) => {
    setReason(value);
  };
  const handelGoBack = () => {
    navigation.goBack();
  };
  const handleInDetailPayment = () => {
    dispatch(SetShowModalCreateCustomer(false));
    navigation.navigate('PrintDetailView', {
      tableName: TableItem.tableName,
      item: paramsData.item,
      nameDetail: 'Payment',
      discount: discount,
      NameCustomer: valueNumber.name,
      PhoneCustomer: valueNumber.phone,
      PointCustomer: valueNumber.point,
    });
  };
  const handlePayment = () => {
    dispatch(SetShowModalCreateCustomer(false));
    dispatch(
      SetDataPaymentForModal({
        ...paramsData,
        Reason: RegexReason.test(Reason) ? '0' : Reason,
        DiscountPayment: discount,
        valueNumber:valueNumber
      }),
    );
    dispatch(
      SetShowModalIsNotification({
        description: PaymentOrder,
        showValue: true,
        functionConfirm: 'handlePayment',
        functionCancel: 'handleCloseModalNotification',
        valueBtnConfirm: '',
        valueBtnCancel: '',
      }),
    );
  };
  const handleSetFilterNumberPhone = (value: string) => {
    if (value.length == 10) {
      const dataFind = dataNumber.find(item => item.phone === value);
      if (dataFind) {
        setValueNumber({
          name: dataFind.name,
          phone: dataFind.phone,
          point: dataFind.point,
          valid: true,
        });
        setShowModalDataNumber(false);
        return;
      } else {
        setValueNumber({name: '', phone: value, point: '', valid: false});
      }
    } else {
      setValueNumber({name: '', phone: value, point: '', valid: false});
    }
    setShowModalDataNumber(true);
  };
  const handleSetNumberPhone = (valueItem: {
    name: string;
    phone: string;
    point: string;
  }) => {
    setValueNumber({
      name: valueItem.name,
      phone: valueItem.phone,
      point: valueItem.point,
      valid: true,
    });
    setShowModalDataNumber(false);
  };
  const handelShowInputNumberPhone = () => {
    if (modalDataNumber) {
      setShowModalDataNumber(false);
    }
    setShowTextInput(!showTextInput);
  };
  const handleCreateCustomer = () => {
    setShowModalDataNumber(false);
    setShowTextInput(false);
    dispatch(SetShowModalCreateCustomer(true));
  };
  const handleRemoveInputCustomer = () => {
    setValueNumber({name: '', phone: '', point: '', valid: false});
    setShowModalDataNumber(false);
    setShowTextInput(false);
  };
  const handlePushItemCustomer = (data: {
    name: string;
    phone: string;
    point: string;
  }) => {
    dataNumber.push(data);
    setValueNumber({
      name: data.name,
      phone: data.phone,
      point: data.point,
      valid: true,
    });
    setShowTextInput(true);
  };
  return (
    <SafeAreaView style={PaymentScreenStyleCss.containerPayment}>
      <View style={PaymentScreenStyleCss.container}>
        <View style={PaymentScreenStyleCss.headerContainer}>
          <TouchableOpacity onPress={handelGoBack}>
            <Ionicons
              name={xmlChangeText(
                PaymentXml,
                'IconNavigation',
                'chevron-back-outline',
              )}
              size={widthScreen * 0.14}
              color={xmlChangeText(
                PaymentXml,
                'IconNavigationColor',
                '#0066CC',
              )}
            />
          </TouchableOpacity>
          <Text style={PaymentScreenStyleCss.paymentText}>Thanh Toán</Text>
        </View>
        <KeyboardAvoidingView
          style={{backgroundColor: '#F5F6F8', flex: 1}}
          behavior={Platform.OS == 'android' ? 'padding' : 'padding'}>
          <View style={{flex: 1, backgroundColor: '#F5F6F8'}}>
            <View
              style={
                Platform.OS == 'ios'
                  ? {zIndex: 2, paddingHorizontal: 10}
                  : {paddingHorizontal: 10}
              }>
              <View style={PaymentScreenStyleCss.containerBtnInput}>
                <TouchableOpacity
                  onPress={handelShowInputNumberPhone}
                  style={PaymentScreenStyleCss.containerBtnInputTouch}>
                  <Text style={PaymentScreenStyleCss.containerBtnInputText}>
                    Nhập số điện thoại khách hàng
                  </Text>
                  <MaterialIcons
                    size={widthScreen * 0.055}
                    name="arrow-drop-down"
                    style={{color: '#333', fontSize: widthScreen * 0.09}}
                  />
                </TouchableOpacity>
                <TouchableOpacity
                  onPress={
                    valueNumber.name !== '' &&
                    valueNumber.phone !== '' &&
                    valueNumber.valid
                      ? handleRemoveInputCustomer
                      : handleCreateCustomer
                  }
                  style={{marginLeft: 20}}>
                  {valueNumber.name !== '' &&
                  valueNumber.phone !== '' &&
                  valueNumber.valid ? (
                    <MaterialIcons
                      size={widthScreen * 0.055}
                      name="highlight-off"
                      style={{
                        color: colors.IconColor,
                        fontSize: widthScreen * 0.09,
                      }}
                    />
                  ) : (
                    <MaterialIcons
                      size={widthScreen * 0.055}
                      name="add-circle-outline"
                      style={{
                        color: colors.IconColor,
                        fontSize: widthScreen * 0.09,
                      }}
                    />
                  )}
                </TouchableOpacity>
              </View>
              {showTextInput ? (
                <View style={PaymentScreenStyleCss.containerInputCustomer}>
                  <View style={PaymentScreenStyleCss.CustomerInput}>
                    <TextInput
                      onFocus={() => setShowModalDataNumber(true)}
                      onChangeText={e => handleSetFilterNumberPhone(e)}
                      value={valueNumber.phone}
                      keyboardType="numeric"
                      style={PaymentScreenStyleCss.inputCustomer}
                    />
                    <Text
                      style={PaymentScreenStyleCss.nameCustomer}
                      numberOfLines={1}>
                      {valueNumber.name}
                    </Text>
                  </View>
                  {modalDataNumber &&
                  DataFilterItemCustomer.find(item => item !== undefined) ? (
                    <CustomerModal
                      DataFilterItemCustomer={DataFilterItemCustomer}
                    />
                  ) : (
                    ''
                  )}
                </View>
              ) : (
                ''
              )}
            </View>
            <ScrollView
              style={{flex: 1}}
              showsVerticalScrollIndicator={false}
              showsHorizontalScrollIndicator={false}>
              <View style={PaymentScreenStyleCss.ContainerHeaderColumn}>
                <Text style={PaymentScreenStyleCss.ColumnChildrenText}>
                  TỔNG HOÁ ĐƠN
                </Text>
                <Text style={PaymentScreenStyleCss.ColumnChildrenText}></Text>
              </View>
              <View style={PaymentScreenStyleCss.ContainerColumn}>
                <View style={PaymentScreenStyleCss.ContainerColumnChildren}>
                  <Text style={PaymentScreenStyleCss.ColumnChildrenText}>
                    Bàn
                  </Text>
                  <Text
                    style={PaymentScreenStyleCss.ColumnChildrenText}
                    numberOfLines={1}>
                    {TableItem.tableName}
                  </Text>
                </View>
                <View style={PaymentScreenStyleCss.ContainerColumnChildren}>
                  <Text style={PaymentScreenStyleCss.ColumnChildrenText}>
                    Tổng số lượng
                  </Text>
                  <Text
                    style={PaymentScreenStyleCss.ColumnChildrenText}
                    numberOfLines={1}>
                    {totalQuantity}
                  </Text>
                </View>
                <View style={PaymentScreenStyleCss.ContainerColumnChildren}>
                  <Text style={PaymentScreenStyleCss.ColumnChildrenText}>
                    Tổng hoá đơn
                  </Text>
                  <Text
                    style={PaymentScreenStyleCss.ColumnChildrenText}
                    numberOfLines={1}>
                    {RoundOff(total)} VND
                  </Text>
                </View>
                <View style={PaymentScreenStyleCss.ContainerColumnChildren}>
                  <Text style={PaymentScreenStyleCss.ColumnChildrenText}>
                    Tổng VAT
                  </Text>
                  <Text
                    style={PaymentScreenStyleCss.ColumnChildrenText}
                    numberOfLines={1}>
                    {RoundOff(totalVat)} VND
                  </Text>
                </View>
                <View style={PaymentScreenStyleCss.ContainerColumnChildren}>
                  <View
                    style={{
                      flexDirection: 'row',
                      alignItems: 'center',
                      marginRight: 10,
                    }}>
                    <Text
                      style={[
                        PaymentScreenStyleCss.ColumnChildrenText,
                        {marginRight: 10},
                      ]}>
                      Giảm giá
                    </Text>
                    <MaterialIcons name="edit" size={widthScreen * 0.05} />
                  </View>
                  <TextInput
                    style={PaymentScreenStyleCss.inputDiscount}
                    numberOfLines={1}
                    keyboardType="numeric"
                    placeholder="0 VND"
                    onChangeText={value => handleDiscount(value)}
                    value={`${discount}`}
                  />
                  <Text style={PaymentScreenStyleCss.ColumnChildrenText}>
                    VND
                  </Text>
                </View>
                <View
                  style={
                    RegexReason.test(Reason) && discount > 0
                      ? PaymentScreenStyleCss.ContainerColumnChildrenError
                      : PaymentScreenStyleCss.ContainerColumnChildren
                  }>
                  <View
                    style={{
                      flexDirection: 'row',
                      alignItems: 'center',
                      marginRight: 10,
                    }}>
                    <Text
                      style={[
                        PaymentScreenStyleCss.ColumnChildrenText,
                        {marginRight: 10},
                      ]}>
                      Ghi chú
                    </Text>
                    <MaterialIcons name="edit" size={widthScreen * 0.05} />
                  </View>
                  <TextInput
                    style={PaymentScreenStyleCss.inputReason}
                    numberOfLines={1}
                    placeholder="Nhập ghi chú"
                    onChangeText={value => handleSetReason(value)}
                  />
                </View>
              </View>
              <View style={PaymentScreenStyleCss.ContainerHeaderColumn}>
                <Text style={PaymentScreenStyleCss.ColumnChildrenText}>
                  TỔNG THANH TOÁN
                </Text>
                <Text style={PaymentScreenStyleCss.ColumnChildrenText}></Text>
              </View>
              <View style={{backgroundColor: '#fff', paddingHorizontal: 10}}>
                {valueNumber.name !== '' &&
                valueNumber.phone !== undefined &&
                valueNumber.valid ? (
                  <View style={PaymentScreenStyleCss.ContainerColumnChildren}>
                    <Text style={PaymentScreenStyleCss.ColumnChildrenText}>
                      Điểm
                    </Text>
                    <Text
                      style={PaymentScreenStyleCss.ColumnChildrenText}
                      numberOfLines={1}>
                      {valueNumber.point !== null &&
                      valueNumber.point !== undefined &&
                      valueNumber.valid
                        ? valueNumber.point
                        : '0'}
                    </Text>
                  </View>
                ) : (
                  ''
                )}
                <View style={PaymentScreenStyleCss.ContainerColumnChildren}>
                  <Text style={PaymentScreenStyleCss.ColumnChildrenText}>
                    Khách hàng phải trả
                  </Text>
                  <Text
                    style={PaymentScreenStyleCss.ColumnChildrenText}
                    numberOfLines={1}>
                    {RoundOff(ValueTotal)} VND
                  </Text>
                </View>
              </View>
            </ScrollView>
          </View>
        </KeyboardAvoidingView>
        <View style={PaymentScreenStyleCss.ContainerBtnFooter}>
          <TouchableOpacity
            onPress={handleInDetailPayment}
            style={[
              PaymentScreenStyleCss.BtnDetailOrder,
              {
                width: xmlChangeText(
                  PaymentXml,
                  'ButtonFooter.WidthBtn',
                  '46%',
                ),
                height: verticalScale(
                  xmlChangeText(PaymentXml, 'ButtonFooter.HeightBtn', 50),
                ),
              },
            ]}>
            <Text style={PaymentScreenStyleCss.DetailOrderText}>
              {xmlChangeText(
                PaymentXml,
                'ButtonFooter.TextBtnMove',
                'In chi tiết hoá đơn',
              )}
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={handlePayment}
            disabled={
              RegexReason.test(Reason) == true && discount > 0 ? true : false
            }
            style={[
              RegexReason.test(Reason) && discount > 0
                ? PaymentScreenStyleCss.BtnPaymentNot
                : PaymentScreenStyleCss.BtnPayment,
              {
                width: xmlChangeText(
                  PaymentXml,
                  'ButtonFooter.WidthBtn',
                  '46%',
                ),
                height: verticalScale(
                  xmlChangeText(PaymentXml, 'ButtonFooter.HeightBtn', 50),
                ),
              },
            ]}>
            <Text style={PaymentScreenStyleCss.TextPayment}>
              {xmlChangeText(
                PaymentXml,
                'ButtonFooter.TextBtnPayment',
                'Thanh toán hoá đơn',
              )}
            </Text>
          </TouchableOpacity>
        </View>
      </View>
      {isNotification ? (
        <ModalNotification
          navigation={navigation}
        />
      ) : (
        ''
      )}
      {isLoading ? <LoadingComponent /> : ''}
      {ShowCreateModal ? (
        <CreateCustomer handlePushItemCustomer={handlePushItemCustomer} />
      ) : (
        ''
      )}
    </SafeAreaView>
  );
};

export default React.memo(PaymentScreen);
