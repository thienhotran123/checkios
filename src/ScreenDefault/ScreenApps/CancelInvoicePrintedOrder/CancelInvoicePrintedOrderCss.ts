import {StyleSheet} from 'react-native';
import {widthScreen} from '../../../Utils/Styles';
export const CancelInvoicePrintedOrderCss = StyleSheet.create({
  HeaderContainer: {
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
  },
  HeaderText: {
    fontSize: widthScreen * 0.05,
    fontWeight: '900',
    color: '#333',
  },
  BodyContainer: {
    flex: 1,
    padding: 10,
  },
  BodyContainerTextConfirm: {
    fontSize: widthScreen * 0.05,
    fontWeight: '700',
    color: '#333',
  },
  TextInputCssContainer: {
    width: '100%',
    height: 60,
    borderRadius: 4,
    marginTop: 20,
  },
  TextInputCss: {
    width: '100%',
    height: 60,
    borderRadius: 4,
    fontSize: widthScreen * 0.04,
    color: '#666666',
    fontWeight: '600',
  },
  BtnConfirmContainer: {
    width: '100%',
    marginTop: 20,
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
    color: '#333',
    fontSize: widthScreen * 0.04,
  },
  BtnConfirmTouch: {
    marginRight: 12,
  },
  BtnConfirmText: {
    fontSize: widthScreen * 0.05,
    fontWeight: '700',
  },
  Loadding: {
    position: 'absolute',
    left: 0,
    right: 0,
    bottom: 0,
    top: 0,
    backgroundColor: 'rgba(0,0,0,0.6)',
  },
  ContainerProduct: {
    paddingHorizontal: 10,
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    backgroundColor: '#DDDDDD',
    width: '100%',
    height: 60,
    alignItems: 'center',
    borderRadius: 6,
    marginBottom: 10,
  },
  TitleProduct: {
    width: '30%',
    fontSize: widthScreen * 0.036,
    fontWeight: '700',
    color: '#666666',
  },
  PriceProduct: {
    width: '20%',
    fontSize: widthScreen * 0.036,
    fontWeight: '700',
    color: '#666666',
  },
  QuantityProduct: {
    width: '10%',
    fontSize: widthScreen * 0.036,
    fontWeight: '700',
    color: '#666666',
  },
  StatusProduct: {
    width: '20%',
    fontSize: widthScreen * 0.036,
    fontWeight: '700',
    color: '#666666',
  },
});
