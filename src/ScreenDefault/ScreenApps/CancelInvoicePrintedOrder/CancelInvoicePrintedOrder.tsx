import {
  SafeAreaView,
  ScrollView,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import React, {useState} from 'react';
import {CancelInvoicePrintedOrderCss} from './CancelInvoicePrintedOrderCss';
import Ionicons from 'react-native-vector-icons/Ionicons';
import {colors, widthScreen} from '../../../Utils/Styles';
import {UpdateTable} from '../../../Utils/GetApi';
import {TextInput} from 'react-native-paper';
import {useDispatch, useSelector} from 'react-redux';
import {RootState} from '../../../Redux/Storage';
import {
  SetCloseModalIsNotification,
  SetModalPosition,
  SetRemoveTableNotExist,
  SetShowModalIsNotification,
  SetTableCancel,
  SetUpdateTableCancel,
} from '../../../Redux/Slide';
import {AuthenticationError, RegexReason} from '../../../Utils/Helper';
import {LoadingComponent} from '../../../Components/Components';
import {ResponsiveMessage} from '../../../Utils/MessageApi';
import {CheckOutTable} from '../../../Utils/GetApi';
import {AuthContext} from '../../../DefaultLayout/DefaultLayoutScreen';
import {Success} from '../../../Utils/Enums';
import {
  CancelInvoiceSSuccess,
  LoginAgain,
  TableStandingNotExist,
  errorNotification,
} from '../../../Utils/MessageNotification';
import { CommaSeparatedValues } from '../../../Services/Services';
const CancelInvoicePrintedOrder = ({navigation, route}) => {
  const dispatch = useDispatch();
  const {signOut} = React.useContext(AuthContext);
  const {nameOption, screenName, valueName, data, NotCheck} = route.params;
  const UserName = useSelector((state: RootState) => state.counter.UserName);
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const TableItem = useSelector((state: RootState) => state.counter.TableItem);
  const [Reason, setRease] = useState<string>('');
  const handleGoback = () => {
    navigation.goBack();
  };
  const handleTableUpdate = dataTable => {
    if (AuthenticationError(dataTable.message , dataTable.status_code)) {
      signOut(LoginAgain);
      setIsLoading(false);
      return;
    }
    if (dataTable.message == ResponsiveMessage.TableDontExist) {
      navigation.goBack();
      dispatch(SetRemoveTableNotExist( TableItem.id));
      dispatch(SetModalPosition(''));
      dispatch(
        SetShowModalIsNotification({
          description: TableStandingNotExist,
          showValue: true,
          functionConfirm: '',
          functionCancel: 'handleCloseModalNotification',
          valueBtnConfirm: '',
          valueBtnCancel: '',
        }),
      );
      setIsLoading(false);
      return;
    }
    if (dataTable.status_code == Success) {
      navigation.goBack();
      dispatch(SetUpdateTableCancel({NotCheck: NotCheck}));
      dispatch(SetCloseModalIsNotification());
      setIsLoading(false);
    }
    else {
      navigation.goBack();
      dispatch(
        SetShowModalIsNotification({
          description: dataTable.message,
          showValue: true,
          functionConfirm: '',
          functionCancel: 'handleCloseModalNotification',
          valueBtnConfirm: '',
          valueBtnCancel: '',
        }),
      );
      setIsLoading(false);
    }
  };
  const handleCheckOutTable = dataCheckOut => {
    navigation.goBack();
    if (AuthenticationError(dataCheckOut.message  ,dataCheckOut.status_code)) {
      signOut(LoginAgain);
      setIsLoading(false);
      return;
    }
    if (dataCheckOut.message == ResponsiveMessage.TableDontExist) {
      dispatch(SetRemoveTableNotExist( TableItem.id));
      dispatch(SetModalPosition(''));
      dispatch(
        SetShowModalIsNotification({
          description: TableStandingNotExist,
          showValue: true,
          functionConfirm: '',
          functionCancel: 'handleCloseModalNotification',
          valueBtnConfirm: '',
          valueBtnCancel: '',
        }),
      );
      setIsLoading(false);
      return;
    }
    if (dataCheckOut.status_code == Success) {
      dispatch(SetModalPosition(''));
      dispatch(
        SetShowModalIsNotification({
          description: CancelInvoiceSSuccess,
          showValue: true,
          functionConfirm: '',
          functionCancel: 'handleCloseModalNotification',
          valueBtnConfirm: '',
          valueBtnCancel: '',
        }),
      );
      dispatch(SetTableCancel());
      setIsLoading(false);
    }
    else {
      dispatch(
        SetShowModalIsNotification({
          description: dataCheckOut.message,
          showValue: true,
          functionConfirm: '',
          functionCancel: 'handleCloseModalNotification',
          valueBtnConfirm: '',
          valueBtnCancel: '',
        }),
      );
      setIsLoading(false);
    }
  };
  const handleCreateTaskLog = async () => {
    setIsLoading(true);
    switch (valueName) {
      case 'deleteItemProductTable':
        try {
          const [dataTable] = await Promise.all([
            UpdateTable({
              token: UserName.token,
              data: {
                id: TableItem.id,
                listitem: JSON.stringify(NotCheck),
                user_id: UserName.id,
                status: 0,
                userordered: JSON.stringify(TableItem.userordered),
              },
            }),
          ]);
          handleTableUpdate(dataTable);
        } catch (e) {
          navigation.goBack();
          dispatch(
            SetShowModalIsNotification({
              description: errorNotification,
              showValue: true,
              functionConfirm: '',
              functionCancel: 'handleCloseModalNotification',
              valueBtnConfirm: '',
              valueBtnCancel: '',
            }),
          );
          setIsLoading(false);
        }
        return;
      case 'CancelTable':
        try {
          const [dataCheckOut] = await Promise.all([
            CheckOutTable({
              token: UserName.token,
              data: {
                id: TableItem.id,
                user_id: UserName.id,
              },
            }),
          ]);
          handleCheckOutTable(dataCheckOut);
        } catch (e) {
          navigation.goBack();
          dispatch(
            SetShowModalIsNotification({
              description: errorNotification,
              showValue: true,
              functionConfirm: '',
              functionCancel: 'handleCloseModalNotification',
              valueBtnConfirm: '',
              valueBtnCancel: '',
            }),
          );
          setIsLoading(false);
        }
        return;
    }
  };
  const handleChangeText = (value: string) => {
    setRease(value);
  };
  return (
    <SafeAreaView style={{flex: 1}}>
      <View style={{flex: 1}}>
        <View style={CancelInvoicePrintedOrderCss.HeaderContainer}>
          <TouchableOpacity onPress={handleGoback}>
            <Ionicons
              name="chevron-back-outline"
              size={widthScreen * 0.12}
              color={colors.IconColor}
            />
          </TouchableOpacity>
          <Text style={CancelInvoicePrintedOrderCss.HeaderText}>
            {nameOption !== undefined ? nameOption : ''}
          </Text>
        </View>
        <View style={CancelInvoicePrintedOrderCss.BodyContainer}>
          {nameOption == 'Huỷ Hoá Đơn' ? (
            <Text style={CancelInvoicePrintedOrderCss.BodyContainerTextConfirm}>
              Bạn có chắc chắn muốn huỷ hoá đơn này? Dữ liệu sẽ được lưu lại.
            </Text>
          ) : (
            <Text style={CancelInvoicePrintedOrderCss.BodyContainerTextConfirm}>
              Hoá đơn này có món đã được in bếp bạn có chắc chắn muốn xoá các
              món này? Dữ liệu sẽ được lưu lại.
            </Text>
          )}
          <View style={CancelInvoicePrintedOrderCss.TextInputCssContainer}>
            <TextInput
              placeholder="Nhập lý do"
              onChangeText={value => handleChangeText(value)}
              style={CancelInvoicePrintedOrderCss.TextInputCss}
              placeholderTextColor="#666666"
            />
          </View>
          <View style={{flex: 1, marginTop: 20}}>
            <ScrollView
              style={{flex: 1}}
              showsHorizontalScrollIndicator={false}
              showsVerticalScrollIndicator={false}>
              {data !== undefined
                ? data.map((item, index: number) => (
                    <View
                      key={index}
                      style={CancelInvoicePrintedOrderCss.ContainerProduct}>
                      <Ionicons
                        name="checkmark-outline"
                        size={widthScreen * 0.06}
                        color="#33CC66"
                      />
                      <Text
                        style={CancelInvoicePrintedOrderCss.TitleProduct}
                        numberOfLines={1}>
                        {item.title}
                      </Text>
                      <Text
                        style={CancelInvoicePrintedOrderCss.PriceProduct}
                        numberOfLines={1}>
                        {CommaSeparatedValues(item.price)}
                      </Text>
                      <Text
                        style={CancelInvoicePrintedOrderCss.QuantityProduct}
                        numberOfLines={1}>
                        {item.quantity}
                      </Text>
                      <Text
                        style={CancelInvoicePrintedOrderCss.StatusProduct}
                        numberOfLines={1}>
                        {item.status == 2 ? 'Đã in bếp' : 'Chưa in bếp'}
                      </Text>
                    </View>
                  ))
                : ''}
            </ScrollView>
          </View>
          <View style={CancelInvoicePrintedOrderCss.BtnConfirmContainer}>
            <TouchableOpacity
              disabled={RegexReason.test(Reason) ? true : false}
              onPress={handleCreateTaskLog}
              style={CancelInvoicePrintedOrderCss.BtnConfirmTouch}>
              <Text
                style={[
                  CancelInvoicePrintedOrderCss.BtnConfirmText,
                  RegexReason.test(Reason)
                    ? {color: '#999999'}
                    : {color: '#0066CC'},
                ]}>
                Xác nhận
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
      {isLoading ? <LoadingComponent /> : ''}
    </SafeAreaView>
  );
};

export default React.memo(CancelInvoicePrintedOrder);
