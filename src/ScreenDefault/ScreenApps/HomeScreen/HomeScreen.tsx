import {
  TouchableOpacity,
  View,
  Text,
  ScrollView,
  LogBox,
  BackHandler,
  Image,
  SafeAreaView,
} from 'react-native';
import React, {useEffect, useCallback, useState} from 'react';
import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import {useSelector, useDispatch} from 'react-redux';
import {
  LoadingComponent,
  ListTableMove,
  ModalNotification,
  TableDetail,
  ItemTable,
} from '../../../Components/Components';
import {
  SetTable,
  SetUserName,
  setProductAndCategory,
  SetReloadTable,
  SetShowMOdalListTable,
  SetShowModalIsNotification,
  SetCloseModalIsNotification,
  SetShowModalCreateCustomer,
} from '../../../Redux/Slide';
import {ActivityIndicator} from 'react-native-paper';
import { widthScreen} from '../../../Utils/Styles';
import {GetCategory, getProduct, getTable} from '../../../Utils/GetApi';
import {HomeScreenCss} from './HomeScreenCss';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {RootState} from '../../../Redux/Storage';
import {
  dataProduct,
  dataUser,
  ProductItem,
  Table,
} from '../../../Utils/interface/interface';
import {AuthenticationError, xmlChangeText} from '../../../Utils/Helper';
import {Success} from '../../../Utils/Enums';
import {
  LoginAgain,
  OutApp,
  OutTable,
  OutTableNotAddProduct,
  errorNotification,
} from '../../../Utils/MessageNotification';
import {AuthContext} from '../../../DefaultLayout/DefaultLayoutScreen';
import {scale} from '../../../Utils/Reponsive';
LogBox.ignoreLogs(['Warning: ...']);
LogBox.ignoreAllLogs();
const HomeScreen = ({navigation}) => {
  const dispatch = useDispatch();
  const {signOut} = React.useContext(AuthContext);
  const [UsingTable, SetUsingTable] = useState<number>(1);
  const ShowTableDetail = useSelector(
    (state: RootState) => state.counter.ShowTableDetail,
  );
  const XmlProperties = useSelector(
    (state: RootState) => state.item.XmlProperties,
  );
  const HomeXmlParser = XmlProperties !== null ? XmlProperties[2] : null;
  const Table = useSelector((state: RootState) => state.counter.Table);
  const ModalPosition = useSelector(
    (state: RootState) => state.counter.ModalPosition,
  );
  const ConnectNetwork = useSelector(
    (state: RootState) => state.item.ConnectNetwork,
  );
  const isNotification = useSelector(
    (state: RootState) => state.counter.isNotification,
  );
  const NameModal = useSelector((state: RootState) => state.counter.NameModal);
  const initialValueModal = useSelector(
    (state: RootState) => state.counter.initialValueModal,
  );
  const ShowMOdalListTable = useSelector(
    (state: RootState) => state.counter.ShowMOdalListTable,
  );
  const ShowCreateModal = useSelector(
    (state: RootState) => state.counter.ShowCreateModal,
  );
  const UserName = useSelector((state: RootState) => state.counter.UserName);
  const isLoading = useSelector((state: RootState) => state.counter.isLoading);
  const [CheckUrl, setCheckUrl] = useState<boolean>(false);
  const renderTable = useSelector(
    (state: RootState) => state.counter.renderTable,
  );
  const [tableLoadCheck, setTableLoadCheck] = useState<boolean>(true);
  const [LoadingData, setLoadingData] = useState<boolean>(false);
  const handelChangeTableUsing = (value: number) => {
    SetUsingTable(value);
  };
  const handleReloadItem = useCallback(() => {
    setTableLoadCheck(false);
    dispatch(SetReloadTable());
    const timeid = setTimeout(() => {
      setTableLoadCheck(true);
    }, 2000);
    return () => {
      clearTimeout(timeid);
    };
  }, []);
  useEffect(() => {
    const fetchTable = async () => {
      setLoadingData(false);
      const datauser = await AsyncStorage.getItem('userToken');
      const dataii = datauser ? JSON.parse(datauser) : null;
      if (dataii !== null) {
        dispatch(SetUserName(dataii));
        dispatch(SetTable([]));
        try {
          const [dataTable, dataProduct] = await Promise.all([
            getTable({token: dataii.token}),
            getProduct({token: dataii.token}),
          ]);
          handelGetTable(dataTable);
          SetUsingTable(1);
          handelGetProduct(dataProduct, dataii);
        } catch (e) {
          handelError(e);
        }
      }
      setLoadingData(true);
    };
    fetchTable();
  }, [renderTable]);
  useEffect(() => {
    fetch(`${UserName.logo}`)
      .then(data => {
        if (data.status == Success) {
          setCheckUrl(true);
        } else {
          setCheckUrl(false);
        }
      })
      .catch(er => setCheckUrl(false));
  }, [UserName]);
  const handelGetProduct = (dataProduct: dataProduct, dataii: dataUser) => {
    if (dataProduct.status_code == 500) {
      dispatch(setProductAndCategory({dataProductItem: [], dataCategory: []}));
      handelError('');
    }
    if (dataProduct.status_code == Success) {
      GetCategory({token: dataii.token})
        .then(data => {
          if (data.status_code == Success) {
            const newArray: ProductItem[] = [];
            for (let i = 0; i < dataProduct.product.length; i++) {
              if (dataProduct.product[i].status !== -1) {
                newArray.push({
                  id: dataProduct.product[i].id,
                  price: dataProduct.product[i].price,
                  check: dataProduct.product[i].check,
                  vat: dataProduct.product[i].vat,
                  quantity: dataProduct.product[i].quantity,
                  image: dataProduct.product[i].image,
                  title: dataProduct.product[i].title,
                  category_id: dataProduct.product[i].category_id,
                  status: dataProduct.product[i].status,
                  qtyStatus: 0,
                });
              }
            }
            dispatch(
              setProductAndCategory({
                dataProductItem: newArray,
                dataCategory: data.category_list,
              }),
            );
          } else {
            dispatch(
              setProductAndCategory({dataProductItem: [], dataCategory: []}),
            );
          }
        })
        .catch(e => handelError(e));
    } else {
      handelError('');
      dispatch(setProductAndCategory({dataProductItem: [], dataCategory: []}));
    }
  };
  const handelGetTable = data => {
    if (AuthenticationError(data.message, data.status_code)) {
      signOut(LoginAgain);
      return;
    }
    if (data.status_code == Success) {
      const newArray: Table[] = [];
      for (let i = 0; i < data.listtable.length; i++) {
        if (data.listtable[i].status !== -1) {
          newArray.push({
            id: data.listtable[i].id,
            listitem:
              data.listtable[i].listitem !== null
                ? JSON.parse(data.listtable[i].listitem)
                : [],
            status: data.listtable[i].status,
            tableName: data.listtable[i].tablename,
            user_id: data.user_id,
            userordered:
              data.listtable[i].userordered !== null
                ? JSON.parse(data.listtable[i].userordered)
                : [],
            name_user:
              data.listtable[i].name_user !== undefined
                ? data.listtable[i].name_user
                : '',
          });
        }
      }
      const datasort = newArray.sort((a: any, b: any) => {
        return a.tableName - b.tableName;
      });
      dispatch(SetTable(datasort));
    } else {
      dispatch(SetTable([]));
    }
  };
  const handelError = (e: any) => {
    dispatch(
      SetShowModalIsNotification({
        description: errorNotification,
        showValue: true,
        functionConfirm: '',
        functionCancel: 'handleCloseModalNotification',
        valueBtnConfirm: '',
        valueBtnCancel: '',
      }),
    );
  };
  const backAction = () => {
    if (NameModal !== '') {
      dispatch(SetShowMOdalListTable({show: false, NameModal:''}));
      return true;
    }
    if (ShowCreateModal) {
      dispatch(SetShowModalCreateCustomer(false));
      return true;
    }
    if (initialValueModal.description !== '') {
      dispatch(SetCloseModalIsNotification());
      return true;
    }
    if (ModalPosition == 'TableAddProduct') {
      dispatch(
        SetShowModalIsNotification({
          description: OutTableNotAddProduct,
          showValue: true,
          functionConfirm: 'handleCheckOutTableProduct',
          functionCancel: 'handleCloseModalNotification',
          valueBtnConfirm: '',
          valueBtnCancel: '',
        }),
      );
      return true;
    }
    if (navigation.isFocused()) {
      if (ModalPosition == 'TableDetail') {
        dispatch(SetShowMOdalListTable({show: false, NameModal: ''}));
        dispatch(
          SetShowModalIsNotification({
            description: OutTable,
            showValue: true,
            functionConfirm: 'handleCheckOutTableDetail',
            functionCancel: 'handleCloseModalNotification',
            valueBtnConfirm: '',
            valueBtnCancel: '',
          }),
        );
        return true;
      }
      dispatch(
        SetShowModalIsNotification({
          description: OutApp,
          showValue: true,
          functionConfirm: 'handleExitApp',
          functionCancel: 'handleCloseModalNotification',
          valueBtnConfirm: '',
          valueBtnCancel: '',
        }),
      );
      return true;
    }
  };
  useEffect(() => {
    const backHandler = BackHandler.addEventListener(
      'hardwareBackPress',
      backAction,
    );
    return () => backHandler.remove();
  }, [
    ModalPosition,
    NameModal,
    initialValueModal.description,
    ShowCreateModal,
  ]);
  const handleOpenDrawerModal = () => {
    dispatch(SetCloseModalIsNotification());
    navigation.openDrawer();
  };
  return (
    <SafeAreaView style={HomeScreenCss.container}>
      <View style={HomeScreenCss.headerContainer}>
        <View style={HomeScreenCss.headerContainerTop}>
          <View style={HomeScreenCss.headerText}>
            <View>
              {UserName.logo !== null &&
              UserName.logo !== undefined &&
              CheckUrl ? (
                <Image
                  style={[
                    HomeScreenCss.ImgLogo,
                    {
                      height: scale(
                        xmlChangeText(HomeXmlParser, 'HeightLogo', 40),
                      ),
                      width: scale(
                        xmlChangeText(HomeXmlParser, 'WidthLogo', 40),
                      ),
                    },
                  ]}
                  source={{uri: `${UserName.logo}`}}
                />
              ) : (
                <Text></Text>
              )}
            </View>
            <View style={{marginLeft: 10}}>
              <Text numberOfLines={1} style={HomeScreenCss.TextUserName}>
                {UserName.name !== null && UserName.name !== undefined? UserName.name: ''}
              </Text>
              <View style={HomeScreenCss.UserTextInfoContainer}>
                <Text style={[HomeScreenCss.StatusText , {color:ConnectNetwork.connected ? '#00CC00' : '#FFD700'}]}>{ConnectNetwork.connected ?  "Đang hoạt động" : "Mất kết nối mạng"}</Text>
                <View style={[HomeScreenCss.statusDot ,{backgroundColor:ConnectNetwork.connected ?  '#33FF00' : '#FFD700'}]}></View>
              </View>
            </View>
          </View>
          <View style={HomeScreenCss.iconHeader}>
            <TouchableOpacity
              onPress={() => handleReloadItem()}
              disabled={tableLoadCheck ? false : true}
              style={HomeScreenCss.containerReload}>
              <Ionicons
                name={xmlChangeText(
                  HomeXmlParser,
                  'IconReload',
                  'reload-outline',
                )}
                size={widthScreen * 0.08}
                color={
                  tableLoadCheck
                    ? xmlChangeText(HomeXmlParser, 'IconReloadColor', '#0066CC')
                    : '#444444'
                }
              />
            </TouchableOpacity>
            <TouchableOpacity onPress={handleOpenDrawerModal}>
              <MaterialIcons
                color={xmlChangeText(
                  HomeXmlParser,
                  'IconOptionColor',
                  '#0066CC',
                )}
                name={xmlChangeText(HomeXmlParser, 'IconOption', 'reorder')}
                size={widthScreen * 0.1}
              />
            </TouchableOpacity>
          </View>
        </View>
        <View style={HomeScreenCss.containerFilter}>
          <View style={HomeScreenCss.containerFilterBody}>
            <TouchableOpacity
              onPress={() => handelChangeTableUsing(1)}
              style={
                UsingTable == 1
                  ? HomeScreenCss.btnFilterValid
                  : HomeScreenCss.btnFilterNot
              }>
              <Text
                style={
                  UsingTable == 1
                    ? HomeScreenCss.containerFilterBodyBtnTextValid
                    : HomeScreenCss.containerFilterBodyBtnTextNotValid
                }>
                {xmlChangeText(
                  HomeXmlParser,
                  'ScrollFilter.NameOption1',
                  'Tất cả',
                )}
              </Text>
              {UsingTable == 1 ? (
                <View style={HomeScreenCss.lineAlan}></View>
              ) : (
                ''
              )}
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => handelChangeTableUsing(2)}
              style={
                UsingTable == 2
                  ? HomeScreenCss.btnFilterValid
                  : HomeScreenCss.btnFilterNot
              }>
              <Text
                style={
                  UsingTable == 2
                    ? HomeScreenCss.containerFilterBodyBtnTextValid
                    : HomeScreenCss.containerFilterBodyBtnTextNotValid
                }>
                {xmlChangeText(
                  HomeXmlParser,
                  'ScrollFilter.NameOption2',
                  'Sử dụng',
                )}
              </Text>
              {UsingTable == 2 ? (
                <View style={HomeScreenCss.lineAlan}></View>
              ) : (
                ''
              )}
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => handelChangeTableUsing(3)}
              style={
                UsingTable == 3
                  ? HomeScreenCss.btnFilterValid
                  : HomeScreenCss.btnFilterNot
              }>
              <Text
                style={
                  UsingTable == 3
                    ? HomeScreenCss.containerFilterBodyBtnTextValid
                    : HomeScreenCss.containerFilterBodyBtnTextNotValid
                }>
                {xmlChangeText(
                  HomeXmlParser,
                  'ScrollFilter.NameOption3',
                  'Còn trống',
                )}
              </Text>
              {UsingTable == 3 ? (
                <View style={HomeScreenCss.lineAlan}></View>
              ) : (
                ''
              )}
            </TouchableOpacity>
          </View>
        </View>
      </View>
      {LoadingData ? (
        <ScrollView
          style={HomeScreenCss.ScrollContainer}
          showsVerticalScrollIndicator={false}>
          <View style={HomeScreenCss.bodyTableProduct}>
            {Table.map((item, index) => {
              if (UsingTable === 1) {
                return (
                  <ItemTable
                    key={index}
                    index={index}
                    item={item}
                    navigation={navigation}
                  />
                );
              } else if (
                (UsingTable === 2 && item.listitem.length > 0) ||
                (UsingTable === 2 && item.status == 0)
              ) {
                return (
                  <ItemTable
                    key={index}
                    index={index}
                    item={item}
                    navigation={navigation}
                  />
                );
              } else if (
                UsingTable === 3 &&
                item.status !== 0 &&
                item.listitem.length <= 0
              ) {
                return (
                  <ItemTable
                    key={index}
                    index={index}
                    item={item}
                    navigation={navigation}
                  />
                );
              }
            })}
          </View>
        </ScrollView>
      ) : (
        <View style={{flex: 1}}>
          <ActivityIndicator
            size={42}
            color="#0099FF"
            style={{alignItems: 'center', justifyContent: 'center', flex: 1}}
          />
        </View>
      )}
      {isNotification ? <ModalNotification /> : ''}
      {ShowTableDetail ? <TableDetail navigation={navigation} /> : ''}
      {ShowMOdalListTable ? <ListTableMove /> : ''}
      {isLoading ? <LoadingComponent /> : ''}
    </SafeAreaView>
  );
};
export default React.memo(HomeScreen);
