import {Platform, StyleSheet} from 'react-native';
import {heightWindown, widthScreen} from '../../../../Utils/Styles';
import {verticalScale} from '../../../../Utils/Reponsive';
export const ContactFormCss = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FF9900',
  },
  header: {
    flex: 1,
    paddingBottom: 30,
    alignItems: 'center',
    flexDirection: 'row',
  },
  footer: {
    flex: 5,
    backgroundColor: '#fff',
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    paddingHorizontal: 20,
    paddingVertical: 20,
  },
  text_header: {
    fontWeight: '700',
    color: '#fff',
    fontSize: widthScreen * 0.06,
  },
  text_footer: {
    color: '#05375A',
    fontSize: widthScreen * 0.04,
    fontWeight: '600',
  },
  action: {
    flexDirection: 'row',
    marginTop: 20,
    borderBottomColor: '#f2f2f2',
    paddingBottom: 2,
    borderBottomWidth: 2,
  },
  textInput: {
    flex: 1,
    marginTop: Platform.OS == 'ios' ? 0 : -12,
    paddingLeft: 10,
    color: '#05375a',
    fontSize: widthScreen * 0.036,
  },
  button: {
    alignItems: 'flex-end',
    marginTop: heightWindown > 540 ? 30 : 20,
  },
  signIn: {
    width: '100%',
    height: verticalScale(60),
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 10,
  },
  TextSign: {
    fontWeight: '700',
    fontSize: widthScreen * 0.05,
  },
  TextError: {
    color: 'red',
    paddingVertical: 6,
    fontSize: widthScreen * 0.036,
    fontWeight: '600',
  },
});
