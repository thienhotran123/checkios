import {
  Text,
  TextInput,
  View,
  Animated,
  TouchableOpacity,
  SafeAreaView,
  TouchableWithoutFeedback,
  Keyboard,
  ScrollView,
  LogBox,
  KeyboardAvoidingView,
} from 'react-native';
import React from 'react';
import {ContactFormCss} from './ContactFormCss';
import LinearGradient from 'react-native-linear-gradient';
import Ionicons from 'react-native-vector-icons/Ionicons';
import {useState} from 'react';
import {widthScreen} from '../../../../Utils/Styles';
import {Formik} from 'formik';
import * as Yup from 'yup';
import {SetShowModalIsNotification} from '../../../../Redux/Slide';
import {CreateContact} from '../../../../Utils/GetApi';
import {useDispatch, useSelector} from 'react-redux';
import {SetDataSettings} from '../../../../Redux/order';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {useEffect} from 'react';
import {RootState} from '../../../../Redux/Storage';
import {
  LoadingComponent,
  ModalNotification,
} from '../../../../Components/Components';
import {xmlChangeText} from '../../../../Utils/Helper';
import {Success} from '../../../../Utils/Enums';
import {
  SendContactFail,
  SendContactSuccess,
  errorNotification,
} from '../../../../Utils/MessageNotification';
LogBox.ignoreLogs(['Warning: ...']);
LogBox.ignoreAllLogs();
const ContactFormScreen = ({navigation}) => {
  const isNotification = useSelector((state: RootState) => state.counter.isNotification);
  const XmlProperties =useSelector((state: RootState) => state.item.XmlProperties)
  const ContactXmlParser = XmlProperties !==null ? XmlProperties[1] : null
  const SignupSchema = Yup.object().shape({
    name: Yup.string().required(xmlChangeText(ContactXmlParser , "Validation.Name.Required",'Vui lòng nhập họ tên của bạn')),
    numberphone: Yup.string()
      .max(11,xmlChangeText(ContactXmlParser , "validation.Numberphone.Max",'Số điện thoại Không Vượt quá 11 số'))
      .required(xmlChangeText(ContactXmlParser , "Validation.Numberphone.Required",'Vui lòng nhập số điện thoại của bạn'))
      .matches(
        /(02|03|028|05|07|08|09|01[2|6|8|9])+([0-9]{8})\b/,
        xmlChangeText(ContactXmlParser , "Validation.Numberphone.MatchesText",'Số điện thoại không đúng định dạng'),
      ),
    address: Yup.string().required(xmlChangeText(ContactXmlParser , "Validation.Address.Required",'Vui lòng nhập địa chỉ của bạn')),
    info_contact: Yup.string().required(xmlChangeText(ContactXmlParser , "Validation.Info_contact.Required",'Vui lòng nhập thông tin')),
  });
  const dispatch = useDispatch();
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const handleOut = e => {
    dispatch(
      SetShowModalIsNotification({
        description: errorNotification,
        showValue: true,
        functionConfirm: '',
        functionCancel: 'handleCloseModalNotification',
        valueBtnConfirm: '',
        valueBtnCancel: '',
      }),
    );
  };
  const handelLoginUser = async e => {
    setIsLoading(true);
    CreateContact({
      name: e.name,
      numberphone: e.numberphone,
      address: e.address,
      info_contact: e.info_contact,
    })
      .then(data => {
        if (data.status_code == Success) {
          dispatch(
            SetShowModalIsNotification({
              description: SendContactSuccess,
              showValue: true,
              functionConfirm: '',
              functionCancel: 'handleCloseModalNotification',
              valueBtnConfirm: '',
              valueBtnCancel: '',
            }),
          );
          navigation.replace('ScreenLogin');
        } else {
          dispatch(
            SetShowModalIsNotification({
              description: SendContactFail,
              showValue: true,
              functionConfirm: '',
              functionCancel: 'handleCloseModalNotification',
              valueBtnConfirm: '',
              valueBtnCancel: '',
            }),
          );
        }
      })
      .catch(e => handleOut(e))
      .finally(() => setIsLoading(false));
  };
  useEffect(() => {
    const getdataCart = async () => {
      const DataSettings = await AsyncStorage.getItem('DataSettings');
      if (DataSettings !== null) {
        const DataSetting = JSON.parse(DataSettings);
        dispatch(SetDataSettings(DataSetting));
      } else {
        dispatch(SetDataSettings({PrintIp: '192.168.1.122'}));
      }
    };
    getdataCart();
  }, []);
  const handelSinginUp = () => {
    navigation.replace('ScreenLogin');
  };
  return (
    <SafeAreaView style={ContactFormCss.container}>
      <KeyboardAvoidingView style={{flex: 1}}>
        <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
          <View style={{flex: 1}}>
            <View style={ContactFormCss.header}>
              <TouchableOpacity onPress={() => handelSinginUp()}>
                <Ionicons
                  name={xmlChangeText(ContactXmlParser , "IconNavigation" , 'chevron-back-outline')}
                  size={widthScreen * 0.12}
                  color={xmlChangeText(ContactXmlParser , "IconNavigationColor",'#fff')}
                />
              </TouchableOpacity>
              <View style={{flex: 1, flexDirection: 'row'}}>
                <Text style={ContactFormCss.text_header}>
                  {xmlChangeText(ContactXmlParser , "TextHeader",'Gửi Thông Tin Liên Hệ')}
                </Text>
              </View>
            </View>
            <View style={ContactFormCss.footer}>
              <ScrollView
                style={{flex: 1}}
                showsVerticalScrollIndicator={false}>
                <Formik
                  initialValues={{
                    name: '',
                    numberphone: '',
                    address: '',
                    info_contact: '',
                  }}
                  validationSchema={SignupSchema}
                  onSubmit={e => handelLoginUser(e)}>
                  {({
                    handleChange,
                    handleSubmit,
                    values,
                    errors,
                    touched,
                    isValid,
                    setFieldTouched,
                  }) => (
                    <View>
                      <Text style={ContactFormCss.text_footer}>{xmlChangeText(ContactXmlParser , "InputName.NameHeader",'Họ và tên')}</Text>
                      <View style={ContactFormCss.action}>
                        <Ionicons
                          name={xmlChangeText(ContactXmlParser , "InputName.Icon",'person-outline')}
                          color={xmlChangeText(ContactXmlParser , "InputName.IconColor",'#05375a')}
                          size={widthScreen * 0.06}
                        />
                        <TextInput
                          placeholder={xmlChangeText(ContactXmlParser , "InputName.PlaceHolder",'Nhập họ và tên của bạn')}
                          placeholderTextColor="#999999"
                          style={ContactFormCss.textInput}
                          autoCapitalize="none"
                          onChangeText={handleChange('name')}
                          onBlur={() => setFieldTouched('name')}
                          value={values.name}
                          keyboardType="default"
                        />
                        {!errors.name && touched.name ? (
                          <Animated.View>
                            <Ionicons
                              name={xmlChangeText(ContactXmlParser , "InputName.IconValid",'checkmark-circle-outline')}
                              color={xmlChangeText(ContactXmlParser , "InputName.IconValidColor",'green')}
                              size={widthScreen * 0.06}
                            />
                          </Animated.View>
                        ) : null}
                      </View>
                      {touched.name && errors.name && (
                        <Text style={ContactFormCss.TextError}>
                          {errors.name}
                        </Text>
                      )}
                      <Text
                        style={[ContactFormCss.text_footer, {marginTop: 12}]}>
                        {xmlChangeText(ContactXmlParser , "InputPhone.NameHeader",'Số điện thoại')}
                      </Text>
                      <View style={ContactFormCss.action}>
                        <Ionicons
                          name={xmlChangeText(ContactXmlParser , "InputPhone.Icon",'call-outline')}
                          color={xmlChangeText(ContactXmlParser , "InputPhone.IconColor",'#05375a')}
                          size={widthScreen * 0.06}
                        />
                        <TextInput
                          placeholder={xmlChangeText(ContactXmlParser , "InputPhone.PlaceHolder",'Nhập số điện thoại của bạn')}
                          placeholderTextColor="#999999"
                          style={ContactFormCss.textInput}
                          autoCapitalize="none"
                          onChangeText={handleChange('numberphone')}
                          onBlur={() => setFieldTouched('numberphone')}
                          value={values.numberphone}
                          keyboardType="numeric"
                        />
                        {!errors.numberphone && touched.numberphone ? (
                          <Animated.View>
                            <Ionicons
                              name={xmlChangeText(ContactXmlParser , "InputPhone.IconValid",'checkmark-circle-outline')}
                              color={xmlChangeText(ContactXmlParser , "InputPhone.IconValidColor",'green')}
                              size={widthScreen * 0.06}
                            />
                          </Animated.View>
                        ) : null}
                      </View>
                      {touched.numberphone && errors.numberphone && (
                        <Text style={ContactFormCss.TextError}>
                          {errors.numberphone}
                        </Text>
                      )}
                      <Text
                        style={[ContactFormCss.text_footer, {marginTop: 12}]}>
                        {xmlChangeText(ContactXmlParser , "InputAdress.NameHeader",'Địa chỉ')}
                      </Text>
                      <View style={ContactFormCss.action}>
                        <Ionicons
                          name={xmlChangeText(ContactXmlParser , "InputAdress.Icon",'navigate-outline')}
                          color={xmlChangeText(ContactXmlParser , "InputAdress.IconColor",'#05375a')}
                          size={widthScreen * 0.06}
                        />
                        <TextInput
                          placeholder={xmlChangeText(ContactXmlParser , "InputAdress.PlaceHolder",'Nhập địa chỉ của bạn')}
                          placeholderTextColor="#999999"
                          style={ContactFormCss.textInput}
                          autoCapitalize="none"
                          onChangeText={handleChange('address')}
                          onBlur={() => setFieldTouched('address')}
                          value={values.address}
                          keyboardType="default"
                        />
                        {!errors.address && touched.address ? (
                          <Animated.View>
                            <Ionicons
                              name={xmlChangeText(ContactXmlParser , "InputAdress.IconValid",'checkmark-circle-outline')}
                              color={xmlChangeText(ContactXmlParser , "InputAdress.IconValidColor",'green')}
                              size={widthScreen * 0.06}
                            />
                          </Animated.View>
                        ) : null}
                      </View>
                      {touched.address && errors.address && (
                        <Text style={ContactFormCss.TextError}>
                          {errors.address}
                        </Text>
                      )}
                      <Text
                        style={[ContactFormCss.text_footer, {marginTop: 12}]}>
                          {xmlChangeText(ContactXmlParser , "InputInfoContact.NameHeader",'Nội dung cần liên lạc')}
                      </Text>
                      <View style={ContactFormCss.action}>
                        <Ionicons
                          name={xmlChangeText(ContactXmlParser , "InputInfoContact.Icon",'information-circle-outline')}
                          color={xmlChangeText(ContactXmlParser , "InputInfoContact.IconColor",'#05375a')}
                          size={widthScreen * 0.06}
                        />
                        <TextInput
                          placeholder={xmlChangeText(ContactXmlParser , "InputInfoContact.placeholderPhone",'Nhập nội dung')}
                          style={ContactFormCss.textInput}
                          autoCapitalize="none"
                          placeholderTextColor="#999999"
                          onChangeText={handleChange('info_contact')}
                          onBlur={() => setFieldTouched('info_contact')}
                          value={values.info_contact}
                        />
                        {!errors.info_contact && touched.info_contact ? (
                          <Animated.View>
                            <Ionicons
                              name={xmlChangeText(ContactXmlParser , "InputInfoContact.IconValid",'checkmark-circle-outline')}
                              color={xmlChangeText(ContactXmlParser , "InputInfoContact.IconValidColor",'green')}
                              size={widthScreen * 0.06}
                            />
                          </Animated.View>
                        ) : null}
                      </View>
                      {touched.info_contact && errors.info_contact && (
                        <Text style={ContactFormCss.TextError}>
                          {errors.info_contact}
                        </Text>
                      )}
                      <TouchableOpacity
                        disabled={!isValid}
                        onPress={() => handleSubmit()}
                        style={ContactFormCss.button}>
                        <LinearGradient colors={isValid ? ['#FF9900', '#FF9900'] : ['#808080', '#808080']} style={ContactFormCss.signIn}>
                          <Text
                            style={[ContactFormCss.TextSign, {color: '#fff'}]}>
                            {xmlChangeText(ContactXmlParser , "ButtonSubmitText",'Gửi Thông Tin')}
                          </Text>
                        </LinearGradient>
                      </TouchableOpacity>
                    </View>
                  )}
                </Formik>
              </ScrollView>
            </View>
          </View>
        </TouchableWithoutFeedback>
      </KeyboardAvoidingView>
      {isLoading ? <LoadingComponent /> : ''}
      {isNotification ? <ModalNotification /> : ''}
    </SafeAreaView>
  );
};

export default ContactFormScreen;
