import {
  Text,
  TextInput,
  View,
  Animated,
  TouchableOpacity,
  SafeAreaView,
  TouchableWithoutFeedback,
  Keyboard,
  ScrollView,
  LogBox,
  Linking,
  Platform,
} from 'react-native';
import React from 'react';
import {ScreenLoginCss} from './ScreenLoginCss';
import LinearGradient from 'react-native-linear-gradient';
import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import {useState} from 'react';
import {widthScreen} from '../../../../Utils/Styles';
import {Formik} from 'formik';
import * as Yup from 'yup';
import {SetShowModalIsNotification} from '../../../../Redux/Slide';
import {Login} from '../../../../Utils/GetApi';
import {useDispatch, useSelector} from 'react-redux';
import {AuthContext} from '../../../../DefaultLayout/DefaultLayoutScreen';
import {SetDataSettings} from '../../../../Redux/order';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {useEffect} from 'react';
import {KeyboardAvoidingView} from 'react-native';
import {RootState} from '../../../../Redux/Storage';
import {
  LoadingComponent,
  ModalNotification
} from '../../../../Components/Components';
import {xmlChangeText} from '../../../../Utils/Helper';
import {Success} from '../../../../Utils/Enums';
import {errorNotification} from '../../../../Utils/MessageNotification';
LogBox.ignoreLogs(['Warning: ...']);
LogBox.ignoreAllLogs();
const ScreenLogin = ({navigation}) => {
  const {signIn} = React.useContext(AuthContext);
  const isNotification = useSelector(
    (state: RootState) => state.counter.isNotification,
  );
  const XmlProperties = useSelector(
    (state: RootState) => state.item.XmlProperties,
  );
  const LoginXmlParser = XmlProperties !== null ? XmlProperties[0] : null;
  const SignupSchema = Yup.object().shape({
    Phone: Yup.string()
      .max(
        11,
        xmlChangeText(
          LoginXmlParser,
          'Validation.Phone.Max',
          'Số điện thoại không vượt quá 11 số',
        ),
      )
      .required(
        xmlChangeText(
          LoginXmlParser,
          'Validation.Phone.Required',
          'Vui lòng nhập số điện thoại của bạn',
        ),
      )
      .matches(
        /(02|03|028|05|07|08|09|01[2|6|8|9])+([0-9]{8})\b/,
        xmlChangeText(
          LoginXmlParser,
          'Validation.Phone.MatchesText',
          'Số điện thoại không đúng định dạng',
        ),
      ),
    passWord: Yup.string()
      .min(
        4,
        xmlChangeText(
          LoginXmlParser,
          'Validation.Password.Min',
          'Độ dài lớn hơn 4 kí tự',
        ),
      )
      .required(
        xmlChangeText(
          LoginXmlParser,
          'Validation.Password.Required',
          'Vui lòng nhập mật khẩu của bạn',
        ),
      ),
  });
  const dispatch = useDispatch();
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const [data, setData] = useState({
    Phone: '',
    password: '',
    check_textInputChage: false,
    secureTextEntry: true,
  });
  const handlesetShowEye = () => {
    setData({...data, secureTextEntry: !data.secureTextEntry});
  };
  const handleChekcLogin = (data, pass: string) => {
    if (data.status_code == Success) {
      signIn(data, pass);
    } else {
      dispatch(
        SetShowModalIsNotification({
          description: data.message,
          showValue: true,
          functionConfirm: '',
          functionCancel: 'handleCloseModalNotification',
          valueBtnConfirm: '',
          valueBtnCancel: '',
        }),
      );
    }
  };
  const handleOut = e => {
    dispatch(
      SetShowModalIsNotification({
        description: errorNotification,
        showValue: true,
        functionConfirm: '',
        functionCancel: 'handleCloseModalNotification',
        valueBtnConfirm: '',
        valueBtnCancel: '',
      }),
    );
  };
  const handelLoginUser = async (e: any) => {
    setIsLoading(true);
    Login({phone: e.Phone, password: e.passWord})
      .then(data => handleChekcLogin(data, e.passWord))
      .catch(e => handleOut(e))
      .finally(() => setIsLoading(false));
  };
  const handleLinkingCall = () => {
    let phoneNumber = '';
    if (Platform.OS === 'android') {
      phoneNumber = `tel:${LoginXmlParser!== null && LoginXmlParser['FooterSupportNumberPhone'] !==undefined ?LoginXmlParser['FooterSupportNumberPhone']: '02862651411'}`;
    } else {
      phoneNumber = `telprompt:${LoginXmlParser!== null && LoginXmlParser['FooterSupportNumberPhone'] !==undefined ?LoginXmlParser['FooterSupportNumberPhone']: '02862651411'}`;
    }
    Linking.openURL(phoneNumber);
  };
  useEffect(() => {
    const getdataCart = async () => {
      const DataSettings = await AsyncStorage.getItem('DataSettings');
      if (DataSettings !== null) {
        const DataSetting = JSON.parse(DataSettings);
        dispatch(SetDataSettings(DataSetting));
      } else {
        dispatch(SetDataSettings({PrintIp: '192.168.1.122'}));
      }
    };
    getdataCart();
  }, []);
  const handelSinginUp = () => {
    navigation.navigate('ContactFormScreen');
  };
  return (
    <SafeAreaView style={ScreenLoginCss.container}>
      <KeyboardAvoidingView style={{flex: 1}}>
        <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
          <View style={{flex: 1}}>
            <View style={ScreenLoginCss.header}>
              <View
                style={{
                  flex: 1,
                  alignItems: 'center',
                  justifyContent: 'center',
                  flexDirection: 'row',
                }}>
                <Text style={ScreenLoginCss.text_header}>
                  {xmlChangeText(LoginXmlParser, 'TextHeader', 'Delta POS')}
                </Text>
                <MaterialIcons
                  name={xmlChangeText(LoginXmlParser, 'IconHeader', 'store')}
                  color="#fff"
                  size={widthScreen * 0.1}
                  style={{marginLeft: 20}}
                />
              </View>
            </View>
            <View style={ScreenLoginCss.footer}>
              <ScrollView
                style={{flex: 1}}
                showsVerticalScrollIndicator={false}>
                <Formik
                  initialValues={{
                    Phone: '',
                    passWord: '',
                  }}
                  validationSchema={SignupSchema}
                  onSubmit={e => handelLoginUser(e)}>
                  {({
                    handleChange,
                    handleSubmit,
                    values,
                    errors,
                    touched,
                    isValid,
                    setFieldTouched,
                  }) => (
                    <View>
                      <Text style={ScreenLoginCss.text_footer}>
                        {xmlChangeText(
                          LoginXmlParser,
                          'InputPhone.NameHeader',
                          'Số điện thoại',
                        )}
                      </Text>
                      <View style={ScreenLoginCss.action}>
                        <Ionicons
                          name={xmlChangeText(
                            LoginXmlParser,
                            'InputPhone.Icon',
                            'call-outline',
                          )}
                          color={xmlChangeText(
                            LoginXmlParser,
                            'InputPhone.IconColor',
                            '#05375a',
                          )}
                          size={widthScreen * 0.06}
                        />
                        <TextInput
                          placeholder={xmlChangeText(
                            LoginXmlParser,
                            'InputPhone.PlaceHolder',
                            'Nhập số điện thoại của bạn',
                          )}
                          placeholderTextColor="#999999"
                          style={ScreenLoginCss.textInput}
                          autoCapitalize="none"
                          onChangeText={handleChange('Phone')}
                          onBlur={() => setFieldTouched('Phone')}
                          value={values.Phone}
                          keyboardType="numeric"
                        />
                        {!errors.Phone && touched.Phone ? (
                          <Animated.View>
                            <Ionicons
                              name={xmlChangeText(
                                LoginXmlParser,
                                'InputPhone.IconValid',
                                'checkmark-circle-outline',
                              )}
                              color={xmlChangeText(
                                LoginXmlParser,
                                'InputPhone.IconValidColor',
                                '#05375a',
                              )}
                              size={widthScreen * 0.06}
                            />
                          </Animated.View>
                        ) : null}
                      </View>
                      {touched.Phone && errors.Phone && (
                        <Text
                          style={{
                            color: 'red',
                            paddingVertical: 6,
                            fontSize: widthScreen * 0.04,
                            fontWeight: '600',
                          }}>
                          {errors.Phone}
                        </Text>
                      )}
                      <Text
                        style={[ScreenLoginCss.text_footer, {marginTop: 12}]}>
                        Mật khẩu
                      </Text>
                      <View style={ScreenLoginCss.action}>
                        <Ionicons
                          name={xmlChangeText(
                            LoginXmlParser,
                            'InputPassword.Icon',
                            'lock-closed-outline',
                          )}
                          color={xmlChangeText(
                            LoginXmlParser,
                            'InputPassword.IconColor',
                            '#05375a',
                          )}
                          size={widthScreen * 0.06}
                        />
                        <TextInput
                          placeholder={xmlChangeText(
                            LoginXmlParser,
                            'InputPassword.PlaceHolder',
                            'Nhập mật khẩu',
                          )}
                          style={ScreenLoginCss.textInput}
                          autoCapitalize="none"
                          placeholderTextColor="#999999"
                          secureTextEntry={data.secureTextEntry}
                          onChangeText={handleChange('passWord')}
                          onBlur={() => setFieldTouched('passWord')}
                          value={values.passWord}
                        />
                        <TouchableOpacity onPress={handlesetShowEye}>
                          {!data.secureTextEntry ? (
                            <Ionicons
                              name={xmlChangeText(
                                LoginXmlParser,
                                'InputPassword.Icon2',
                                'eye-outline',
                              )}
                              size={widthScreen * 0.06}
                              color={xmlChangeText(
                                LoginXmlParser,
                                'InputPassword.Icon2Color',
                                '#999999',
                              )}
                            />
                          ) : (
                            <Ionicons
                              name={xmlChangeText(
                                LoginXmlParser,
                                'InputPassword.Icon3',
                                'eye-off-outline',
                              )}
                              size={widthScreen * 0.06}
                              color={xmlChangeText(
                                LoginXmlParser,
                                'InputPassword.Icon3Color',
                                '#999999',
                              )}
                            />
                          )}
                        </TouchableOpacity>
                      </View>
                      {touched.passWord && errors.passWord && (
                        <Text
                          style={{
                            color: 'red',
                            paddingTop: 8,
                            fontSize: widthScreen * 0.04,
                            fontWeight: '600',
                          }}>
                          {errors.passWord}
                        </Text>
                      )}
                      <TouchableOpacity
                        disabled={!isValid}
                        onPress={() => handleSubmit()}
                        style={ScreenLoginCss.button}>
                        <LinearGradient
                          colors={
                            isValid
                              ? ['#FF9900', '#FF9900']
                              : ['#808080', '#808080']
                          }
                          style={ScreenLoginCss.signIn}>
                          <Text
                            style={[ScreenLoginCss.TextSign, {color: '#fff'}]}>
                            {xmlChangeText(
                              LoginXmlParser,
                              'ButtonSubmitText',
                              'Đăng Nhập',
                            )}
                          </Text>
                        </LinearGradient>
                      </TouchableOpacity>
                    </View>
                  )}
                </Formik>
                <View style={ScreenLoginCss.ContainerSentInfo}>
                  <Text
                    style={[
                      ScreenLoginCss.TextAccountNot,
                      {
                        color: xmlChangeText(
                          LoginXmlParser,
                          'TextContactColor',
                          '#999999',
                        ),
                      },
                    ]}>
                    {xmlChangeText(
                      LoginXmlParser,
                      "TextContact",
                      'Bạn chưa có tài khoản? ',
                    )}
                  </Text>
                  <TouchableOpacity onPress={handelSinginUp}>
                    <Text
                      style={[
                        ScreenLoginCss.SentInfo,
                        {
                          color: xmlChangeText(
                            LoginXmlParser,
                            'TextContact1Color',
                            '#05375A',
                          ),
                        },
                      ]}>
                      {xmlChangeText(
                        LoginXmlParser,
                        'TextContact1',
                        'Gửi liên hệ',
                      )}
                    </Text>
                  </TouchableOpacity>
                </View>
              </ScrollView>
              <View style={ScreenLoginCss.ContainerPhoneCall}>
                <View style={ScreenLoginCss.Alan}></View>
                <View style={ScreenLoginCss.ContainerPhoneCallText}>
                  <MaterialIcons
                    name={xmlChangeText(
                      LoginXmlParser,
                      'FooterSupportIcon',
                      'phone',
                    )}
                    color={xmlChangeText(
                      LoginXmlParser,
                      'colorSupport1',
                      '#888888',
                    )}
                    size={widthScreen * 0.05}
                  />
                  <Text
                    style={[
                      ScreenLoginCss.TextHelp,
                      {
                        color: xmlChangeText(
                          LoginXmlParser,
                          'FooterSupportTextColor',
                          '#888888',
                        ),
                      },
                    ]}>
                    {xmlChangeText(
                      LoginXmlParser,
                      'FooterSupportText' + ':',
                      'Hỗ trợ : ',
                    )}
                  </Text>
                  <TouchableOpacity onPress={handleLinkingCall}>
                    <Text
                      style={[
                        ScreenLoginCss.TextPhoneNumber,
                        {
                          color: xmlChangeText(
                            LoginXmlParser,
                            'NumberPhoneColor',
                            '#05375A',
                          ),
                        },
                      ]}>
                      {xmlChangeText(
                        LoginXmlParser,
                        'FooterSupportNumberPhone',
                        '02862651411',
                      )}
                    </Text>
                  </TouchableOpacity>
                </View>
                <View style={ScreenLoginCss.Alan}></View>
              </View>
            </View>
          </View>
        </TouchableWithoutFeedback>
      </KeyboardAvoidingView>
      {isLoading ? <LoadingComponent /> : ''}
      {isNotification ? <ModalNotification /> : ''}
    </SafeAreaView>
  );
};

export default ScreenLogin;
