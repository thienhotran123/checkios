import {Platform, StyleSheet} from 'react-native';
import {heightWindown, widthScreen} from '../../../../Utils/Styles';
import {verticalScale} from '../../../../Utils/Reponsive';
export const ScreenLoginCss = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FF9900',
  },
  containerBody: {
    flex: 1,
  },
  header: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: 16,
    paddingBottom: 30,
  },
  footer: {
    flex: 4,
    backgroundColor: '#fff',
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    paddingHorizontal: 20,
    paddingVertical: 20,
  },
  text_header: {
    color: '#fff',
    fontSize: widthScreen * 0.08,
    fontWeight: '700',
  },
  text_footer: {
    color: '#05375A',
    fontSize: widthScreen * 0.04,
    fontWeight: '600',
  },
  action: {
    flexDirection: 'row',
    marginTop: 20,
    borderBottomColor: '#f2f2f2',
    paddingBottom: 2,
    borderBottomWidth: 2,
  },
  textInput: {
    flex: 1,
    marginTop: Platform.OS == 'ios' ? 0 : -12,
    paddingLeft: 10,
    color: '#05375a',
    fontSize: widthScreen * 0.036,
  },
  button: {
    alignItems: 'flex-end',
    marginTop: heightWindown > 540 ? 30 : 20,
  },
  signIn: {
    width: '100%',
    height: verticalScale(60),
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 6,
  },
  TextSign: {
    fontSize: widthScreen * 0.05,
    fontWeight: '700',
  },
  TextPhoneNumber: {
    fontSize: widthScreen * 0.04,
    fontWeight: '700',
    marginLeft: 3,
  },
  TextHelp: {
    fontSize: widthScreen * 0.04,
    fontWeight: '700',
    marginLeft: 3,
  },
  ContainerPhoneCallText: {
    paddingHorizontal: 10,
    flexDirection: 'row',
    alignItems: 'center',
  },
  ContainerSentInfo: {
    flexDirection: 'row',
    justifyContent: 'center',
    marginTop: 20,
  },
  TextAccountNot: {
    fontSize: widthScreen * 0.046,
    fontWeight: '600',
    color: '#999999',
  },
  SentInfo: {
    fontSize: widthScreen * 0.046,
    fontWeight: '600',
  },
  Alan: {borderColor: '#888888', borderWidth: 1, flex: 1},
  ContainerPhoneCall: {
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
  },
});
