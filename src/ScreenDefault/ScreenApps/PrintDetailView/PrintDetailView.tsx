import {
  Image,
  ScrollView,
  Text,
  TouchableOpacity,
  View,
  NativeModules,
  Platform,
  TextProps,
} from 'react-native';
import React, {useEffect, useRef, useState} from 'react';
import {NetPrinter} from 'react-native-thermal-receipt-printer-image-qr';
import {PrintDetailViewCss} from './PrintDetailViewCss';
import {captureRef} from 'react-native-view-shot';
import Ionicons from 'react-native-vector-icons/Ionicons';
import {colors, widthScreen} from '../../../Utils/Styles';
import {useDispatch, useSelector} from 'react-redux';
import {SafeAreaView} from 'react-native';
import {RootState} from '../../../Redux/Storage';
import {
  AuthenticationError,
  getDateTime,
  xmlChangeText,
} from '../../../Utils/Helper';
import {ProductItem} from '../../../Utils/interface/interface';
import {
  ItemPrintDetailView,
  LoadingComponent,
  ModalNotification,
} from '../../../Components/Components';
import {SetShowModalIsNotification} from '../../../Redux/Slide';
import {GetFooterBill} from '../../../Utils/GetApi';
import {SetDataDataFooter} from '../../../Redux/order';
import {Success} from '../../../Utils/Enums';
import {
  CantConnectIP,
  LoginAgain,
  PrintDetailSuccess,
} from '../../../Utils/MessageNotification';
import {AuthContext} from '../../../DefaultLayout/DefaultLayoutScreen';
import { RoundOff, TotalMoney, TotalMoneyVat, TotalQuantity, TotalVat } from '../../../Services/Services';
interface dataRoute {
  nameDetail: string;
  item: ProductItem[];
  tableName:  string;
  discount: number;
  NameCustomer: string;
  PhoneCustomer: string;
  PointCustomer:  string;
}
const PrintDetailView = ({navigation, route}) => {
  const date = new Date();
  let RegexString = /^[—-]+$/;
  let RegexStringEqual = /^=*$/;
  const dispatch = useDispatch();
  const {signOut} = React.useContext(AuthContext);
  const [dataRoute, setDataRoute] = useState<dataRoute>({
    nameDetail: '',
    item: [],
    tableName: '',
    discount: 0,
    NameCustomer: '',
    PhoneCustomer: '',
    PointCustomer: '',
  });
  const viewShotRef = useRef<View>(null);
  const scrollViewRef = useRef<ScrollView>(null);
  const [isLoading, SetIsLoading] = useState(false);
  const isNotification = useSelector((state: RootState) => state.counter.isNotification);
  const dataFooter = useSelector((state: RootState) => state.item.DataFooter);
  const UserName = useSelector((state: RootState) => state.counter.UserName);
  const DataSetting = useSelector((state: RootState) => state.item.DataSetting);
  const XmlProperties = useSelector(
    (state: RootState) => state.item.XmlProperties,
  );
  const PrintDetailXml = XmlProperties !== null ? XmlProperties[5] : null;
  const [CheckUrl, setCheckUrl] = useState<boolean>(false);
  const [checkUrlFooter, setCheckUrlFooter] = useState<boolean>(false);
  useEffect(() => {
    setDataRoute(route.params);
  }, []);
  useEffect(() => {
    const GetFooter = async () => {
      GetFooterBill({token: UserName.token})
        .then(data => {
          if (AuthenticationError(data.message  ,data.status_code)) {
            signOut(LoginAgain);
            return;
          }
          if (data.status_code == Success) {
            dispatch(
              SetDataDataFooter({
                content: data.footer.content,
                image: data.footer.image,
              }),
            );
          } else {
            dispatch(SetDataDataFooter({content: '', image: null}));
          }
        })
        .catch(er => {
          dispatch(SetDataDataFooter({content: '', image: null}));
        });
    };
    GetFooter();
  }, []);
  useEffect(() => {
    const CheckImage=()=>{
      fetch(`${UserName.logo}`)
      .then(data => {
        if (data.status == Success) {
          setCheckUrl(true);
        } else {
          setCheckUrl(false);
        }
      })
      .catch(er => setCheckUrl(false));
    }
    CheckImage()
  }, []);
  useEffect(()=>{
    const CheckImage = ()=>{
      fetch(`${dataFooter.image}`)
      .then(data => {
        if (data.status == Success) {
          setCheckUrlFooter(true);
        } else {
          setCheckUrlFooter(false);
        }
      })
      .catch(er => setCheckUrlFooter(false));
    }
    CheckImage()
  },[dataFooter])
  const array =
    dataFooter.content !== '' &&
    dataFooter !== null &&
    dataFooter.content !== null
      ? dataFooter.content.split('\n')
      : '';
  const dataArrayPush: Array<React.ReactElement<TextProps, string>> = [];
  if (array !== '') {
    for (let i = 0; i < array.length; i++) {
      if (RegexString.test(array[i]) == true && array[i] !== '') {
        let TotalLength = 0;
        for (let j = 0; j < array[i].length; j++) {
          TotalLength += array[i][j] == '—' ? 2 : 1;
        }
        TotalLength >= 3
          ? dataArrayPush.push(
              <Text
                style={PrintDetailViewCss.AlanCss}
                numberOfLines={1}
                ellipsizeMode="clip">
                {Platform.OS == 'android'
                  ? '-------------------------------------------------------'
                  : '-------------------------------------------------------'}
              </Text>,
            )
          : dataArrayPush.push(
              <Text
                style={{
                  textAlign: 'center',
                  color: '#333',
                  fontWeight: '700',
                  fontSize: 18,
                }}>
                {TotalLength == 2 ? '--' : '-'}
              </Text>,
            );
      } else if (array[i] == ' ') {
        dataArrayPush.push(<Text></Text>);
      } else if (RegexStringEqual.test(array[i]) == true && array[i] !== '') {
        dataArrayPush.push(
          <Text
            style={PrintDetailViewCss.AlanCss}
            numberOfLines={1}
            ellipsizeMode="clip">
            {Platform.OS == 'ios'
              ? `=====================================`
              : `============================================`}
          </Text>,
        );
      } else {
        dataArrayPush.push(
          <Text
            style={{
              textAlign: 'center',
              color: '#333',
              fontWeight: '700',
              fontSize: 18,
            }}>
            {array[i]}
          </Text>,
        );
      }
    }
    dataArrayPush.push(<Text></Text>);
  } else {
    dataArrayPush.push(
      <View style={{flex: 1}}>
        <Text
          style={PrintDetailViewCss.AlanCss}
          numberOfLines={1}
          ellipsizeMode="clip">
          {Platform.OS == 'android'
            ? '-------------------------------------------------------'
            : '-------------------------------------------------------'}
        </Text>
      </View>,
    );
    dataArrayPush.push(
      <View style={{flex: 1, marginTop: 10}}>
        <Text
          style={{
            textAlign: 'center',
            color: '#333',
            fontWeight: '700',
            fontSize: 18,
          }}>
          Xin cảm ơn quý khách, hẹn gặp lại!
        </Text>
      </View>,
    );
  }
  const handleIn = () => {
    if (scrollViewRef.current) {
      SetIsLoading(true);
      NativeModules.UIManager.measure(
        scrollViewRef.current?.getInnerViewNode(),
        (x: number, y: number, width: number, height: number) => {
          captureRef(viewShotRef, {
            result: 'base64',
            format: 'png',
            quality: 0.8,
          }).then(uri => {
            if (Platform.OS == 'ios') {
              NetPrinter.init()
                .then(() => {
                  NetPrinter.connectPrinter(DataSetting.PrintIp, 9100)
                    .then(() => {
                      NetPrinter.printImageBase64(uri, {
                        imageHeight: height + 200,
                        imageWidth: 600,
                        cut: true,
                        beep: false,
                      });
                    })
                    .then(() => {
                      dispatch(
                        SetShowModalIsNotification({
                          description: PrintDetailSuccess,
                          showValue: true,
                          functionConfirm: '',
                          functionCancel: 'handleCloseModalNotification',
                          valueBtnConfirm: '',
                          valueBtnCancel: '',
                        }),
                      );
                      SetIsLoading(false);
                    })
                    .catch(() => {
                      SetIsLoading(false);
                      dispatch(
                        SetShowModalIsNotification({
                          description: CantConnectIP,
                          showValue: true,
                          functionConfirm: '',
                          functionCancel: 'handleCloseModalNotification',
                          valueBtnConfirm: '',
                          valueBtnCancel: '',
                        }),
                      );
                    });
                })
                .catch(() => {
                  SetIsLoading(false);
                  dispatch(
                    SetShowModalIsNotification({
                      description: CantConnectIP,
                      showValue: true,
                      functionConfirm: '',
                      functionCancel: 'handleCloseModalNotification',
                      valueBtnConfirm: '',
                      valueBtnCancel: '',
                    }),
                  );
                });
            } else {
              const {PingModule} = NativeModules;
              PingModule.PingAddress(DataSetting.PrintIp, (value: string) => {
                if (value == 'Success') {
                  NetPrinter.init()
                    .then(() => {
                      NetPrinter.connectPrinter(DataSetting.PrintIp, 9100)
                        .then(() => {
                          NetPrinter.printImageBase64(uri, {
                            imageHeight: height + 100,
                            imageWidth: 576,
                          });
                        })
                        .then(() => SetIsLoading(false))
                        .then(() =>
                          dispatch(
                            SetShowModalIsNotification({
                              description: PrintDetailSuccess,
                              showValue: true,
                              functionConfirm: '',
                              functionCancel: 'handleCloseModalNotification',
                              valueBtnConfirm: '',
                              valueBtnCancel: '',
                            }),
                          ),
                        )
                        .catch(() => {
                          SetIsLoading(false);
                          dispatch(
                            SetShowModalIsNotification({
                              description: CantConnectIP,
                              showValue: true,
                              functionConfirm: '',
                              functionCancel: 'handleCloseModalNotification',
                              valueBtnConfirm: '',
                              valueBtnCancel: '',
                            }),
                          );
                        });
                    })
                    .catch(() => {
                      SetIsLoading(false);
                      dispatch(
                        SetShowModalIsNotification({
                          description: CantConnectIP,
                          showValue: true,
                          functionConfirm: '',
                          functionCancel: 'handleCloseModalNotification',
                          valueBtnConfirm: '',
                          valueBtnCancel: '',
                        }),
                      );
                    });
                  return;
                }
                if (value == 'Failure') {
                  dispatch(
                    SetShowModalIsNotification({
                      description: CantConnectIP,
                      showValue: true,
                      functionConfirm: '',
                      functionCancel: 'handleCloseModalNotification',
                      valueBtnConfirm: '',
                      valueBtnCancel: '',
                    }),
                  );
                  SetIsLoading(false);
                  return;
                }
              });
            }
          });
        },
      );
    }
  };
  const handleCloseCartOrder = () => {
    navigation.goBack();
  };
  return (
    <SafeAreaView
      style={{
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#ffffff',
      }}>
      <View style={PrintDetailViewCss.ContainerHeader}>
        <View style={{flexDirection: 'row', alignItems: 'center'}}>
          <TouchableOpacity onPress={handleCloseCartOrder}>
            <Ionicons
              name={xmlChangeText(PrintDetailXml,'IconNavigation', 'chevron-back-outline')}
              size={widthScreen * 0.14}
              color={xmlChangeText(PrintDetailXml,'IconNavigationColor', '#0066CC')}
            />
          </TouchableOpacity>
          <Text style={PrintDetailViewCss.HeaderText}>Chi Tiết Hoá Đơn</Text>
        </View>
        <TouchableOpacity style={{}} onPress={handleIn}>
          <Ionicons
            name={xmlChangeText(PrintDetailXml,'IconPrinter', 'print-outline')}
            size={widthScreen * 0.11}
            color={xmlChangeText(PrintDetailXml,'ColorIconPrinter', '#0066CC')}
          />
        </TouchableOpacity>
      </View>
      <ScrollView
        style={{flex: 1, width: 380, paddingHorizontal: 10}}
        ref={scrollViewRef}
        showsVerticalScrollIndicator={false}>
        <View
          style={{backgroundColor: '#fff'}}
          collapsable={false}
          ref={viewShotRef}>
          <View
            style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
            {UserName.logo !== null &&
            UserName.logo !== undefined &&
            CheckUrl ? (
              <Image
                style={{width:xmlChangeText(PrintDetailXml , 'WidthImageHeader' ,120), height: xmlChangeText(PrintDetailXml , 'HeightImageFooter' ,120)}}
                source={{uri: `${UserName.logo}`}}
              />
            ) : (
              <Text></Text>
            )}
          </View>
          <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <View style={{flex: 1}}>
              <Text style={PrintDetailViewCss.NameCompanyText}>
                {UserName.storename !== undefined &&
                UserName.storename !== null &&
                UserName.storename !== 'null'
                  ? UserName.storename
                  : 'Delta Pos'}
              </Text>
              <Text style={PrintDetailViewCss.AddressCompanyText}>
                {UserName.addresscompany !== undefined &&
                UserName.addresscompany !== null &&
                UserName.addresscompany !== 'null'
                  ? UserName.addresscompany
                  : ''}
              </Text>
              <Text style={PrintDetailViewCss.PhoneNumberConpanyText}>
                {UserName.phonenumbercompany !== undefined &&
                UserName.phonenumbercompany !== null &&
                UserName.phonenumbercompany !== 'null'
                  ? UserName.phonenumbercompany
                  : ''}
              </Text>
            </View>
          </View>
          <View style={PrintDetailViewCss.ContainerTextPaymentHD}>
            <Text style={PrintDetailViewCss.ContainerTextPaymentHDText}>
              HOÁ ĐƠN THANH TOÁN
            </Text>
          </View>
          <View style={{flex: 1, marginTop: 18}}>
              <View style={{flexDirection: 'row', width: '100%'}}>
                <Text style={PrintDetailViewCss.HeaderInfoCssTextLable}>
                  Bàn:
                </Text>
                <Text style={PrintDetailViewCss.HeaderInfoCssText}>
                  {dataRoute.tableName}
                </Text>
              </View>
            <View style={{flexDirection: 'row', marginTop: 4}}>
              <Text style={PrintDetailViewCss.HeaderInfoCssTextLable}>
                Ngày:
              </Text>
              <Text style={PrintDetailViewCss.HeaderInfoCssText}>
                {getDateTime(date)}
              </Text>
            </View>
            <View style={{flexDirection: 'row', marginTop: 4}}>
              <Text style={PrintDetailViewCss.HeaderInfoCssTextLable}>
                Nhân viên:
              </Text>
              <Text style={PrintDetailViewCss.HeaderInfoCssText}>
                {UserName.name}
              </Text>
            </View>
            {dataRoute.NameCustomer !== '' ? (
              <View style={{flexDirection: 'row', marginTop: 4}}>
                <Text style={PrintDetailViewCss.HeaderInfoCssTextLable}>
                  Khách hàng:
                </Text>
                <Text style={PrintDetailViewCss.HeaderInfoCssText}>
                  {dataRoute.NameCustomer}
                </Text>
              </View>
            ) : (
              ''
            )}
            {dataRoute.PointCustomer !== '' ? (
              <View style={{flexDirection: 'row', marginTop: 4}}>
                <Text style={PrintDetailViewCss.HeaderInfoCssTextLable}>
                  Điểm:
                </Text>
                <Text style={PrintDetailViewCss.HeaderInfoCssText}>
                  {dataRoute.PointCustomer}
                </Text>
              </View>
            ) : (
              ''
            )}
          </View>
          <View style={{flex: 1}}>
            <Text
              style={PrintDetailViewCss.AlanCss}
              numberOfLines={1}
              ellipsizeMode="clip">
              {Platform.OS == 'android'
                ? '-------------------------------------------------------'
                : '-------------------------------------------------------'}
            </Text>
          </View>
          <View style={{flex: 1}}>
            <View style={PrintDetailViewCss.HeaderBillContainer}>
              <View style={{width: '30%'}}>
                <Text style={PrintDetailViewCss.HeaderBillTilte}>Tên Món</Text>
              </View>
              <View style={{width: '10%'}}>
                <Text style={PrintDetailViewCss.HeaderBill}>SL</Text>
              </View>
              <View style={{width: '25%'}}>
                <Text style={PrintDetailViewCss.HeaderBill}>Đơn Giá</Text>
              </View>
              <View style={{width: '10%'}}>
                <Text style={PrintDetailViewCss.HeaderBill}>VAT</Text>
              </View>
              <View style={{width: '25%'}}>
                <Text style={PrintDetailViewCss.HeaderBillTT}>T.Tiền</Text>
              </View>
            </View>
            <View style={{flex: 1}}>
              <Text
                style={PrintDetailViewCss.AlanCss}
                numberOfLines={1}
                ellipsizeMode="clip">
                {Platform.OS == 'android'
                  ? '-------------------------------------------------------'
                  : '-------------------------------------------------------'}
              </Text>
              <View style={{flex: 1}}>
                {dataRoute.item.map((item, index: number) => (
                  <ItemPrintDetailView key={index} item={item} />
                ))}
                <Text
                  style={PrintDetailViewCss.AlanCss}
                  numberOfLines={1}
                  ellipsizeMode="clip">
                  {Platform.OS == 'android'
                    ? '-------------------------------------------------------'
                    : '-------------------------------------------------------'}
                </Text>
              </View>
              <View style={{flex: 1}}>
                <View style={PrintDetailViewCss.BodyFooterContainer}>
                  <Text style={PrintDetailViewCss.BodyFooterText}>
                    Tổng số lượng:
                  </Text>
                  <Text style={PrintDetailViewCss.BodyFooterTextName}>
                    {TotalQuantity(dataRoute.item)}
                  </Text>
                </View>
                <View style={PrintDetailViewCss.BodyFooterContainer}>
                  <Text style={PrintDetailViewCss.BodyFooterText}>
                    Tổng VAT:
                  </Text>
                  <Text style={PrintDetailViewCss.BodyFooterTextName}>
                    {RoundOff(TotalVat(dataRoute.item))} VND
                  </Text>
                </View>
                <View style={PrintDetailViewCss.BodyFooterContainer}>
                  <Text style={PrintDetailViewCss.BodyFooterText}>
                    Tổng tiền trước thuế:
                  </Text>
                  <Text style={PrintDetailViewCss.BodyFooterTextName}>
                    {RoundOff(TotalMoney(dataRoute.item))} VND
                  </Text>
                </View>
                {dataRoute.discount > 0 ? (
                  <View style={PrintDetailViewCss.BodyFooterContainer}>
                    <Text style={PrintDetailViewCss.BodyFooterText}>
                      Giảm giá:
                    </Text>
                    <Text style={PrintDetailViewCss.BodyFooterTextName}>
                      {RoundOff(dataRoute.discount)} VND
                    </Text>
                  </View>
                ) : (
                  ''
                )}
                <View style={PrintDetailViewCss.BodyFooterContainer}>
                  <Text style={PrintDetailViewCss.BodyFooterText}>
                    Khách hàng phải trả:
                  </Text>
                  <Text style={PrintDetailViewCss.BodyFooterTextName}>
                    {RoundOff(
                      TotalMoneyVat(dataRoute.item) - dataRoute.discount,
                    )}{' '}
                    VND
                  </Text>
                </View>
              </View>
              <View style={{width: '100%', marginTop: 20}}>
                {dataFooter.image !== null &&
                dataFooter.image !== undefined &&
                checkUrlFooter ? (
                  <View
                    style={{
                      flex: 1,
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}>
                    <Image
                      style={{width: xmlChangeText(PrintDetailXml , 'WidthImageFooter' ,120), height: xmlChangeText(PrintDetailXml , 'HeightImageFooter' ,120)}}
                      source={{uri: `${dataFooter.image}`}}
                    />
                  </View>
                ) : (
                  ''
                )}

                <View style={{width: '100%', marginTop: 20}}>
                  {dataArrayPush.map((item, index) => (
                    <View key={index} style={{width: '100%'}}>
                      {item}
                    </View>
                  ))}
                </View>
              </View>
              <View style={PrintDetailViewCss.FooterTextPoweredContainer}>
                <Text style={PrintDetailViewCss.FooterTextPowered}>
                  Powered by Delta Pos
                </Text>
              </View>
              <View></View>
            </View>
          </View>
        </View>
      </ScrollView>
      {isLoading ? <LoadingComponent /> : ''}
      {isNotification ? <ModalNotification /> : ''}
    </SafeAreaView>
  );
};

export default React.memo(PrintDetailView);
