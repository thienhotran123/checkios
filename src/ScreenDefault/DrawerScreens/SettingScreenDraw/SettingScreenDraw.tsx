import {
  SafeAreaView,
  ScrollView,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import React from 'react';
import Ionicons from 'react-native-vector-icons/Ionicons';
import {colors, widthScreen} from '../../../Utils/Styles';
import {useDispatch, useSelector} from 'react-redux';
import {ModalNotification} from '../../../Components/Components';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {SetDataSettings} from '../../../Redux/order';
import {
  SetShowModalIsNotification,
} from '../../../Redux/Slide';
import {Formik} from 'formik';
import * as Yup from 'yup';
import {scale, verticalScale} from '../../../Utils/Reponsive';
import {RootState} from '../../../Redux/Storage';
import DeviceInfo from 'react-native-device-info';
import {
  SaveChangeSuccess,
  errorNotification,
} from '../../../Utils/MessageNotification';
const SettingSchema = Yup.object().shape({
  DataPrinter: Yup.string()
    .required('Vui lòng nhập địa chỉ ip của máy in')
    .matches(/^[0-9.]+$/, 'Địa chỉ ip không hợp lệ'),
});
const SettingScreenDraw = ({navigation}) => {
  const isNotification = useSelector(
    (state: RootState) => state.counter.isNotification,
  );
  const DataSetting = useSelector((state: RootState) => state.item.DataSetting);
  const dispatch = useDispatch();
  const handleOpenMenu = () => {
    navigation.goBack();
  };
  const handleSaveData = async data => {
    await AsyncStorage.setItem(
      'DataSettings',
      JSON.stringify({
        PrintIp:
          data.DataPrinter !== null || data.DataPrinter !== undefined
            ? data.DataPrinter
            : DataSetting.PrintIp,
      }),
    )
      .then(() => dispatch(SetDataSettings({PrintIp: data.DataPrinter})))
      .then(() =>
        dispatch(
          SetShowModalIsNotification({
            description: SaveChangeSuccess,
            showValue: true,
            functionConfirm: '',
            functionCancel: 'handleCloseModalNotification',
            valueBtnConfirm: '',
            valueBtnCancel: '',
          }),
        ),
      )
      .catch(er =>
        dispatch(
          SetShowModalIsNotification({
            description: errorNotification,
            showValue: true,
            functionConfirm: '',
            functionCancel: 'handleCloseModalNotification',
            valueBtnConfirm: '',
            valueBtnCancel: '',
          }),
        ),
      );
  };
  return (
    <SafeAreaView style={{flex: 1, backgroundColor: '#fff'}}>
      <Formik
        initialValues={{
          DataPrinter: DataSetting.PrintIp,
        }}
        validationSchema={SettingSchema}
        onSubmit={e => handleSaveData(e)}>
        {({
          handleChange,
          handleSubmit,
          values,
          errors,
          touched,
          isValid,
          setFieldTouched,
        }) => (
          <View style={{flex: 1}}>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                padding: 10,
              }}>
              <TouchableOpacity onPress={handleOpenMenu}>
                <Ionicons
                  name="chevron-back-outline"
                  size={widthScreen * 0.12}
                  color={colors.IconColor}
                />
              </TouchableOpacity>
              <Text
                style={{
                  fontSize: widthScreen * 0.05,
                  fontWeight: '700',
                  color: '#333',
                  marginLeft: 10,
                }}>
                Thiết Lập
              </Text>
            </View>
            <ScrollView
              style={{flex: 1, padding: 10}}
              showsVerticalScrollIndicator={false}>
              <View style={{marginTop: 10}}>
                <Text
                  style={{
                    padding: 6,
                    fontWeight: '700',
                    color: '#555555',
                    fontSize: widthScreen * 0.036,
                  }}>
                  Nhập IP Của Máy In
                </Text>
                <View
                  style={{
                    flexDirection: 'column',
                  }}>
                  <TextInput
                    placeholderTextColor={'#9E9E9E'}
                    onChangeText={handleChange('DataPrinter')}
                    onBlur={() => setFieldTouched('DataPrinter')}
                    value={values.DataPrinter}
                    style={{
                      width: '50%',
                      fontSize: widthScreen * 0.034,
                      height: verticalScale(52),
                      borderWidth: DeviceInfo.isTablet() ? 2 : 2,
                      borderRadius: 4,
                      paddingHorizontal: 10,
                      color: '#333',
                      borderColor: '#888888',
                    }}
                  />
                  {touched.DataPrinter && errors.DataPrinter && (
                    <Text
                      style={{
                        color: 'red',
                        paddingVertical: 6,
                        fontSize: widthScreen * 0.036,
                        padding: 6,
                        fontWeight: '700',
                      }}>
                      {errors.DataPrinter}
                    </Text>
                  )}
                </View>
              </View>
            </ScrollView>
            <View
              style={{
                paddingHorizontal: 10,
                paddingVertical: 10,
                justifyContent: 'flex-end',
                alignItems: 'flex-end',
              }}>
              <TouchableOpacity
                disabled={!isValid}
                onPress={() => handleSubmit()}
                style={{
                  backgroundColor: isValid ? colors.btnColor : '#808080',
                  width: scale(120),
                  height: verticalScale(48),
                  borderRadius: 6,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Text
                  style={{
                    color: '#fff',
                    fontWeight: '600',
                    fontSize: widthScreen * 0.04,
                  }}>
                  Lưu lại
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        )}
      </Formik>
      {isNotification ? <ModalNotification /> : ''}
    </SafeAreaView>
  );
};
export default React.memo(SettingScreenDraw);
