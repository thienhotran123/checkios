import {
  ActivityIndicator,
  Text,
  TextInput,
  View,
  TouchableOpacity,
} from 'react-native';
import React, {useEffect, useState} from 'react';
import {SafeAreaView} from 'react-native';
import {HistoryPaymentCss} from './HistoryPaymentCss';
import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import {colors, widthScreen} from '../../../Utils/Styles';
import {
  DetailPaymentHistory,
  HistoryPaymentItem,
  ModalNotification,
} from '../../../Components/Components';
import {useDispatch, useSelector} from 'react-redux';
import {FlatList} from 'react-native';
import {RootState} from '../../../Redux/Storage';
import {GetAllPaymentUser} from '../../../Utils/GetApi';
import {getYear, getMonth, getDay, xmlChangeText} from '../../../Utils/Helper';
import {SetShowModalIsNotification} from '../../../Redux/Slide';
import {ResponsiveMessage} from '../../../Utils/MessageApi';
import DatePicker from 'react-native-date-picker';
import {Success} from '../../../Utils/Enums';
import {
  DayBeginNotThanDayEnd,
  errorNotification,
} from '../../../Utils/MessageNotification';
const HistoryPayment = ({navigation}) => {
  const dispatch = useDispatch();
  const date = new Date();
  const XmlProperties = useSelector((state: RootState) => state.item.XmlProperties);
  const HistoryPaymentXml = XmlProperties !== null ? XmlProperties[6] : null;
  const [dateValue, setDateValue] = useState<{value: string; error: string}>({
    value: `${getYear(date)}/${getMonth(date)}/${getDay(date)}`,
    error: '',
  });
  const [startDate, setStartDate] = useState<{value: string; error: string}>({
    value: `${getYear(date)}/${getMonth(date)}/${getDay(date)}`,
    error: '',
  });
  const [endDate, setEndDate] = useState<{value: string; error: string}>({
    value: `${getYear(date)}/${getMonth(date)}/${getDay(date)}`,
    error: '',
  });
  const UserName = useSelector((state: RootState) => state.counter.UserName);
  const ShowDetailPaymentHistory = useSelector(
    (state: RootState) => state.counter.ShowDetailPaymentHistory,
  );
  const [showCalendar, setShowCalendar] = useState<boolean>(false);
  const [paymentHistory, setPaymentHistory] = useState<any[]>([]);
  const [changeHistoryFilter, setChangeHistoryFilter] = useState<number>(0);
  const [DateValid, setDateValid] = useState<number>(0);
  const [Loading, setLoading] = useState<boolean>(false);
  const [SearchCheck, setSearchCheck] = useState<boolean>(false);
  const isNotification = useSelector(
    (state: RootState) => state.counter.isNotification,
  );
  const [reRenderFilterDate, setReRenderFilterDate] = useState<boolean>(false);
  useEffect(() => {
    const GetPaymentHistory = () => {
      if (changeHistoryFilter == 0) {
        filterDate();
        return;
      }
      if (changeHistoryFilter == 1) {
        filterDateStartEnd();
        return;
      }
    };
    GetPaymentHistory();
  }, [reRenderFilterDate]);
  //filter date
  const filterDate = () => {
    setLoading(true);
    const convertDate = dateValue.value.replace(/\//g, '-');
    GetAllPaymentUser({
      token: UserName.token,
      data: {user_id: UserName.id, date: `${convertDate}`},
    })
      .then(data => {
        if (data.status_code == Success) {
          const data2 = data.payment.filter(item=>item.status!==-1).reverse();
          setPaymentHistory(data2);
        } else {
          setPaymentHistory([]);
        }
      })
      .catch(e => {
        dispatch(
          SetShowModalIsNotification({
            description: errorNotification,
            showValue: true,
            functionConfirm: '',
            functionCancel: 'handleCloseModalNotification',
            valueBtnConfirm: '',
            valueBtnCancel: '',
          }),
        );
      })
      .finally(() => setLoading(false));
  };
  //filter startDate and endDate
  const filterDateStartEnd = () => {
    setLoading(true);
    const convertStartDate = startDate.value.replace(/\//g, '-');
    const convertEndDate = endDate.value.replace(/\//g, '-');
    GetAllPaymentUser({
      token: UserName.token,
      data: {
        user_id: UserName.id,
        startdate: `${convertStartDate}`,
        enddate: `${convertEndDate}`,
      },
    })
      .then(data => {
        if (data.message == ResponsiveMessage.StartDateNotThanEndDate) {
          dispatch(
            SetShowModalIsNotification({
              description: DayBeginNotThanDayEnd,
              showValue: true,
              functionConfirm: '',
              functionCancel: 'handleCloseModalNotification',
              valueBtnConfirm: '',
              valueBtnCancel: '',
            }),
          );
          setPaymentHistory([]);
          return;
        }
        if (data.status_code == Success) {
          const dataPaymentHistory = data.payment.filter(item=>item.status !==-1).reverse();
          setPaymentHistory(dataPaymentHistory);
        } else {
          setPaymentHistory([]);
        }
      })
      .catch(e => {
        dispatch(
          SetShowModalIsNotification({
            description: errorNotification,
            showValue: true,
            functionConfirm: '',
            functionCancel: 'handleCloseModalNotification',
            valueBtnConfirm: '',
            valueBtnCancel: '',
          }),
        );
      })
      .finally(() => setLoading(false));
  };
  const handleGoBack = () => {
    navigation.goBack();
  };
  const handleChangeDate = (value: string) => {
    const Regex = /\d{4}\/\d{2}\/\d{2}/;
    if (value == '') {
      setDateValue({value: value, error: 'Vui lòng nhập trường này'});
      return;
    }
    if (Regex.test(value)) {
      setDateValue({value: value, error: ''});
    } else {
      setDateValue({value: value, error: 'Không đúng định dạng'});
    }
  };
  const handleSearchPayment = async () => {
    setSearchCheck(true);
    setReRenderFilterDate(!reRenderFilterDate);
    const timeid = setTimeout(() => {
      setSearchCheck(false);
    }, 2000);
    return () => {
      clearTimeout(timeid);
    };
  };
  const handleShowCalendar = (NumberValue: number) => {
    if (changeHistoryFilter == 0) {
      setShowCalendar(true);
      setDateValid(NumberValue);
      return;
    }
    if (changeHistoryFilter == 1) {
      setShowCalendar(true);
      setDateValid(NumberValue);
      return;
    }
  };
  const hideDatePicker = () => {
    setShowCalendar(false);
  };
  const handleConfirm = date => {
    const dateTime = new Date(date);
    const convertedDate = new Date(dateTime.getTime());
    const Time = `${convertedDate.getFullYear()}/${
      convertedDate.getMonth() + 1 < 10
        ? '0' + (convertedDate.getMonth() + 1)
        : convertedDate.getMonth() + 1
    }/${
      convertedDate.getDate() < 10
        ? '0' + convertedDate.getDate()
        : convertedDate.getDate()
    }`;
    setShowCalendar(false);
    if (DateValid == 0) {
      setDateValue({value: Time, error: ''});
      return;
    }
    if (DateValid == 1) {
      setStartDate({value: Time, error: ''});
      return;
    }
    if (DateValid == 2) {
      setEndDate({value: Time, error: ''});
      return;
    }
  };
  const handleChangeFilterHistory = () => {
    setChangeHistoryFilter(changeHistoryFilter == 0 ? 1 : 0);
  };
  return (
    <SafeAreaView style={{flex: 1, backgroundColor: '#F5F5F5'}}>
      <View style={{display: 'flex', flex: 1, padding: 10}}>
        <View style={HistoryPaymentCss.Header}>
          <View style={HistoryPaymentCss.HeaderContainer}>
            <View style={HistoryPaymentCss.HeaderHistory}>
              <TouchableOpacity onPress={handleGoBack}>
                <Ionicons
                  name={xmlChangeText(HistoryPaymentXml, 'IconNavigation', 'chevron-back-outline')}
                  size={widthScreen * 0.12}
                  color={xmlChangeText(HistoryPaymentXml, 'IconNavigationColor', '#0066CC')}
                />
              </TouchableOpacity>
              <Text style={HistoryPaymentCss.HeaderContainerText}>
                Lịch Sử Thanh Toán
              </Text>
            </View>
          </View>
          <View style={{width: '100%'}}>
            <View style={HistoryPaymentCss.ChangeFilterHistoryContainer}>
              <Text style={HistoryPaymentCss.filterBtnText}>
                {changeHistoryFilter == 0
                  ? 'Lọc theo ngày'
                  : 'Lọc trong khoảng'}
              </Text>
              <TouchableOpacity
                onPress={handleChangeFilterHistory}
                style={HistoryPaymentCss.filterBtn}>
                <MaterialIcons
                  name="autorenew"
                  size={widthScreen * 0.09}
                  color={colors.IconColor}
                />
              </TouchableOpacity>
            </View>
            <View></View>
          </View>
          {changeHistoryFilter == 0 ? (
            <View>
              <View style={HistoryPaymentCss.SearchContainer}>
                <TextInput
                  style={HistoryPaymentCss.InputContainer}
                  clearButtonMode="always"
                  placeholder="vd: Năm/tháng/ngày"
                  placeholderTextColor="#888888"
                  onChangeText={value => handleChangeDate(value)}
                  autoCorrect={false}
                  value={dateValue.value}
                  autoCapitalize="none"
                />
                <TouchableOpacity
                  onPress={() => handleShowCalendar(0)}
                  style={{marginRight: 16}}>
                  <Ionicons
                    name={xmlChangeText(HistoryPaymentXml, 'IconCalendar', 'calendar-outline')}
                    size={widthScreen * 0.072}
                    color={xmlChangeText(HistoryPaymentXml, 'ColorIconCalendar', '#0066CC')}
                  />
                </TouchableOpacity>
                <TouchableOpacity
                  disabled={
                    dateValue.error !== '' || SearchCheck ? true : false
                  }
                  onPress={handleSearchPayment}
                  style={{marginRight: 16}}>
                  <MaterialIcons
                    name={
                      dateValue.error !== '' || SearchCheck
                        ? xmlChangeText(HistoryPaymentXml, 'IconSearchOff', 'search-off')
                        : xmlChangeText(HistoryPaymentXml, 'IconSearch', 'search')
                    }
                    size={widthScreen * 0.072}
                    color={
                      dateValue.error !== '' || SearchCheck ? 'red' : '#0066CC'
                    }
                  />
                </TouchableOpacity>
              </View>
              {dateValue.error !== '' ? (
                <View
                  style={{width: '100%', marginTop: 10, paddingHorizontal: 10}}>
                  <Text
                    style={{
                      color: 'red',
                      fontWeight: '700',
                      fontSize: widthScreen * 0.038,
                    }}>
                    {dateValue.error !== '' ? dateValue.error : ''}
                  </Text>
                </View>
              ) : (
                ''
              )}
            </View>
          ) : (
            <View style={HistoryPaymentCss.filterInClauseContainer}>
              <View style={HistoryPaymentCss.TextDateContainer}>
                <Text style={HistoryPaymentCss.ContainerStartDateText}>
                  Ngày bắt đầu
                </Text>
                <Text style={HistoryPaymentCss.ContainerEndDateText}>
                  Ngày kết thúc
                </Text>
              </View>
              <View style={HistoryPaymentCss.BtnFilterStartEndContainer}>
                <View style={HistoryPaymentCss.ContainerStartDate}>
                  <View style={HistoryPaymentCss.BtnFilterStartDate}>
                    <Text style={HistoryPaymentCss.BtnFilterStartDateText}>
                      {startDate.value}
                    </Text>
                    <TouchableOpacity onPress={() => handleShowCalendar(1)}>
                      <Ionicons
                        name={xmlChangeText(HistoryPaymentXml, 'IconCalendar', 'calendar-outline')}
                        size={widthScreen * 0.08}
                        color={xmlChangeText(HistoryPaymentXml, 'ColorIconCalendar', '#0066CC')}
                      />
                    </TouchableOpacity>
                  </View>
                </View>
                <View style={HistoryPaymentCss.ContainerEndDate}>
                  <View style={HistoryPaymentCss.BtnFilterEndDate}>
                    <Text style={HistoryPaymentCss.BtnFilterEndDateText}>
                      {endDate.value}
                    </Text>
                    <TouchableOpacity onPress={() => handleShowCalendar(2)}>
                      <Ionicons
                        name={xmlChangeText(HistoryPaymentXml, 'IconCalendar', 'calendar-outline')}
                        size={widthScreen * 0.08}
                        color={xmlChangeText(HistoryPaymentXml, 'ColorIconCalendar', '#0066CC')}
                      />
                    </TouchableOpacity>
                  </View>
                </View>
                <TouchableOpacity
                  disabled={SearchCheck ? true : false}
                  onPress={handleSearchPayment}>
                  <MaterialIcons
                    name={SearchCheck ? xmlChangeText(HistoryPaymentXml, 'IconSearchOff', 'search-off') : xmlChangeText(HistoryPaymentXml, 'IconSearch', 'search')}
                    size={widthScreen * 0.08}
                    color={
                      dateValue.error !== '' || SearchCheck ? 'red' : '#0066CC'
                    }
                  />
                </TouchableOpacity>
              </View>
            </View>
          )}
        </View>
        <View style={HistoryPaymentCss.HeaderContainerBody}>
          {Loading ? (
            <View style={{flex: 1}}>
              <ActivityIndicator
                size={42}
                color="#0099FF"
                style={{
                  alignItems: 'center',
                  justifyContent: 'center',
                  flex: 1,
                }}
              />
            </View>
          ) : paymentHistory.length > 0 ? (
            <FlatList
              showsVerticalScrollIndicator={false}
              showsHorizontalScrollIndicator={false}
              data={paymentHistory}
              renderItem={({item, index}) => (
                <HistoryPaymentItem item={item} />
              )}
              keyExtractor={(item, index) => index.toString()}
            />
          ) : (
            <View
              style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
              <Text
                style={{
                  fontSize: widthScreen * 0.05,
                  fontWeight: '700',
                  color: '#888888',
                }}>
                Không có hoá đơn nào
              </Text>
            </View>
          )}
        </View>
      </View>
      <DatePicker
        modal
        mode="date"
        locale="vi-vn"
        open={showCalendar}
        date={new Date()}
        onConfirm={date => {
          handleConfirm(date);
        }}
        onCancel={hideDatePicker}
      />
      {isNotification ? <ModalNotification /> : ''}
      {ShowDetailPaymentHistory ? (
        <DetailPaymentHistory navigation={navigation} />
      ) : (
        ''
      )}
    </SafeAreaView>
  );
};

export default React.memo(HistoryPayment);
