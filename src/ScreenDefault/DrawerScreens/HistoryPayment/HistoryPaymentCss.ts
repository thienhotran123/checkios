import {StyleSheet} from 'react-native';
import {widthScreen} from '../../../Utils/Styles';
import {verticalScale} from '../../../Utils/Reponsive';
export const HistoryPaymentCss = StyleSheet.create({
  container: {
    width: '100%',
  },
  Header: {
    width: '100%',
  },
  HeaderContainer: {
    display: 'flex',
    alignItems: 'center',
    flexDirection: 'row',
  },
  HeaderContainerText: {
    fontSize: widthScreen * 0.046,
    color: '#333',
    fontWeight: '900',
  },
  HeaderContainerBody: {
    flex: 1,
    marginTop: 10,
  },
  SearchContainer: {
    marginTop: 10,
    justifyContent: 'flex-end',
    alignItems: 'center',
    display: 'flex',
    flexDirection: 'row',
    backgroundColor: '#DDDDDD',
    borderRadius: 10,
    height: verticalScale(54),
  },
  InputContainer: {
    flex: 1,
    backgroundColor: '#DDDDDD',
    borderWidth: 1,
    borderColor: '#DDDDDD',
    height: verticalScale(54),
    fontSize: widthScreen * 0.04,
    paddingHorizontal: 16,
    color: '#666666',
    borderRadius: 10,
    width: '100%',
  },
  filterBtn: {
    paddingHorizontal: 10,
    flexDirection: 'row',
    alignItems: 'center',
  },
  filterBtnText: {
    fontSize: widthScreen * 0.048,
    fontWeight:'700',
    color:'#888888',
  },
  HeaderHistory: {
    width:'100%',
    alignItems: 'center',
    flexDirection: 'row',
  },
  ChangeFilterHistoryContainer:{
    display:'flex',
    flexDirection:'row',
    alignItems:'center'
  },
  filterInClauseContainer:{
    marginTop:10,
    width:'100%',

  },
  BtnFilterStartDate:{
    flexDirection:'row',
    alignItems:'center'
  },
  BtnFilterStartDateText:{
    fontSize:widthScreen*0.04,
    fontWeight:'700',
    paddingRight:10,
    color:'#888888'
  },
  BtnFilterEndDate:{
    flexDirection:'row',
    alignItems:'center',
  },
  BtnFilterEndDateText:{
    fontSize:widthScreen*0.04,
    paddingRight:10,
    fontWeight:'700',
    color:'#888888'
  },
  ContainerEndDate:{
    width:'43%'
  },
  ContainerEndDateText:{
    width:'43%',
    fontSize:widthScreen*0.048,
    fontWeight:'700',
    color:'#888888'
  },
  BtnFilterStartEndContainer:{
    flexDirection:'row',
    alignItems:'center',
    marginTop:10
  },
  ContainerStartDate:{
    width:'43%'
  },
  ContainerStartDateText:{
    width:'43%',
    fontSize:widthScreen*0.048,
    fontWeight:'700',
    color:'#888888'
  },
  TextDateContainer:{
    flexDirection:'row',
    alignItems:'center'
  }
});
