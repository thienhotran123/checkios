import {
  ActivityIndicator,
  SafeAreaView,
  ScrollView,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import React, {useEffect, useMemo, useState} from 'react';
import {styles} from './DayReportCss';
import Ionicons from 'react-native-vector-icons/Ionicons';
import {colors, widthScreen} from '../../../Utils/Styles';
import {verticalScale} from '../../../Utils/Reponsive';
import {RootState} from '../../../Redux/Storage';
import {useDispatch, useSelector} from 'react-redux';
import {
  getDay,
  getMonth,
  getYear,
} from '../../../Utils/Helper';
import {GetAllPaymentUser} from '../../../Utils/GetApi';
import {ModalNotification} from '../../../Components/Components';
import {SetShowModalIsNotification} from '../../../Redux/Slide';
import {Success} from '../../../Utils/Enums';
import {errorNotification} from '../../../Utils/MessageNotification';
import { CommaSeparatedValues } from '../../../Services/Services';
const DayReport = ({navigation}) => {
  const dispatch = useDispatch();
  const [DayReport, setDayReport] = useState('');
  const UserName = useSelector((state: RootState) => state.counter.UserName);
  const [dataPayment, setDataPayment] = useState<any>([]);
  const isNotification = useSelector(
    (state: RootState) => state.counter.isNotification,
  );
  const [loading, setLoading] = useState<boolean>(false);
  const [Reload, setReload] = useState<boolean>(false);
  const [ReloadDayReport, setReloadDayReport] = useState<boolean>(false);
  useEffect(() => {
    const getDataPayment = () => {
      setLoading(true);
      const date = new Date();
      const Time = `${getYear(date)}-${getMonth(date)}-${getDay(date)}`;
      GetAllPaymentUser({
        token: UserName.token,
        data: {user_id: UserName.id, date: Time},
      })
        .then(data => {
          if (data.status_code == Success) {
            const dataPayment = data.payment.filter(item=>item.status!==-1)
            setDataPayment(dataPayment);
          } else {
            setDataPayment([]);
          }
        })
        .catch(() => {
          dispatch(
            SetShowModalIsNotification({
              description: errorNotification,
              showValue: true,
              functionConfirm: '',
              functionCancel: 'handleCloseModalNotification',
              valueBtnConfirm: '',
              valueBtnCancel: '',
            }),
          );
          setDataPayment([]);
        })
        .finally(() => setLoading(false));
      setDayReport(Time);
    };
    getDataPayment();
  }, [ReloadDayReport]);
  const handleReload = () => {
    setReload(true);
    setReloadDayReport(!ReloadDayReport);
    const timeid = setTimeout(() => {
      setReload(false);
    }, 2000);
    return () => {
      clearTimeout(timeid);
    };
  };
  const handelGoBack = () => {
    navigation.goBack();
  };
  const totalIncome = useMemo(() => {
    if (dataPayment.length > 0) {
      return dataPayment.reduce((init, cur) => {
        if (cur.valuetotal !== undefined) {
          return (init += cur.valuetotal);
        } else {
          return 0;
        }
      }, 0);
    } else {
      return 0;
    }
  }, [dataPayment]);
  const Total = useMemo(() => {
    if (dataPayment.length > 0) {
      return dataPayment.reduce((init: number, cur) => {
        const dataItem = JSON.parse(cur.items);
        return (init += dataItem.item.reduce((init2: number, cur2) => {
          return (init2 += cur2.price * (cur2.quantity));
        }, 0));
      }, 0);
    } else {
      return 0;
    }
  }, [dataPayment]);
  const TotalVat = useMemo(() => {
    if (dataPayment.length > 0) {
      return dataPayment.reduce((init: number, cur) => {
        const dataItem = JSON.parse(cur.items);
        return (init += dataItem.item.reduce((init2: number, cur2) => {
          return (init2 += cur2.price * (cur2.vat / 100) * cur2.quantity)
        }, 0));
      }, 0);
    } else {
      return 0;
    }
  }, [dataPayment]);
  const TotalQuantity = useMemo(() => {
    if (dataPayment.length > 0) {
      return dataPayment.reduce((init: number, cur) => {
        const dataItem = JSON.parse(cur.items);
        return (init += dataItem.item.reduce((init2: number, cur2) => {
          return (init2 += cur2.quantity);
        }, 0));
      }, 0);
    } else {
      return 0;
    }
  }, [dataPayment]);
  return (
    <SafeAreaView style={styles.containerPayment}>
      <View style={styles.container}>
        <View style={styles.headerContainer}>
          <View style={{flexDirection: 'row', alignItems: 'center', flex: 1}}>
            <TouchableOpacity onPress={handelGoBack}>
              <Ionicons
                name="chevron-back-outline"
                size={widthScreen * 0.14}
                color={colors.IconColor}
              />
            </TouchableOpacity>
            <Text style={styles.paymentText}>Báo Cáo Hôm Nay</Text>
          </View>
          <TouchableOpacity
            disabled={Reload ? true : false}
            onPress={handleReload}
            style={{marginRight: 10}}>
            <Ionicons
              name="reload-outline"
              size={widthScreen * 0.08}
              color={Reload ? '#444444' : colors.IconColor}
            />
          </TouchableOpacity>
        </View>
        {loading ? (
          <View style={{flex: 1}}>
            <ActivityIndicator
              size={42}
              color="#0099FF"
              style={{alignItems: 'center', justifyContent: 'center', flex: 1}}
            />
          </View>
        ) : (
          <View
            style={{position: 'relative', backgroundColor: '#F5F6F8', flex: 1}}>
            <ScrollView
              style={{flex: 1, backgroundColor: '#F5F6F8'}}
              showsVerticalScrollIndicator={false}
              showsHorizontalScrollIndicator={false}>
              <View style={{paddingHorizontal: 10}}></View>
              <View
                style={{
                  padding: 10,
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'space-between',
                }}>
                <Text
                  style={{
                    color: '#777777',
                    fontSize: widthScreen * 0.036,
                    fontWeight: '900',
                  }}>
                  TỔNG HOÁ ĐƠN
                </Text>
                <Text
                  style={{
                    color: '#777777',
                    fontSize: widthScreen * 0.036,
                    fontWeight: '700',
                  }}></Text>
              </View>
              <View style={{backgroundColor: '#fff', paddingHorizontal: 10}}>
                <View
                  style={{
                    display: 'flex',
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: 'space-between',
                    borderBottomColor: '#EEEEEE',
                    borderBottomWidth: 1,
                    height: verticalScale(70),
                  }}>
                  <Text
                    style={{
                      fontSize: widthScreen * 0.04,
                      color: '#777777',
                      fontWeight: '700',
                    }}>
                    Ngày
                  </Text>
                  <Text
                    style={{
                      fontSize: widthScreen * 0.04,
                      color: '#777777',
                      fontWeight: '700',
                    }}
                    numberOfLines={1}>
                    {DayReport}
                  </Text>
                </View>
                <View
                  style={{
                    display: 'flex',
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: 'space-between',
                    borderBottomColor: '#EEEEEE',
                    borderBottomWidth: 1,
                    height: verticalScale(70),
                  }}>
                  <Text
                    style={{
                      fontSize: widthScreen * 0.04,
                      color: '#777777',
                      fontWeight: '700',
                    }}>
                    Nhân viên
                  </Text>
                  <Text
                    style={{
                      fontSize: widthScreen * 0.04,
                      color: '#777777',
                      fontWeight: '700',
                    }}
                    numberOfLines={1}>
                    {UserName.name}
                  </Text>
                </View>
                <View
                  style={{
                    display: 'flex',
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: 'space-between',
                    borderBottomColor: '#EEEEEE',
                    borderBottomWidth: 1,
                    height: verticalScale(70),
                  }}>
                  <Text
                    style={{
                      fontSize: widthScreen * 0.04,
                      color: '#777777',
                      fontWeight: '700',
                    }}>
                    Hoá đơn đã thanh toán
                  </Text>
                  <Text
                    style={{
                      fontSize: widthScreen * 0.04,
                      color: '#777777',
                      fontWeight: '700',
                    }}
                    numberOfLines={1}>
                    {dataPayment.length}
                  </Text>
                </View>
                <View
                  style={{
                    display: 'flex',
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: 'space-between',
                    borderBottomColor: '#EEEEEE',
                    borderBottomWidth: 1,
                    height: verticalScale(70),
                  }}>
                  <Text
                    style={{
                      fontSize: widthScreen * 0.04,
                      color: '#777777',
                      fontWeight: '700',
                    }}>
                    Tổng tiền
                  </Text>
                  <Text
                    style={{
                      fontSize: widthScreen * 0.04,
                      color: '#777777',
                      fontWeight: '700',
                    }}
                    numberOfLines={1}>
                    {CommaSeparatedValues(Total)} VND
                  </Text>
                </View>
                <View
                  style={{
                    display: 'flex',
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: 'space-between',
                    borderBottomColor: '#EEEEEE',
                    borderBottomWidth: 1,
                    height: verticalScale(70),
                  }}>
                  <Text
                    style={{
                      fontSize: widthScreen * 0.04,
                      color: '#777777',
                      fontWeight: '700',
                    }}>
                    Tổng VAT
                  </Text>
                  <Text
                    style={{
                      fontSize: widthScreen * 0.04,
                      color: '#777777',
                      fontWeight: '700',
                    }}
                    numberOfLines={1}>
                    {CommaSeparatedValues(TotalVat)} VND
                  </Text>
                </View>
                <View
                  style={{
                    display: 'flex',
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: 'space-between',
                    borderBottomColor: '#EEEEEE',
                    borderBottomWidth: 1,
                    height: verticalScale(70),
                  }}>
                  <Text
                    style={{
                      fontSize: widthScreen * 0.04,
                      color: '#777777',
                      fontWeight: '700',
                    }}>
                    Tổng số lượng món
                  </Text>
                  <Text
                    style={{
                      fontSize: widthScreen * 0.04,
                      color: '#777777',
                      fontWeight: '700',
                    }}
                    numberOfLines={1}>
                    {TotalQuantity}
                  </Text>
                </View>
              </View>
              <View style={{padding: 10}}>
                <Text
                  style={{
                    color: '#777777',
                    fontSize: widthScreen * 0.036,
                    fontWeight: '900',
                  }}>
                  THU NHẬP
                </Text>
              </View>
              <View style={{backgroundColor: '#fff', paddingHorizontal: 10}}>
                <View
                  style={{
                    display: 'flex',
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: 'space-between',
                    borderBottomColor: '#EEEEEE',
                    borderBottomWidth: 1,
                    height: verticalScale(70),
                  }}>
                  <Text
                    style={{
                      fontSize: widthScreen * 0.04,
                      color: '#777777',
                      fontWeight: '700',
                    }}>
                    Tổng khách trả
                  </Text>
                  <Text
                    style={{
                      fontSize: widthScreen * 0.04,
                      color: '#777777',
                      fontWeight: '700',
                    }}
                    numberOfLines={1}>
                    {CommaSeparatedValues(totalIncome)} VND
                  </Text>
                </View>
              </View>
            </ScrollView>
          </View>
        )}
      </View>
      {isNotification ? <ModalNotification /> : ''}
    </SafeAreaView>
  );
};

export default React.memo(DayReport);
