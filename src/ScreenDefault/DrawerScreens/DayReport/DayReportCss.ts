import {StyleSheet} from 'react-native';
import {Mode, colors, widthScreen} from '../../../Utils/Styles';
import {scale, verticalScale} from '../../../Utils/Reponsive';

export const styles = StyleSheet.create({
  container: {
    backgroundColor: '#F5F6F8',
    flex: 1,
  },
  headerContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: '#ffffff',
  },
  paymentText: {
    flex: 1,
    fontSize: widthScreen * 0.05,
    color: '#333',
    fontWeight: '900',
  },
  contentBill: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginVertical: 10,
  },
  backgroundBottom: {
    backgroundColor: colors.primaryColor,
    width: '100%',
    height: widthScreen * 0.12,
    justifyContent: 'center',
    alignItems: 'center',
    marginVertical: 10,
    borderRadius: 8,
  },
  backgroundBottomDisabled: {
    backgroundColor: '#C0C0C0',
    width: '100%',

    height: widthScreen * 0.12,
    justifyContent: 'center',
    alignItems: 'center',
    marginVertical: 10,
    borderRadius: 8,
  },
  welcome: {
    color: '#fff',
    fontSize: widthScreen * 0.04,
  },
  InfoBill: {
    flexDirection: 'row',
    paddingTop: 10,
    width: '100%',
    alignItems: 'center',
  },
  InfoBillTMain: {
    width: '20%',
  },
  InfoBillSLMain: {
    width: '20%',
  },
  InfoBillDGMain: {
    width: '20%',
  },
  InfoBillTTMain: {
    width: '20%',
  },
  InfoBillTMainText: {
    fontWeight: '700',
    fontSize: widthScreen * 0.04,
    color: colors.Textcolor,
  },
  InfoBillSLMainText: {
    fontWeight: '700',
    fontSize: widthScreen * 0.04,
    color: colors.Textcolor,
    textAlign: 'center',
  },
  InfoBillDGMainText: {
    fontWeight: '700',
    fontSize: widthScreen * 0.04,
    color: colors.Textcolor,
    textAlign: 'center',
  },
  InfoBillTTMainText: {
    fontWeight: '700',
    fontSize: widthScreen * 0.04,
    color: colors.Textcolor,
    textAlign: 'center',
  },
  InfoBillTMainItem: {
    width: '20%',
  },
  InfoBillTMainItemText: {
    width: '100%',
    fontWeight: '700',
  },
  InfoBillSLMainItem: {
    width: '20%',
  },
  InfoBillSLMainItemText: {
    textAlign: 'center',
    width: '100%',
    fontWeight: '700',
  },
  InfoBillDGMainItem: {
    width: '20%',
  },
  InfoBillDGMainItemText: {
    textAlign: 'center',
    width: '100%',
    fontWeight: '700',
  },
  InfoBillTTMainItem: {
    width: '20%',
  },
  InfoBillTTMainItemText: {
    textAlign: 'center',
    width: '100%',
    fontWeight: '700',
  },
  Loadding: {
    backgroundColor: 'rgba(0,0,0,0.6)',
    zIndex: 12,
    position: 'absolute',
    alignItems: 'center',
    justifyContent: 'center',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    flex: 1,
  },
  containerPayment: {
    flex: 1,
    backgroundColor: '#FFFFFF',
  },
  containerBtnInput: {
    width: '100%',
    display: 'flex',
    alignItems: 'center',
    flexDirection: 'row',
  },
  containerBtnInputTouch: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
  },
  containerBtnInputText: {
    fontSize: widthScreen * 0.04,
    color: '#777777',
    fontWeight: '700',
  },
  containerInputCustomer: {
    marginTop: 4,
    width: '100%',
  },
  CustomerInput: {
    height: verticalScale(48),
    marginTop: 2,
    width: '100%',
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
  },
  inputCustomer: {
    color: '#333',
    backgroundColor: Mode == 'dark' ? '#fff' : '#fff',
    width: '60%',
    height: '100%',
    borderWidth: 2,
    borderColor: Mode == 'dark' ? '#fff' : '#CCCCCC',
    borderRadius: 8,
    padding: 10,
    fontSize: widthScreen * 0.03,
  },
  nameCustomer: {
    fontSize: widthScreen * 0.04,
    fontWeight: '600',
    color: Mode == 'dark' ? '#fff' : '#888888',
    marginLeft: 20,
    flex: 1,
  },
  scrollCustomerContainer: {
    position: 'absolute',
    width: '60%',
    paddingHorizontal: 10,
    height: scale(200),
    backgroundColor: Mode == 'dark' ? '#fff' : '#333',
    zIndex: 10,
    elevation: 3,
    borderRadius: 8,
    top: '104%',
  },
  btnCustomer: {
    display: 'flex',
    flexDirection: 'row',
    paddingVertical: 8,
  },
  btnCustomerPhone: {
    color: Mode == 'dark' ? '#333' : '#fff',
    fontSize: widthScreen * 0.04,
  },
  btnCustomerName: {
    color: Mode == 'dark' ? '#333' : '#fff',
    fontSize: widthScreen * 0.04,
    flex: 1,
  },
  containerFooterPayment: {
    borderTopColor: colors.AlanFooterColor,
    borderTopWidth: 3,
    justifyContent: 'center',
    alignItems: 'center',
  },
  containerFooterPaymentInfo: {
    width: '70%',
    justifyContent: 'center',
  },
  containerFooterPoint: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
  },
  containerFooterPointTextValid: {
    fontSize: widthScreen * 0.04,
    color: Mode == 'dark' ? '#fff' : '#333',
    paddingLeft: 10,
  },
  containerFooterPointTextNotValid: {
    fontSize: widthScreen * 0.04,
    color: Mode == 'dark' ? '#fff' : '#333',
    textDecorationLine: 'line-through',
    paddingLeft: 10,
  },
  TextFooterPayment: {
    fontSize: widthScreen * 0.04,
    color: '#333',
    paddingVertical: 3,
  },
  btnFooterPayment: {
    backgroundColor: colors.btnColor,
    justifyContent: 'center',
    alignItems: 'center',
    height: verticalScale(48),
    width: '100%',
    borderRadius: 6,
    marginVertical: 6,
  },
  textBtnFooterPayment: {
    color: '#fff',
    fontSize: widthScreen * 0.038,
    fontWeight: '700',
  },
});
