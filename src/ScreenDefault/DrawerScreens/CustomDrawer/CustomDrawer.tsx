import {Linking, Platform, Text, TouchableOpacity, View} from 'react-native';
import React from 'react';
import {
  DrawerContentScrollView,
  DrawerItemList,
} from '@react-navigation/drawer';
import Ionicons from 'react-native-vector-icons/Ionicons';
import {useDispatch, useSelector} from 'react-redux';
import {SetShowModalIsNotification} from '../../../Redux/Slide';
import {RootState} from '../../../Redux/Storage';
import {widthScreen} from '../../../Utils/Styles';
import DeviceInfo from 'react-native-device-info';
import {LogOutApp} from '../../../Utils/MessageNotification';
const CustomDrawer = props => {
  const UserName = useSelector((state: RootState) => state.counter.UserName);
  const dispatch = useDispatch();
  const handleLogOut = () => {
    props.navigation.closeDrawer();
    dispatch(
      SetShowModalIsNotification({
        description: LogOutApp,
        showValue: true,
        functionConfirm: 'handleLogOut',
        functionCancel: 'handleCloseModalNotification',
        valueBtnConfirm: '',
        valueBtnCancel: '',
      }),
    );
  };
  const handlePhoneCall = (value: string) => {
    let phoneNumber = value;
    if (Platform.OS === 'android') {
      phoneNumber = `tel:${phoneNumber}`;
    } else {
      phoneNumber = `telprompt:${phoneNumber}`;
    }
    Linking.openURL(phoneNumber);
  };
  return (
    <View style={{flex: 1}}>
      <DrawerContentScrollView
        contentContainerStyle={{backgroundColor: '#555555'}}
        {...props}>
        <View
          style={{
            padding: 20,
            backgroundColor: '#555555',
            flexDirection: 'row',
            alignItems: 'center',
          }}>
          <View style={{flex: 1}}>
            <Text
              style={{
                fontSize: widthScreen * 0.04,
                color: '#fff',
                fontWeight: '700',
              }}>
              {UserName.name}
            </Text>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                paddingTop: 6,
              }}>
              <Text
                style={{
                  fontSize: widthScreen * 0.03,
                  color: '#33FF00',
                  fontWeight: '700',
                }}>
                Đang hoạt động
              </Text>
              <View
                style={{
                  width: 10,
                  height: 10,
                  backgroundColor: '#00FF00',
                  marginLeft: 10,
                  borderRadius: 100,
                }}></View>
            </View>
          </View>
        </View>
        <View style={{flex: 1, backgroundColor: '#fff', paddingTop: 10}}>
          <DrawerItemList {...props} />
          <View style={{paddingVertical: 10, paddingHorizontal: 20}}>
            <TouchableOpacity
              style={{flexDirection: 'row', alignItems: 'center'}}
              onPress={() => handlePhoneCall('02862651411')}>
              <Ionicons
                name="call-outline"
                size={widthScreen * 0.07}
                color="#333"
              />
              <Text
                style={{
                  marginLeft: 10,
                  fontSize: widthScreen * 0.036,
                  color: '#333',
                  fontWeight: '600',
                }}>
                Hỗ trợ
              </Text>
              <Text
                style={{
                  marginLeft: 10,
                  fontSize: widthScreen * 0.036,
                  color: '#0066CC',
                  fontWeight: '600',
                  flex: 1,
                }}
                numberOfLines={1}>
                02862651411
              </Text>
            </TouchableOpacity>
          </View>
          {UserName.phonenumbercompany !== undefined &&
          UserName.phonenumbercompany !== null ? (
            <View style={{paddingVertical: 10, paddingHorizontal: 20}}>
              <TouchableOpacity
                style={{flexDirection: 'row', alignItems: 'center'}}
                onPress={() =>
                  handlePhoneCall(`${UserName.phonenumbercompany}`)
                }>
                <Ionicons
                  name="call-outline"
                  size={widthScreen * 0.07}
                  color="#333"
                />
                <Text
                  style={{
                    marginLeft: 10,
                    fontSize: widthScreen * 0.036,
                    color: '#333',
                    fontWeight: '600',
                  }}>
                  Chủ
                </Text>
                <Text
                  style={{
                    marginLeft: 10,
                    fontSize: widthScreen * 0.036,
                    color: '#0066CC',
                    fontWeight: '600',
                    flex: 1,
                  }}
                  numberOfLines={1}>
                  {UserName.phonenumbercompany}
                </Text>
              </TouchableOpacity>
            </View>
          ) : (
            ''
          )}
        </View>
      </DrawerContentScrollView>
      <View
        style={{
          padding: 20,
          borderTopWidth: DeviceInfo.isTablet() ? 2 : 1,
          borderTopColor: '#ccc',
        }}>
        <TouchableOpacity onPress={handleLogOut}>
          <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <Ionicons
              name="log-out-outline"
              size={widthScreen * 0.06}
              color="#333"
            />
            <Text
              style={{
                fontWeight: '700',
                color: '#333',
                fontSize: widthScreen * 0.04,
                paddingLeft: 15,
              }}>
              Đăng xuất
            </Text>
          </View>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default React.memo(CustomDrawer);
