import { StyleSheet } from 'react-native'
import { widthScreen } from '../../../Utils/Styles'

export const HistoryEditOrderCss = StyleSheet.create({
    HeaderContainer:{
        flexDirection:'row',
        alignItems:'center'
    },
    HeaderContainerText:{
        fontSize:widthScreen*0.04,
        fontWeight:'600',
        color:'#333'
    }
})