import {Text, View, SafeAreaView, TouchableOpacity} from 'react-native';
import React from 'react';
import {HistoryEditOrderCss} from './HistoryEditOrderCss';
import Ionicons from 'react-native-vector-icons/Ionicons';
import {colors, widthScreen} from '../../../Utils/Styles';
const HistoryEditOrder = ({navigation}) => {
  const handleGoBack = () => {
    navigation.goBack();
  };
  return (
    <SafeAreaView style={{flex: 1}}>
      <View style={{flex: 1}}>
        <View style={HistoryEditOrderCss.HeaderContainer}>
          <TouchableOpacity onPress={handleGoBack}>
            <Ionicons
              name="chevron-back-outline"
              size={widthScreen * 0.14}
              color={colors.IconColor}
            />
          </TouchableOpacity>
          <Text style={HistoryEditOrderCss.HeaderContainerText}>
            Lịch Sử Chỉnh Sửa
          </Text>
        </View>
      </View>
    </SafeAreaView>
  );
};

export default React.memo(HistoryEditOrder);
