import {Alert, LogBox} from 'react-native';
import React, {useEffect, useReducer, useState} from 'react';
import DefaultLayoutDrawer from './DefaultLayoutDrawer';
import {LoadingApp} from '../Components/Components';
import {NavigationContainer} from '@react-navigation/native';
import {navigationRef} from '../Utils/NavigationFuc';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {getXmlStaff, Login} from '../Utils/GetApi';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {loginData} from '../Utils/interface/interface';
import {XMLParser} from 'fast-xml-parser';
import NetInfo from '@react-native-community/netinfo';
import {
  ScreenLogin,
  ContactFormScreen,
  CancelInvoicePrintedOrder,
  PaymentScreen,
  ProductScreen,
  PrintDetailView,
} from '../ScreenDefault/Screens';
import {SetConnectNetwork, SetXmlProperties} from '../Redux/order';
import {useDispatch, useSelector} from 'react-redux';
import {RootState} from '../Redux/Storage';
import {Success} from '../Utils/Enums';
import {ResetState, SetShowModalIsNotification} from '../Redux/Slide';
import { errorNotification } from '../Utils/MessageNotification';
LogBox.ignoreLogs(['Warning: ...']);
LogBox.ignoreAllLogs();
const AuthContext = React.createContext({
  signIn: (data: loginData, pass: string) => {},
  signOut: (Notification: string) => {},
});
const DefaultLayoutScreen = () => {
  const dispatchState = useDispatch();
  const checkCallGetXml = useSelector((state: RootState) => state.item.checkCallGetXml,);
  const [isLoading, setIsLoading] = useState(false);
  const [stateToken, dispatch] = useReducer(
    (prevState, action) => {
      switch (action.type) {
        case 'RESTORE_TOKEN':
          return {...prevState, userToken: action.token};
        case 'SIGN_IN':
          return {
            userToken: action.token,
          };
        case 'SIGN_OUT':
          return {
            userToken: action.token,
          };
      }
    },
    {userToken: null},
  );
  const Stack = createNativeStackNavigator();
  const screenOptions = {
    headerShown: false,
  };
  const handleCheckLogin = async (data, pass) => {
    if (data.status_code == Success) {
      dispatch({type: 'RESTORE_TOKEN', token: data});
      await AsyncStorage.setItem(
        'userToken',
        JSON.stringify({
          token: data.access_token,
          name: data.user.name,
          id: data.user.id,
          phone: data.user.phone,
          address: data.user.address,
          status: data.user.status,
          pass: pass,
          storename: data.user.storename,
          logo: data.user.logo,
          addresscompany: data.user.addresscompany,
          phonenumbercompany: data.user.phonenumbercompany,
        }
        ));
      return;
    }
    else {
      await AsyncStorage.removeItem('userToken');
      dispatch({type: 'SIGN_IN', token: null});
    }
  };
  const handleOut = async e => {
    await AsyncStorage.removeItem('userToken');
    dispatch({type: 'SIGN_IN', token: null});
    dispatchState(
      SetShowModalIsNotification({
        description: errorNotification,
        showValue: true,
        functionConfirm: '',
        functionCancel: 'handleCloseModalNotification',
        valueBtnConfirm: '',
        valueBtnCancel: ''
      }));
  };
  useEffect(() => {
    const getData = async () => {
      const userToken = await AsyncStorage.getItem('userToken');
      const dataUser = userToken ? JSON.parse(userToken) : null;
      if (dataUser !== null) {
        setIsLoading(true);
        Login({phone: dataUser.phone, password: dataUser.pass})
          .then(data => handleCheckLogin(data, dataUser.pass))
          .catch(e => handleOut(e))
          .finally(() => setIsLoading(false));
      } else {
        dispatch({type: 'RESTORE_TOKEN', token: null});
      }
    };
    getData();
  }, []);
  useEffect(() => {
    const unsubscribe = NetInfo.addEventListener(state => {
      if (state.isConnected && checkCallGetXml) {
        getXmlStaff()
          .then(data => {
            const parser = new XMLParser();
            let jObj = parser.parse(data);
            AsyncStorage.setItem('XmlProperty', JSON.stringify(jObj))
              .then(() =>{
                dispatchState(SetXmlProperties({data: jObj.lang.screen, isCheck: false}))
              })
              .catch(() => {
                dispatchState(SetXmlProperties({data: null, isCheck: true}));
              });
          })
          .catch(e => {
            AsyncStorage.getItem('XmlProperty')
              .then(data => {
                if (data !== null) {
                  dispatchState(
                    SetXmlProperties({data: JSON.parse(data), isCheck: true}),
                  );
                } else {
                  dispatchState(SetXmlProperties({data: null, isCheck: true}));
                }
              })
              .catch(() => {
                dispatchState(SetXmlProperties({data: null, isCheck: true}));
              });
          });
      }
      if(!state.isConnected)
      {
        dispatchState(SetConnectNetwork({show:true,connected:false}))
        return ()=>{
          unsubscribe()
        }
      }
      if(state.isConnected)
      {
        dispatchState(SetConnectNetwork({show:true,connected:true}))
        return ()=>{
          unsubscribe()
        }
      }
    })
    return ()=>{
      unsubscribe()
    }
  }, []);
  const authContext = React.useMemo(
    () => ({
      signIn: async (data, pass) => {
        if (data.status == -1) {
          Alert.alert('Delta Pos', 'Tài Khoản Đã Được Dừng Hoạt Động ', [
            {text: 'Huỷ'},
          ]);
          dispatch({type: 'SIGN_IN', token: null});
        } else {
          await AsyncStorage.setItem(
            'userToken',
            JSON.stringify({
              token: data.access_token,
              name: data.user.name,
              id: data.user.id,
              phone: data.user.phone,
              address: data.user.address,
              status: data.user.status,
              pass: pass,
              storename: data.user.storename,
              logo: data.user.logo,
              addresscompany: data.user.addresscompany,
              phonenumbercompany: data.user.phonenumbercompany,
            }),
          );
          dispatch({type: 'SIGN_IN', token: 'dummy-auth-token'});
        }
      },
      signOut: async (Notification: string) => {
        dispatchState(ResetState());
        await AsyncStorage.removeItem('userToken');
        dispatch({type: 'SIGN_OUT', token: null});
        if (Notification !== '') {
          dispatchState(
            SetShowModalIsNotification({
              description: Notification,
              showValue: true,
              functionConfirm: '',
              functionCancel: 'handleCloseModalNotification',
              valueBtnConfirm: '',
              valueBtnCancel: '',
            }),
          );
        }
      },
    }),
    [],
  );
  return (
    <AuthContext.Provider value={authContext}>
      <NavigationContainer ref={navigationRef}>
        <Stack.Navigator screenOptions={screenOptions}>
          {stateToken.userToken == null ? (
            <>
              {isLoading ? (
                <Stack.Screen name="Loading" component={LoadingApp} />
              ) : (
                <>
                  <Stack.Screen name="ScreenLogin" component={ScreenLogin} />
                  <Stack.Screen name="ContactFormScreen" component={ContactFormScreen}/>
                </>
              )}
            </>
          ) : (
            <>
              <Stack.Screen name="DrawerScreen" component={DefaultLayoutDrawer}/>
              <Stack.Screen name="CancelInvoicePrintedOrder" component={CancelInvoicePrintedOrder}/>
              <Stack.Screen name="PaymentScreen" component={PaymentScreen} />
              <Stack.Screen name="ProductScreen" component={ProductScreen} />
              <Stack.Screen name="PrintDetailView" component={PrintDetailView} />
            </>
          )}
        </Stack.Navigator>
      </NavigationContainer>
    </AuthContext.Provider>
  );
};
export {AuthContext};
export default DefaultLayoutScreen;
