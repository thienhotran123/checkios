import React from 'react';
import {createDrawerNavigator} from '@react-navigation/drawer';
import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { colors, widthScreen } from '../Utils/Styles';
import { CustomDrawer } from '../ScreenDefault/Screens';
import { HomeScreen ,HistoryPayment ,DayReport ,SettingScreenDraw} from '../ScreenDefault/Screens'
const DefaultLayoutDrawer = () => {
  const Drawer = createDrawerNavigator();
  return (
    <Drawer.Navigator
      drawerContent={props => <CustomDrawer {...props} />}
      screenOptions={{
      headerShown: false ,
      drawerActiveBackgroundColor:colors.btnColor,
      drawerInactiveTintColor:'#333',
      drawerActiveTintColor:'#fff',
      drawerLabelStyle:{marginLeft: - 20 , fontSize:widthScreen*0.036}
      }}>
      <Drawer.Screen name="Trang chủ" component={HomeScreen} options={{drawerIcon: ({color}) => ( <Ionicons name='home-outline' size={widthScreen*0.06} color={color}/>), }} />
      <Drawer.Screen name="Lịch sử thanh toán" component={HistoryPayment} options={{drawerIcon: ({color}) => ( <MaterialIcons name='history' size={widthScreen*0.06} color={color}/>),}} />
      <Drawer.Screen name="Báo cáo hôm nay" component={DayReport} options={{drawerIcon: ({color}) => (<MaterialIcons name='text-snippet' size={widthScreen*0.06} color={color}/>),}}/>
      <Drawer.Screen name="Thiết lập" component={SettingScreenDraw} options={{drawerIcon: ({color}) => ( <Ionicons name='settings-outline' size={widthScreen*0.06} color={color}/> )}} />
    </Drawer.Navigator>
  )
}

export default DefaultLayoutDrawer
