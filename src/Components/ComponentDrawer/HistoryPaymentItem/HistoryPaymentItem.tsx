import {Text, TouchableOpacity, View, LogBox} from 'react-native';
import React from 'react';
import {HistoryPaymentItemCss} from './HistoryPaymentItemCss';
import {useDispatch, useSelector} from 'react-redux';
import {SetShowDetailPaymentHistory} from '../../../Redux/Slide';
import { styleShadow, widthScreen} from '../../../Utils/Styles';
import {RoundOff} from '../../../Services/Services';
import { RootState } from '../../../Redux/Storage';
import { getTimeClock, getTimeConverter, xmlChangeText } from '../../../Utils/Helper';
LogBox.ignoreLogs(['Warning: ...']);
LogBox.ignoreAllLogs();
const HistoryPaymentItem = ({item}) => {
  const dispatch = useDispatch();
  const dataJson = JSON.parse(item.items);
  const XmlProperties = useSelector((state: RootState) => state.item.XmlProperties);
  const HistoryPaymentItemXml = XmlProperties !== null ? XmlProperties[6] : null;
  const handleDetailPayment = () => {
    dispatch(
      SetShowDetailPaymentHistory({
        tableName: dataJson.tableName,
        valuetotal: item.valuetotal,
        item: dataJson.item,
        show: true,
        discountPayment:dataJson.discountPayment
      }))
  };
  return (
    <View style={{flex: 1}}>
      <View
        style={{
          paddingVertical: 10,
          flexDirection: 'row',
          justifyContent: 'space-between',
          alignItems: 'center',
        }}>
        <Text style={[HistoryPaymentItemCss.TextDay , {color:xmlChangeText(HistoryPaymentItemXml , 'HistoryPaymentItem.ColorTextDay' ,'#888888' )}]}>
          {getTimeConverter(item.created_at)}
        </Text>
        <Text style={[HistoryPaymentItemCss.TextIdPaymentRd ,{color:xmlChangeText(HistoryPaymentItemXml , 'HistoryPaymentItem.ColorTextIdPaymentRd' ,'#0066CC' )}]}>
        Hoá đơn số : {item.id}
        </Text>
      </View>
      <View style={[HistoryPaymentItemCss.card, styleShadow]}>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            width: '100%',
            alignItems: 'center',
          }}>
          <Text numberOfLines={1} style={[HistoryPaymentItemCss.heading,{color:xmlChangeText(HistoryPaymentItemXml , 'HistoryPaymentItem.ColorTextHeading' ,'#888888')}]}>
            {'Bàn ' + dataJson.tableName}
          </Text>
          <View style={HistoryPaymentItemCss.headingId}>
            <Text numberOfLines={1} style={[HistoryPaymentItemCss.headingIdText,{color:xmlChangeText(HistoryPaymentItemXml , 'HistoryPaymentItem.ColorTextHeadingIdText' ,'#888888')}]}>
            {getTimeClock(item.created_at)}
            </Text>
          </View>
        </View>
        <Text numberOfLines={2} style={[HistoryPaymentItemCss.bodyText,{color:xmlChangeText(HistoryPaymentItemXml , 'HistoryPaymentItem.ColorTextReason' ,'#888888')}]}>
          {item.reason == '' || item.reason === '0'
            ? ``
            : `Ghi chú: ${item.reason}`}
        </Text>
        <View style={HistoryPaymentItemCss.footerBtn}>
          <View style={HistoryPaymentItemCss.footerBtnDetail}>
            <Text
              style={{
                fontSize: widthScreen * 0.04,
                color: xmlChangeText(HistoryPaymentItemXml , 'HistoryPaymentItem.ColorTextTotalPrice' ,'#0066CC'),
                fontWeight: '700',
              }}
              numberOfLines={1}>
              Tổng tiền: {RoundOff(item.valuetotal)} VND
            </Text>
          </View>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'flex-end',
              alignItems: 'center',
            }}>
            <TouchableOpacity onPress={handleDetailPayment}>
              <Text style={[HistoryPaymentItemCss.footerBtnText,{color:xmlChangeText(HistoryPaymentItemXml , 'HistoryPaymentItem.ColorTextBtnShowDetail' ,'#0066CC')}]}>
                Xem chi tiết
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </View>
  );
};

export default React.memo(HistoryPaymentItem);
