import {StyleSheet} from 'react-native';
import {colors, widthScreen} from '../../../Utils/Styles';
export const HistoryPaymentItemCss = StyleSheet.create({
  card: {
    backgroundColor: '#fff',
    borderRadius: 8,
    paddingVertical: 26,
    paddingHorizontal: 26,
    width: '100%',
    marginVertical: 10,
  },
  heading: {
    fontSize: widthScreen * 0.04,
    fontWeight: '700',
    width: '78%',
  },
  headingId: {
    paddingHorizontal: 0,
  },
  headingIdText: {
    fontSize: widthScreen * 0.04,
    fontWeight: '700',
  },
  shadowProp: {
    elevation: 4,
    shadowColor: '#171717',
    shadowOffset: {width: -2, height: 4},
    shadowOpacity: 0.2,
    shadowRadius: 3,
  },
  TextDay: {fontWeight: '700', fontSize: widthScreen * 0.04},
  TextIdPaymentRd: {
    fontWeight: '700',
    fontSize: widthScreen * 0.04,
  },
  bodyTextbodyText: {
    flex: 1,
  },
  bodyText: {
    fontSize: widthScreen * 0.04,
    fontWeight: '700',
    paddingVertical: 12,
  },
  footerBtn: {
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'row',
    width: '100%',
  },
  footerBtnText: {
    fontSize: widthScreen * 0.04,
    fontWeight: '700',
  },
  footerBtnDetail: {
    flex: 1,
  },
  footerBtnDelete: {
    marginLeft: 20,
  },
  footerBtnTextDelete: {
    color: '#FF9900',
    fontSize: widthScreen * 0.04,
    fontWeight: '600',
  },
});
