import {
  ScrollView,
  Text,
  TouchableOpacity,
  View,
  LogBox,
  Platform,
} from 'react-native';
import React from 'react';
import {useSelector, useDispatch} from 'react-redux';
import {SetCloseDetailPaymentHistory} from '../../../Redux/Slide';
import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import {colors, widthScreen} from '../../../Utils/Styles';
import {scale, verticalScale} from '../../../Utils/Reponsive';
import {DetailPaymentHistoryCss} from './DetailPaymentHistoryCss';
import {RootState} from '../../../Redux/Storage';
import {ItemPaymentDetail} from '../../Components';
import DeviceInfo from 'react-native-device-info';
import { RoundOff, TotalMoney, TotalVat } from '../../../Services/Services';
LogBox.ignoreLogs(['Warning: ...']);
LogBox.ignoreAllLogs();
const DetailPaymentHistory = ({navigation}) => {
  const DetailItemPaymentOld = useSelector((state: RootState) => state.counter.DetailItemPaymentOld,);
  const dispatch = useDispatch();
  const handelCloseDetail = () => {
    dispatch(SetCloseDetailPaymentHistory());
  };
  const totalValue =TotalMoney(DetailItemPaymentOld.item)
  const totalVat =TotalVat(DetailItemPaymentOld.item)
  const handlePrinterDetail = () => {
    navigation.navigate('PrintDetailView', {
      tableName: DetailItemPaymentOld.tableName ,
      item: DetailItemPaymentOld.item,
      nameDetail: 'DetailPaymentHistory',
      discount: DetailItemPaymentOld.discountPayment,
      NameCustomer: '',
      PhoneCustomer: '',
      PointCustomer: '',
    });
  };
  return (
    <View style={DetailPaymentHistoryCss.centeredView}>
      <View style={{flex: 1}}>
        <View style={DetailPaymentHistoryCss.ModalHeader}>
          <TouchableOpacity
            onPress={handelCloseDetail}
            style={DetailPaymentHistoryCss.ModalHeaderBtn}>
            <Ionicons
              name="close-outline"
              size={widthScreen * 0.12}
              color={colors.IconColor}
            />
          </TouchableOpacity>
          <View
            style={{
              display: 'flex',
              alignItems: 'center',
              flexDirection: 'row',
              flex: 1,
              justifyContent: 'flex-end',
            }}>
            <Text
              style={DetailPaymentHistoryCss.ModalHeaderTextTb}
              numberOfLines={1}>
              { `Bàn ${DetailItemPaymentOld.tableName}`}
            </Text>
          </View>
        </View>
        <ScrollView
          style={{flex: 1, marginTop: 8, backgroundColor: '#F5F6F8'}}
          showsVerticalScrollIndicator={false}>
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'space-between',
            }}>
            <View
              style={{
                backgroundColor: '#fff',
                width: scale(120),
                marginBottom: 10,
                height: verticalScale(40),
                marginTop: 10,
                marginLeft: 8,
                borderRadius: 10,
                borderWidth: 2,
                borderColor: '#EEEEEE',
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Text
                style={{
                  color: '#333',
                  fontWeight: '700',
                  fontSize: widthScreen * 0.036,
                }}>
                {DetailItemPaymentOld.tableName !== undefined
                  ? 'Tại chỗ'
                  : 'Mang về'}
              </Text>
            </View>
            <TouchableOpacity onPress={handlePrinterDetail}>
              <Ionicons
                name="print-outline"
                style={{paddingHorizontal: 10}}
                size={widthScreen * 0.11}
                color={colors.IconColor}
              />
            </TouchableOpacity>
          </View>
          <View style={{width: '100%'}}>
            {DetailItemPaymentOld.item.map((item, index) => (
              <ItemPaymentDetail item={item} index={index} key={index} />
            ))}
          </View>
        </ScrollView>
      </View>
      {DetailItemPaymentOld.item.length <= 0 ? (
        <TouchableOpacity style={{position: 'absolute', bottom: 12, right: 12}}>
          <MaterialIcons
            name="add-circle"
            size={widthScreen * 0.14}
            color={colors.IconColor}
          />
        </TouchableOpacity>
      ) : (
        ''
      )}
      {DetailItemPaymentOld.item.length > 0 ? (
        <View
          style={
            Platform.OS == 'ios'
              ? {
                  paddingTop: 10,
                  paddingBottom: DeviceInfo.hasNotch() ? 28 : 10,
                  paddingHorizontal: 10,
                  alignItems: 'center',
                  width: '100%',
                }
              : {
                  padding: 10,
                  alignItems: 'center',
                  width: '100%',
                }
          }>
          <View style={DetailPaymentHistoryCss.footerTotal}>
            <View
              style={{
                display: 'flex',
                flex: 1,
              }}>
              <Text
                numberOfLines={1}
                style={DetailPaymentHistoryCss.footerTotalText}>
                Tổng hoá đơn: {RoundOff(totalValue)} VND
              </Text>
              <Text
                numberOfLines={1}
                style={DetailPaymentHistoryCss.footerTotalText}>
                Tổng VAT: {RoundOff(totalVat)} VND
              </Text>
              <Text
                numberOfLines={1}
                style={DetailPaymentHistoryCss.footerTotalText}>
                Giảm giá: {RoundOff(DetailItemPaymentOld.discountPayment)} VND
              </Text>
              <Text
                numberOfLines={1}
                style={DetailPaymentHistoryCss.footerTotalText}>
                Khách trả: {RoundOff(DetailItemPaymentOld.valuetotal)} VND
              </Text>
            </View>
          </View>
        </View>
      ) : (
        ''
      )}
    </View>
  );
};

export default DetailPaymentHistory;
