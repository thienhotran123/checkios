import {StyleSheet} from 'react-native';
import {widthScreen} from '../../../Utils/Styles';
import {verticalScale} from '../../../Utils/Reponsive';
export const CreateCustomerCss = StyleSheet.create({
  ModalOptionContainer: {
    position: 'absolute',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(0,0,0,0.6)',
    zIndex: 13,
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
  },
  ModalOption: {
    backgroundColor: 'rgba(255,255,255,1)',
    width: '80%',
    padding: 10,
    borderRadius: 10,
    height: verticalScale(320),
    zIndex: 10,
  },
  ModalOptionPrint: {
    backgroundColor: 'rgba(255,255,255,1)',
    width: '80%',
    padding: 10,
    borderRadius: 10,
    height: widthScreen * 0.6,
    zIndex: 10,
  },
  ModalOptionPayment: {
    backgroundColor: 'rgba(255,255,255,1)',
    width: '80%',
    padding: 10,
    borderRadius: 10,
    height: widthScreen * 0.76,
    zIndex: 10,
  },
  ModalOptionBtnTable: {
    width: '100%',
    alignItems: 'flex-end',
    justifyContent: 'flex-end',
    flexDirection: 'row',
    paddingBottom: 6,
  },
  IconClose: {},
  ModalOptionBtn: {
    paddingHorizontal: 10,
  },
  ModalOptionBtnText: {
    textAlign: 'center',
    fontSize: widthScreen * 0.042,
    fontWeight: '700',
  },
  ModalOptionBtnTextError: {
    textAlign: 'center',
    color: '#CCCCCC',
    fontSize: widthScreen * 0.042,
    fontWeight: '700',
  },
  textStyleModal: {
    fontSize: widthScreen * 0.044,
    textAlign: 'center',
    fontWeight: '700',
    color: '#666666',
  },
  BodyInput: {
    flex: 1,
    justifyContent:'center',
  },
  BodyInputName: {
    width: '100%',
    height: '100%',
    padding: 10,
    color: '#666666',
  },
  BodyInputPhone: {
    width: '100%',
    height: '100%',
    padding: 10,
    color: '#666666',
  },
  BodyName: {
    width: '100%',
    height: verticalScale(66),
    borderRadius: 8,
    borderColor: '#DDDDDD',
    borderWidth: 2,
    marginVertical: 5,
  },
  BodyPhone: {
    width: '100%',
    height: verticalScale(66),
    borderRadius: 8,
    borderColor: '#DDDDDD',
    borderWidth: 2,
    marginVertical: 5,
  },
  ErrorContainer:{
    paddingHorizontal:10
  },
  ErrorText:{
    color:'red',
    fontSize:14,
    fontWeight:'600'
  }
});
