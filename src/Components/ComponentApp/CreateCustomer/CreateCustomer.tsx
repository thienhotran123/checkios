import {
  KeyboardAvoidingView,
  Platform,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import React, {useState} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {CreateCustomerCss} from './CreateCustomerCss';
import {
  SetShowLoading,
  SetShowModalCreateCustomer,
  SetShowModalIsNotification,
} from '../../../Redux/Slide';
import {AuthenticationError, RegexReason, regexNumber, regexString} from '../../../Utils/Helper';
import {CreateCustomerReward} from '../../../Utils/GetApi';
import {RootState} from '../../../Redux/Storage';
import {ResponsiveMessage} from '../../../Utils/MessageApi';
import {
  CreateCustomerSuccess,
  CustomerExist,
  LoginAgain,
  errorNotification,
} from '../../../Utils/MessageNotification';
import { AuthContext } from '../../../DefaultLayout/DefaultLayoutScreen';
const CreateCustomer = ({handlePushItemCustomer}:any) => {
  const {signOut} = React.useContext(AuthContext);
  const dispatch = useDispatch();
  const UserName = useSelector((state: RootState) => state.counter.UserName);
  const [PhoneCustomer, SetPhoneCustomer] = useState<{
    Text: string;
    error: string;
  }>({
    Text: '',
    error: '',
  });
  const [NameCustomer, SetNameCustomer] = useState<{
    Text: string;
    error: string;
  }>({Text: '', error: ''});
  const handleChangeText = (text: string, value: string) => {
    switch (value) {
      case 'name':
        SetNameCustomer({
          Text: text,
          error:
            text !== ''
              ? regexString.test(text) && !RegexReason.test(text)
                ? ''
                : 'Chỉ Được Phép Nhập Kí Tự'
              : 'Vui lòng nhập trường này',
        });
        return;
      case 'phone':
        SetPhoneCustomer({
          Text: text,
          error:
            text !== ''
              ? regexNumber.test(text) && /^\d+$/.test(text)
                ? ''
                : 'Số Điện Thoại Không Đúng Định Dạng'
              : 'Vui lòng nhập trường này',
        });
        return;
    }
  };
  const handleCreateCustomer = () => {
    if (NameCustomer.Text.length <= 0 || PhoneCustomer.Text.length <= 0) {
      NameCustomer.Text.length <= 0
        ? SetNameCustomer({Text: '', error: 'Vui lòng nhập trường này'})
        : '';
      PhoneCustomer.Text.length <= 0
        ? SetPhoneCustomer({Text: '', error: 'Vui lòng nhập trường này'})
        : '';
      return;
    }
    dispatch(SetShowModalCreateCustomer(false));
    dispatch(SetShowLoading(true));
    CreateCustomerReward({
      token: UserName.token,
      name: NameCustomer.Text,
      phone: PhoneCustomer.Text,
    })
      .then(data => {
        if(AuthenticationError(data.message , data.status_code))
        {
          signOut(LoginAgain)
          return
        }
        if (data.message == ResponsiveMessage.CustomerExist) {
          dispatch(
            SetShowModalIsNotification({
              description: CustomerExist,
              showValue: true,
              functionConfirm: '',
              functionCancel: 'handleCloseModalNotification',
              valueBtnConfirm: '',
              valueBtnCancel: '',
            }),
          );
          return;
        }
        if (data.status_code == 200) {
          dispatch(
            SetShowModalIsNotification({
              description: CreateCustomerSuccess,
              showValue: true,
              functionConfirm: '',
              functionCancel: 'handleCloseModalNotification',
              valueBtnConfirm: '',
              valueBtnCancel: '',
            }),
          );
          if (handlePushItemCustomer !== undefined) {
            handlePushItemCustomer({
              name: data.customer.name,
              phone: data.customer.phone,
              point: 0,
            });
          }
        }
        else {
          dispatch(
            SetShowModalIsNotification({
              description: data.message,
              showValue: true,
              functionConfirm: '',
              functionCancel: 'handleCloseModalNotification',
              valueBtnConfirm: '',
              valueBtnCancel: '',
            }),
          );
        }
      })
      .catch(e => {
        dispatch(
          SetShowModalIsNotification({
            description: errorNotification,
            showValue: true,
            functionConfirm: '',
            functionCancel: 'handleCloseModalNotification',
            valueBtnConfirm: '',
            valueBtnCancel: '',
          }),
        );
      })
      .finally(() => {
        dispatch(SetShowLoading(false));
      });
  };
  const handleCloseModalCustomer = () => {
    dispatch(SetShowModalCreateCustomer(false));
  };
  const handleBlur = (value: string) => {
    switch (value) {
      case 'name':
        NameCustomer.Text.length <= 0
          ? SetNameCustomer({Text: '', error: 'Vui lòng nhập trường này'})
          : '';
        return;
      case 'phone':
        PhoneCustomer.Text.length <= 0
          ? SetPhoneCustomer({Text: '', error: 'Vui lòng nhập trường này'})
          : '';
        return;
    }
  };
  return (
    <KeyboardAvoidingView
      behavior={Platform.OS === 'ios' ? 'padding' : 'padding'}
      style={CreateCustomerCss.ModalOptionContainer}>
      <View style={CreateCustomerCss.ModalOption}>
        <View style={{flex: 1}}>
          <View style={{alignItems: 'center', paddingTop: 10}}>
            <Text style={CreateCustomerCss.textStyleModal}>Tạo khách hàng</Text>
          </View>
          <View style={CreateCustomerCss.BodyInput}>
            <View style={CreateCustomerCss.BodyName}>
              <TextInput
                placeholderTextColor="#999999"
                style={CreateCustomerCss.BodyInputName}
                placeholder="Nhập tên khách hàng"
                onChangeText={text => handleChangeText(text, 'name')}
                onBlur={() => handleBlur('name')}
              />
            </View>
            {NameCustomer.error !== '' ? (
              <View style={CreateCustomerCss.ErrorContainer}>
                <Text style={CreateCustomerCss.ErrorText}>
                  {NameCustomer.error}
                </Text>
              </View>
            ) : (
              ''
            )}
            <View style={CreateCustomerCss.BodyPhone}>
              <TextInput
                onChangeText={text => handleChangeText(text, 'phone')}
                placeholderTextColor="#999999"
                style={CreateCustomerCss.BodyInputPhone}
                placeholder="Nhập số điện thoại khách hàng"
                onBlur={() => handleBlur('phone')}
                keyboardType='numeric'
              />
            </View>
            {PhoneCustomer.error !== '' ? (
              <View style={CreateCustomerCss.ErrorContainer}>
                <Text style={CreateCustomerCss.ErrorText}>
                  {PhoneCustomer.error}
                </Text>
              </View>
            ) : (
              ''
            )}
          </View>
          <View style={CreateCustomerCss.ModalOptionBtnTable}>
            <TouchableOpacity
              disabled={
                NameCustomer.error !== '' || PhoneCustomer.error !== ''
                  ? true
                  : false
              }
              onPress={() => handleCreateCustomer()}
              style={CreateCustomerCss.ModalOptionBtn}>
              <Text
                style={[
                  CreateCustomerCss.ModalOptionBtnText,
                  NameCustomer.error !== '' || PhoneCustomer.error !== ''
                    ? {color: '#888888'}
                    : {color: '#4876FF'},
                ]}>
                Đồng ý
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => handleCloseModalCustomer()}
              style={CreateCustomerCss.ModalOptionBtn}>
              <Text
                style={[
                  CreateCustomerCss.ModalOptionBtnText,
                  {color: '#4876FF'},
                ]}>
                Huỷ
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </KeyboardAvoidingView>
  );
};

export default CreateCustomer;
