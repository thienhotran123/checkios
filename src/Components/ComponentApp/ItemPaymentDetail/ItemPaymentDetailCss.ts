import { StyleSheet } from 'react-native'
import { colors, widthScreen } from '../../../Utils/Styles'
import { scale } from '../../../Utils/Reponsive'
export const ItemPaymentDetailCss = StyleSheet.create({
    ItemCartOrder: {
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        borderRadius:4,
        paddingHorizontal:10,
        paddingVertical:10,
        backgroundColor:'#FFFFFF',
        borderBottomColor:'#EEEEEE' ,
        borderBottomWidth:4
    },
    ItemCartOrderValid: {
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingHorizontal:10,
        paddingVertical:10,
        backgroundColor: "rgba(60, 179, 113,0.4)",
        borderRadius:4,
        borderBottomColor:'#EEEEEE' ,
        borderBottomWidth:4
    },
    ItemCartOrderImg: {
        width: scale(60),
        height:scale(60),
        borderRadius: 6
    },
    ItemCartOrderName: {
        paddingHorizontal:6,
       fontSize: 18,
        width: '30%',
        fontWeight: '600',

    },
    ItemCartOrderNameText:{
        fontSize: widthScreen * 0.042,
        width: '100%',
        fontWeight:'700',
        color:'#333'
    },
    ItemCartOrderPrice: {
        justifyContent:'center',
        alignItems:'center',
        width: '30%',

    },
    ItemCartOrderPriceText:{
        fontSize: widthScreen * 0.04,
        fontWeight: '600',
        color:'#333'
    },
    ItemCartOrderBtnUpAndDown: {
        width: '22%',
    },
    ItemCartOrderBtnUpAndDownMain:{
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        width: '100%',
    },
    ItemCartOrderBtnUp: {

    },

    ItemCartOrderBtnUpText: {
        color: colors.IconColor,
        fontWeight: '600'
    },
    ItemCartOrderBtnTextQty: {
        fontSize: 18,
        alignItems: 'center',
        flex: 1,

    },
    ItemCartOrderBtnText: {
        fontSize: widthScreen * 0.04,
        color:'#333',
        fontWeight: '600'
    },
    ItemCartOrderBtnDown: {

    },
    ItemCartOrderBtnDownText: {
        color: colors.IconColor,
        fontWeight: '600'
    },
    ItemCartOrderBtnDownTextPrintOld:{
        color: colors.gray,
        fontWeight: '600'
    },
    ItemQuantity:{
        width:'10%',
        justifyContent:'center',
        alignItems:'center',
    } ,
    ItemVatCss:{
        width:'10%',
        justifyContent:'center',
        alignItems:'center',
    },
    ItemImageContainerCss:{
        width:'20%',
        justifyContent:'center',
        alignItems:'center',
    }
})