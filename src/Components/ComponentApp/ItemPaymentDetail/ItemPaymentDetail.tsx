import {Image, Text, View} from 'react-native';
import React from 'react';
import {ItemPaymentDetailCss} from './ItemPaymentDetailCss';
import { CommaSeparatedValues } from '../../../Services/Services';
const ItemPaymentDetail = ({item, index}) => {
  return (
    <View
      style={
        item.status == 2
          ? ItemPaymentDetailCss.ItemCartOrderValid
          : ItemPaymentDetailCss.ItemCartOrder
      }>
      <View style={ItemPaymentDetailCss.ItemImageContainerCss}>
        <Image
          source={{uri: item.image}}
          style={ItemPaymentDetailCss.ItemCartOrderImg}
        />
      </View>
      <View style={ItemPaymentDetailCss.ItemCartOrderName}>
        <Text
          numberOfLines={2}
          style={ItemPaymentDetailCss.ItemCartOrderNameText}>
          {item.title}
        </Text>
      </View>
      <View style={ItemPaymentDetailCss.ItemCartOrderPrice}>
        <Text
          numberOfLines={1}
          style={ItemPaymentDetailCss.ItemCartOrderPriceText}>
          {CommaSeparatedValues(item.price)}
        </Text>
      </View>
      <View style={ItemPaymentDetailCss.ItemVatCss}>
        <Text
          numberOfLines={1}
          style={ItemPaymentDetailCss.ItemCartOrderPriceText}>
          {item.vat}%
        </Text>
      </View>
      <View style={ItemPaymentDetailCss.ItemQuantity}>
        <Text
          numberOfLines={1}
          style={ItemPaymentDetailCss.ItemCartOrderBtnText}>
          {item.quantity}
        </Text>
      </View>
    </View>
  );
};

export default ItemPaymentDetail;
