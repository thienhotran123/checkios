import { StyleSheet } from 'react-native'
export const LoadingComponentCss = StyleSheet.create({
    Loading: {
        position: 'absolute',
        left: 0,
        right: 0,
        bottom: 0,
        top: 0,
        backgroundColor: 'rgba(0,0,0,0.6)',
      },
})