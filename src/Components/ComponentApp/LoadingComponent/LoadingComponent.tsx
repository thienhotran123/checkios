import {ActivityIndicator, View, LogBox} from 'react-native';
import React from 'react';
import {LoadingComponentCss} from './LoadingConponentCss';
LogBox.ignoreLogs(['Warning: ...']);
LogBox.ignoreAllLogs();
const LoadingComponent = () => {
  return (
    <View style={LoadingComponentCss.Loading}>
      <ActivityIndicator
        size={42}
        color="#0099FF"
        style={{alignItems: 'center', justifyContent: 'center', flex: 1}}
      />
    </View>
  );
};
export default LoadingComponent;
