import {Text, View} from 'react-native';
import React from 'react';
import {ItemProductCss} from './ItemProductCss';
import {TouchableOpacity} from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import {Image} from 'react-native';
import {colors, widthScreen} from '../../../Utils/Styles';
import {useSelector} from 'react-redux';
import {RootState} from '../../../Redux/Storage';
import {xmlChangeText} from '../../../Utils/Helper';
import {scale} from '../../../Utils/Reponsive';
const ItemProduct = ({
  item,
  index,
  onIncrease,
  onReduceItem,
  onDeleteQtyItem,
}) => {
  const XmlProperties = useSelector(
    (state: RootState) => state.item.XmlProperties,
  );
  const ItemProductXml = XmlProperties !== null ? XmlProperties[3] : null;
  return (
    <TouchableOpacity
      onLongPress={() => onDeleteQtyItem(index, item)}
      onPress={() => onIncrease(index, item)}
      style={[
        ItemProductCss.ScrollProductItemContainer,
        item.quantity > 0
          ? ItemProductCss.containerItemProductCheck
          : ItemProductCss.containerItemProductNotCheck,
      ]}>
      <View
        style={[
          ItemProductCss.ScrollBodyImg,
          {
            width: scale(
              xmlChangeText(ItemProductXml, 'ItemProduct.Width', 60),
            ),
            height: scale(
              xmlChangeText(ItemProductXml, 'ItemProduct.Height', 60),
            ),
          },
        ]}>
        <Image source={{uri: item.image}} style={ItemProductCss.imgStyles} />
        {item.quantity > 0 ? (
          <View style={ItemProductCss.coatingImg}>
            <MaterialIcons
              name="check-circle"
              color={colors.IconColor}
              size={widthScreen * 0.06}
            />
          </View>
        ) : (
          ''
        )}
      </View>
      <View style={ItemProductCss.ItemText}>
        <Text style={[ItemProductCss.ItemTextName,{color:xmlChangeText(ItemProductXml , 'ItemProduct.ColorNameProduct', '#0066CC')}]} numberOfLines={2}>
          {item.title}
        </Text>
        <Text style={[ItemProductCss.ItemTextPrice,{color:xmlChangeText(ItemProductXml , 'ItemProduct.ColorNamePrice', '#333')}]}>
          {item.price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',')} VND
        </Text>
        <Text style={[ItemProductCss.ItemTextVatNot,{color:xmlChangeText(ItemProductXml , 'ItemProduct.ColorVatText', '#777777')}]}>
          {xmlChangeText(
            ItemProductXml,
            'ItemProduct.TextVatNot',
            '(Chưa bao gồm thuế)',
          )}
        </Text>
      </View>
      <View style={ItemProductCss.BtnItemFunc}>
        {item.quantity > 0 ? (
          <TouchableOpacity onPress={() => onReduceItem(index, item)}>
            <Ionicons
              name="remove-circle"
              size={widthScreen * 0.068}
              color="#0066CC"
            />
          </TouchableOpacity>
        ) : (
          ''
        )}
        <Text style={ItemProductCss.BtnItemFuncText}>x{item.quantity}</Text>
        {item.quantity > 0 ? (
          <TouchableOpacity onPress={() => onIncrease(index, item)}>
            <Ionicons
              name="add-circle"
              size={widthScreen * 0.068}
              color="#0066CC"
            />
          </TouchableOpacity>
        ) : (
          ''
        )}
      </View>
    </TouchableOpacity>
  );
};

export default React.memo(ItemProduct);
