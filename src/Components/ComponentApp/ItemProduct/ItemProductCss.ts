import {StyleSheet} from 'react-native';
import {scale, verticalScale} from '../../../Utils/Reponsive';
import {colors, widthScreen} from '../../../Utils/Styles';
export const ItemProductCss = StyleSheet.create({
  ScrollProductItemContainer: {
    width: '100%',
    height: verticalScale(100),
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: '#fff',
  },
  ScrollBodyImg: {
    position: 'relative',
    alignItems: 'center',
    justifyContent: 'center',
    marginLeft: 10,
  },
  ItemText: {flex: 1, marginLeft: 10},
  ItemTextName: {
    fontWeight: '700',
    fontSize: widthScreen * 0.042,
  },
  ItemTextPrice: {
    fontWeight: '700',
    fontSize: widthScreen * 0.032,
  },
  BtnItemFunc: {
    display: 'flex',
    alignItems: 'center',
    flexDirection: 'row',
    marginRight: 10,
  },
  BtnItemFuncText: {
    fontSize: widthScreen * 0.046,
    fontWeight: '700',
    paddingHorizontal: 8,
    color: '#333',
  },
  coatingImg: {
    position: 'absolute',
    backgroundColor: 'rgba(0,0,0,0.5)',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 6,
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
  },
  imgStyles: {width: '100%', height: '100%', borderRadius: 6},
  containerItemProductCheck: {
    backgroundColor: '#fff',
    borderBottomColor: '#EEEEEE',
    borderBottomWidth: 3,
  },
  containerItemProductNotCheck: {
    borderBottomColor: '#EEEEEE',
    borderBottomWidth: 3,
  },
  ItemTextVatNot: {
    fontSize: widthScreen * 0.032,
    fontWeight: '600',
  },
});
