import {Text, View} from 'react-native';
import React from 'react';
import {ItemPrintDetailViewCss} from './ItemPrintDetailViewCss';
import { CommaSeparatedValues, TotalMoneyWithVat } from '../../../Services/Services';
const ItemPrintDetailView = ({item}) => {
  return (
    <View style={ItemPrintDetailViewCss.BodyBillContainer}>
      <View style={{width: '30%'}}>
        <Text style={ItemPrintDetailViewCss.BodyBillTitle}>{item.title}</Text>
      </View>
      <View style={{width: '10%'}}>
        <Text style={ItemPrintDetailViewCss.BodyBillQuantity}>
          {item.qtyStatus !==undefined ? item.quantity + item.qtyStatus : item.quantity}
        </Text>
      </View>
      <View style={{width: '25%'}}>
        <Text style={ItemPrintDetailViewCss.BodyBillQuantity}>
          {CommaSeparatedValues(item.price)}
        </Text>
      </View>
      <View style={{width: '10%'}}>
        <Text style={ItemPrintDetailViewCss.BodyBillPrice}>{item.vat}%</Text>
      </View>
      <View style={{width: '25%'}}>
        <Text style={ItemPrintDetailViewCss.BodyBillTotal}>
          {CommaSeparatedValues(TotalMoneyWithVat(item))}
        </Text>
      </View>
    </View>
  );
};

export default React.memo(ItemPrintDetailView);
