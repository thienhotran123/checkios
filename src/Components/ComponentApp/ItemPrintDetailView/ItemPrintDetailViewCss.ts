import {StyleSheet} from 'react-native';
export const ItemPrintDetailViewCss = StyleSheet.create({
    BodyBillContainer: {flexDirection: 'row', alignItems: 'center', marginTop: 8},
    BodyBillTitle: {fontSize: 16, fontWeight: '600', color: '#333'},
    BodyBillQuantity: {
      textAlign: 'center',
      fontSize: 16,
      fontWeight: '600',
      color: '#333',
    },
    BodyBillPrice: {
      textAlign: 'center',
      fontSize: 16,
      fontWeight: '600',
      color: '#333',
    },
    BodyBillTotal: {
      textAlign: 'right',
      fontSize: 16,
      fontWeight: '600',
      color: '#333',
    },
});
