import {StyleSheet} from 'react-native';
export const LoadingAppCss = StyleSheet.create({
  LoadingProduct: {
    flex: 1,
  },
  ActivityIndicatorCss: {
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
  },
});
