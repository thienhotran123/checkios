import {ActivityIndicator, View, LogBox} from 'react-native';
import React from 'react';
import {LoadingAppCss} from './LoadingAppCss';
LogBox.ignoreLogs(['Warning: ...']);
LogBox.ignoreAllLogs();
const LoadingApp = () => {
  return (
    <View style={LoadingAppCss.LoadingProduct}>
      <ActivityIndicator
        size={42}
        color="#0099FF"
        style={LoadingAppCss.ActivityIndicatorCss}
      />
    </View>
  );
};

export default LoadingApp;
