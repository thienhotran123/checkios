import { StyleSheet} from 'react-native';
import {  widthScreen} from '../../../Utils/Styles';
export const ModalNotificationCss = StyleSheet.create({
  ModalOpitionContainer: {
    position: 'absolute',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(0,0,0,0.6)',
    zIndex: 13,
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
  },
  ModalOpition: {
    backgroundColor: 'rgba(255,255,255,1)',
    width: '80%',
    padding: 10,
    borderRadius: 10,
    height: widthScreen * 0.4,
    zIndex: 10,
  },
  ModalOpitionPrint: {
    backgroundColor: 'rgba(255,255,255,1)',
    width: '80%',
    padding: 10,
    borderRadius: 10,
    height: widthScreen * 0.6,
    zIndex: 10,
  },
  ModalOpitionPayment: {
    backgroundColor: 'rgba(255,255,255,1)',
    width: '80%',
    padding: 10,
    borderRadius: 10,
    height: widthScreen * 0.76,
    zIndex: 10,
  },
  ModalOpitionBtnTable: {
    width: '100%',
    alignItems: 'flex-end',
    justifyContent: 'flex-end',
    flexDirection: 'row',
    paddingBottom: 6,
  },
  IconClose: {},
  ModalOpitionBtn: {
    paddingHorizontal: 10,
  },
  ModalOpitionBtnText: {
    textAlign: 'center',
    color: '#4876FF',
    fontSize: widthScreen * 0.042,
    fontWeight:'700'
  },
  ModalOpitionBtnTextError: {
    textAlign: 'center',
    color: '#CCCCCC',
    fontSize: widthScreen * 0.042,
    fontWeight:'700'
  },
  textStyleModal: {
    fontSize: widthScreen * 0.044,
    marginBottom: 18,
    textAlign:'center',
    fontWeight:'700',
    color: '#666666',
  },
});
