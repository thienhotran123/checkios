import {
  KeyboardAvoidingView,
  Platform,
  Text,
  TouchableOpacity,
  View,
  LogBox,
  NativeModules,
  BackHandler,
} from 'react-native';
import React, {useState, useMemo} from 'react';
import {ModalNotificationCss} from './ModalNotificationCss';
import {useSelector, useDispatch} from 'react-redux';
import {
  SetDeleteItemListMenu,
  SetPrintTableDetail,
  SetClearDataProductTable,
  SetShowLoading,
  SetModalPosition,
  SetUpdateTable,
  SetShowModalIsNotification,
  SetCloseModalIsNotification,
  SetRemoveTableNotExist,
  SetCloseDetailTable,
} from '../../../Redux/Slide';
import ThermalPrinterModule from 'react-native-thermal-printer';
import {TextInput} from 'react-native-paper';
import {AuthContext} from '../../../DefaultLayout/DefaultLayoutScreen';
import {
  CheckOutTable,
  UpdateTable,
  CreatePayment,
  UpdateCustomerReward,
  LogOut,
} from '../../../Utils/GetApi';
import {
  ColumnAlignment,
  COMMANDS,
  NetPrinter,
} from 'react-native-thermal-receipt-printer-image-qr';
import {widthScreen} from '../../../Utils/Styles';
import {ResponsiveMessage} from '../../../Utils/MessageApi';
import {RootState} from '../../../Redux/Storage';
import {
  AuthenticationError,
  getDateTime,
  getDay,
  getHours,
  getMinutes,
  getMonth,
  getYearReduce,
  removeVietnameseaccents,
} from '../../../Utils/Helper';
import {ProductItem} from '../../../Utils/interface/interface';
import {Success} from '../../../Utils/Enums';
import {
  CreateKitchenVouchers,
  LoginAgain,
  PaymentSuccess,
  PrintSuccess,
  TableStandingNotExist,
  UpdateTableFail,
  errorNotification,
} from '../../../Utils/MessageNotification';
import {CantConnectIP} from '../../../Utils/MessageNotification';
LogBox.ignoreLogs(['Warning: ...']);
LogBox.ignoreAllLogs();
const ModalNotification = ({
  navigation,
}: any) => {
  const {signOut} = React.useContext(AuthContext);
  const date = new Date();
  const {PingModule} = NativeModules;
  const dispatch = useDispatch();
  const DataPayment = useSelector((state: RootState) => state.counter.DataPayment);
  const UserName = useSelector((state: RootState) => state.counter.UserName);
  const DataSetting = useSelector((state: RootState) => state.item.DataSetting)
  const TableItem = useSelector((state: RootState) => state.counter.TableItem);
  const initialValueModal = useSelector((state: RootState) => state.counter.initialValueModal,);
  const [noteValue, setNoteValue] = useState<string>('');
  const BOLD_ON = COMMANDS.TEXT_FORMAT.TXT_BOLD_ON;
  const BOLD_OFF = COMMANDS.TEXT_FORMAT.TXT_BOLD_OFF;
  const CENTER = COMMANDS.TEXT_FORMAT.TXT_ALIGN_CT;
  const OFF_CENTER = COMMANDS.TEXT_FORMAT.TXT_ALIGN_LT;
  const LEFT = COMMANDS.TEXT_FORMAT.TXT_ALIGN_LT;
  const timeStamS = useMemo(() => {return `${getDay(date)}${getMonth(date)}${getYearReduce(date,2000)}${getHours(date)}${getMinutes(date)}-${Math.floor(Math.random() * 999)}`;}, []);
  //data call function modal
  const functions = {
    handelCloseSignUp: () => {
      dispatch(SetCloseModalIsNotification());
    },
    handleDeleteItemListMenu: () => {
      dispatch(SetDeleteItemListMenu());
      dispatch(SetCloseModalIsNotification());
    },
    handleLogOut: () => {
      dispatch(SetCloseModalIsNotification());
      dispatch(SetShowLoading(true));
      LogOut({token: UserName.token})
        .then(data => {
          if (data.status_code == Success) {
            signOut('');
            return;
          }
          if (AuthenticationError(data.message , data.status_code)) {
            signOut('');
            return;
          } else {
            dispatch(
              SetShowModalIsNotification({
                description: errorNotification,
                showValue: true,
                functionConfirm: '',
                functionCancel: 'handleCloseModalNotification',
                valueBtnConfirm: '',
                valueBtnCancel: '',
              }),
            );
          }
        })
        .catch(err => {
          dispatch(
            SetShowModalIsNotification({
              description: errorNotification,
              showValue: true,
              functionConfirm: '',
              functionCancel: 'handleCloseModalNotification',
              valueBtnConfirm: '',
              valueBtnCancel: '',
            }),
          );
        })
        .finally(() => dispatch(SetShowLoading(false)));
    },
    handleClearDataTable: () => {
      dispatch(SetClearDataProductTable());
      dispatch(SetCloseModalIsNotification());
    },
    handleYesIpconfig: () => {
      dispatch(SetCloseModalIsNotification());
    },
    handleExitApp: () => {
      BackHandler.exitApp();
      dispatch(SetCloseModalIsNotification());
    },
    handleCheckOutTableProduct: () => {
      dispatch(SetShowLoading(true));
      dispatch(SetCloseModalIsNotification());
      CheckOutTable({
        token: UserName.token,
        data: {
          id: TableItem.id,
          user_id: UserName.id,
        },
      })
        .then(data => {
          if (AuthenticationError(data.message , data.status_code)) {
            signOut(LoginAgain);
            return;
          }
          if (data.message == ResponsiveMessage.TableDontExist) {
            navigation.goBack();
            dispatch(SetModalPosition(''));
            dispatch(SetRemoveTableNotExist(TableItem.id));
            dispatch(
              SetShowModalIsNotification({
                description: TableStandingNotExist,
                showValue: true,
                functionConfirm: '',
                functionCancel: 'handleCloseModalNotification',
                valueBtnConfirm: '',
                valueBtnCancel: '',
              }),
            );
            return;
          }
          if (data.status_code == Success) {
            navigation.goBack();
            dispatch(SetModalPosition(''));
            dispatch(SetCloseDetailTable());
          }
          else {
            dispatch(
              SetShowModalIsNotification({
                description: data.message,
                showValue: true,
                functionConfirm: '',
                functionCancel: 'handleCloseModalNotification',
                valueBtnConfirm: '',
                valueBtnCancel: '',
              }),
            );
          }
        })
        .catch(e => {
          dispatch(
            SetShowModalIsNotification({
              description: errorNotification,
              showValue: true,
              functionConfirm: '',
              functionCancel: 'handleCloseModalNotification',
              valueBtnConfirm: '',
              valueBtnCancel: '',
            }),
          );
        })
        .finally(() => dispatch(SetShowLoading(false)));
    },
    handlePayment: () => {
      dispatch(SetCloseModalIsNotification());
      dispatch(SetShowLoading(true));
      const itemProduct =DataPayment.item.map((item :ProductItem)=>{
        return {
          category_id:item.category_id,
          check:item.check,
          id:item.id,
          image:item.image,
          price:item.price ,
          quantity:item.quantity + item.qtyStatus,
          status:item.status,
          title:item.title,
          vat:item.vat,
          TotalPrice : (item.price + (item.price *(item.vat/100)))
        }
      })
      CreatePayment({
        token: UserName.token,
        data: {
          nameusertt: UserName.name,
          reason: DataPayment.Reason,
          items: JSON.stringify({
            item:itemProduct , 
            discountPayment : DataPayment.DiscountPayment, 
            tableName: TableItem.tableName
          }),
          user_id: UserName.id,
          idpaymentrd: timeStamS,
          discount: DataPayment.DiscountPayment,
        },
      })
        .then(data => {
          if (AuthenticationError(data.message , data.status_code)) {
            signOut(LoginAgain);
            dispatch(SetShowLoading(false));
            return;
          }
          if (data.status_code == Success) {
            if (
              DataPayment.valueNumber !== null &&
              DataPayment.valueNumber !== undefined &&
              DataPayment.valueNumber.name !== '' &&
              DataPayment.valueNumber.phone !== '' &&
              DataPayment.valueNumber.valid
            ) {
              UpdateCustomerReward({
                token: UserName.token,
                data: {
                  total_money: data.payment.valuetotal,
                  phone: DataPayment.valueNumber.phone,
                },
              })
                .then()
                .catch();
            }
            if (TableItem.id !== undefined) {
              if (DataPayment.CheckValue == 'All') {
                CheckOutTable({
                  token: UserName.token,
                  data: {
                    id: TableItem.id,
                    user_id: UserName.id,
                  },
                })
                  .then(data => {
                    if (AuthenticationError(data.message , data.status_code)) {
                      signOut(LoginAgain);
                      dispatch(SetShowLoading(false));
                      return;
                    }
                    if (data.message == ResponsiveMessage.TableDontExist) {
                      dispatch(SetRemoveTableNotExist(TableItem.id));
                      dispatch(SetModalPosition(''));
                      dispatch(
                        SetShowModalIsNotification({
                          description: PaymentSuccess,
                          showValue: true,
                          functionConfirm: '',
                          functionCancel: 'handleCloseModalNotification',
                          valueBtnConfirm: '',
                          valueBtnCancel: '',
                        }),
                      );
                      navigation.goBack();
                      return;
                    }
                    if (data.status_code == Success) {
                      dispatch(
                        SetUpdateTable({
                          idTable: TableItem.id,
                          CheckValue: DataPayment.CheckValue,
                          dataUpdate: [],
                        }),
                      );
                      dispatch(SetModalPosition(''));
                      dispatch(
                        SetShowModalIsNotification({
                          description: PaymentSuccess,
                          showValue: true,
                          functionConfirm: '',
                          functionCancel: 'handleCloseModalNotification',
                          valueBtnConfirm: '',
                          valueBtnCancel: '',
                        }),
                      );
                      navigation.goBack();
                    }
                    else {
                      navigation.goBack();
                      dispatch(
                        SetShowModalIsNotification({
                          description: data.message,
                          showValue: true,
                          functionConfirm: '',
                          functionCancel: 'handleCloseModalNotification',
                          valueBtnConfirm: '',
                          valueBtnCancel: '',
                        }),
                      );
                    }
                  })
                  .catch(e => {
                    navigation.goBack();
                    dispatch(
                      SetShowModalIsNotification({
                        description: UpdateTableFail,
                        showValue: true,
                        functionConfirm: '',
                        functionCancel: 'handleCloseModalNotification',
                        valueBtnConfirm: '',
                        valueBtnCancel: '',
                      }),
                    );
                  })
                  .finally(() => dispatch(SetShowLoading(false)));
                return;
              }
              if (DataPayment.CheckValue == 'NotAll') {
                const TableProduct = TableItem.listitem.filter(item => item.check !== 1,);
                const indexUserOrderOld =
                TableItem.userordered !== undefined
                    ? TableItem.userordered.findIndex(
                        item => item.id == UserName.id,
                      )
                    : -1;
                UpdateTable({
                  token: UserName.token,
                  data: {
                    id: TableItem.id,
                    listitem: JSON.stringify(TableProduct),
                    user_id: UserName.id,
                    status: 0,
                    userordered: indexUserOrderOld !== -1 ? JSON.stringify(TableItem.userordered): JSON.stringify(TableItem.userordered !== undefined ? [  ...TableItem.userordered, {Name: UserName.name, id: UserName.id}] : '[]')},
                })
                  .then(data => {
                    if (AuthenticationError(data.message , data.status_code)) {
                      signOut(LoginAgain);
                      dispatch(SetShowLoading(false));
                      return;
                    }
                    if (data.message == ResponsiveMessage.TableDontExist) {
                      dispatch(SetModalPosition(''));
                      dispatch(SetRemoveTableNotExist(TableItem.id));
                      dispatch(
                        SetShowModalIsNotification({
                          description: PaymentSuccess,
                          showValue: true,
                          functionConfirm: '',
                          functionCancel: 'handleCloseModalNotification',
                          valueBtnConfirm: '',
                          valueBtnCancel: '',
                        }),
                      );
                      navigation.goBack();
                      return;
                    }
                    if (data.status_code == Success) {
                      dispatch(
                        SetUpdateTable({
                          idTable: TableItem.id,
                          CheckValue: DataPayment.CheckValue,
                          dataUpdate: TableProduct,
                        }),
                      );
                      dispatch(
                        SetShowModalIsNotification({
                          description: PaymentSuccess,
                          showValue: true,
                          functionConfirm: '',
                          functionCancel: 'handleCloseModalNotification',
                          valueBtnConfirm: '',
                          valueBtnCancel: '',
                        }),
                      );
                      navigation.goBack();
                    }
                    else {
                      navigation.goBack();
                      dispatch(
                        SetShowModalIsNotification({
                          description: data.message,
                          showValue: true,
                          functionConfirm: '',
                          functionCancel: 'handleCloseModalNotification',
                          valueBtnConfirm: '',
                          valueBtnCancel: '',
                        }),
                      );
                    }
                  })
                  .catch(error => {
                    navigation.goBack();
                    dispatch(
                      SetShowModalIsNotification({
                        description: UpdateTableFail,
                        showValue: true,
                        functionConfirm: '',
                        functionCancel: 'handleCloseModalNotification',
                        valueBtnConfirm: '',
                        valueBtnCancel: '',
                      }),
                    );
                  })
                  .finally(() => dispatch(SetShowLoading(false)));
                return;
              }
              return;
            }
          }
          else {
            dispatch(SetShowLoading(false));
            navigation.goBack();
            dispatch(
              SetShowModalIsNotification({
                description: data.message,
                showValue: true,
                functionConfirm: '',
                functionCancel: 'handleCloseModalNotification',
                valueBtnConfirm: '',
                valueBtnCancel: '',
              }),
            );
          }
        })
        .catch(error => {
          dispatch(SetShowLoading(false));
          dispatch(
            SetShowModalIsNotification({
              description: errorNotification,
              showValue: true,
              functionConfirm: '',
              functionCancel: 'handleCloseModalNotification',
              valueBtnConfirm: '',
              valueBtnCancel: '',
            }),
          );
        });
    },
    handleCheckOutTableDetail: () => {
      dispatch(SetCloseModalIsNotification());
      dispatch(SetShowLoading(true));
      if (TableItem.listitem.length <= 0) {
        CheckOutTable({
          token: UserName.token,
          data: {
            id: TableItem.id,
            user_id: UserName.id,
          },
        })
          .then(data => {
            if (AuthenticationError(data.message , data.status_code)) {
              signOut(LoginAgain);
              return;
            }
            if (data.message == ResponsiveMessage.TableDontExist) {
              dispatch(SetModalPosition(''));
              dispatch(SetRemoveTableNotExist(TableItem.id));
              dispatch(
                SetShowModalIsNotification({
                  description: TableStandingNotExist,
                  showValue: true,
                  functionConfirm: '',
                  functionCancel: 'handleCloseModalNotification',
                  valueBtnConfirm: '',
                  valueBtnCancel: '',
                }),
              );
              return;
            }
            if (data.status_code == Success) {
              dispatch(SetCloseDetailTable());
              dispatch(SetModalPosition(''));
              dispatch(SetCloseModalIsNotification());
            }
            else {
              dispatch(SetCloseDetailTable());
              dispatch(
                SetShowModalIsNotification({
                  description: data.message,
                  showValue: true,
                  functionConfirm: '',
                  functionCancel: 'handleCloseModalNotification',
                  valueBtnConfirm: '',
                  valueBtnCancel: '',
                }))}
          })
          .catch((e) => {
            dispatch(
              SetShowModalIsNotification({
                description: errorNotification,
                showValue: true,
                functionConfirm: '',
                functionCancel: 'handleCloseModalNotification',
                valueBtnConfirm: '',
                valueBtnCancel: '',
              }),
            );
          })
          .finally(() => dispatch(SetShowLoading(false)));
        return;
      }
      if (TableItem.listitem.length > 0) {
        const indexUserOrderOld =
          TableItem.userordered !== undefined
            ? TableItem.userordered.findIndex(item => item.id == UserName.id)
            : -1;
        UpdateTable({
          token: UserName.token,
          data: {
            id: TableItem.id,
            listitem: JSON.stringify(TableItem.listitem),
            user_id: UserName.id,
            status: 1,
            userordered:
              indexUserOrderOld !== -1
                ? JSON.stringify(TableItem.userordered)
                : JSON.stringify(
                    TableItem.userordered !== undefined
                      ? [
                          ...TableItem.userordered,
                          {Name: UserName.name, id: UserName.id},
                        ]
                      : '[]',
                  ),
          },
        })
          .then(data => {
            if (AuthenticationError(data.message , data.status_code)) {
              signOut(LoginAgain);
              return;
            }
            if (data.message == ResponsiveMessage.TableDontExist) {
              dispatch(SetModalPosition(''));
              dispatch(SetRemoveTableNotExist(TableItem.id));
              dispatch(
                SetShowModalIsNotification({
                  description: TableStandingNotExist,
                  showValue: true,
                  functionConfirm: '',
                  functionCancel: 'handleCloseModalNotification',
                  valueBtnConfirm: '',
                  valueBtnCancel: '',
                }),
              );
              return;
            }
            if (data.status_code == Success) {
              dispatch(SetCloseDetailTable());
              dispatch(SetModalPosition(''));
              dispatch(SetCloseModalIsNotification());
            }
            else {
              dispatch(SetCloseDetailTable());
              dispatch(
                SetShowModalIsNotification({
                  description: data.message,
                  showValue: true,
                  functionConfirm: '',
                  functionCancel: 'handleCloseModalNotification',
                  valueBtnConfirm: '',
                  valueBtnCancel: '',
                }),
              );
            }
          })
          .catch(e => {
            dispatch(
              SetShowModalIsNotification({
                description: errorNotification,
                showValue: true,
                functionConfirm: '',
                functionCancel: 'handleCloseModalNotification',
                valueBtnConfirm: '',
                valueBtnCancel: '',
              }),
            );
          })
          .finally(() => dispatch(SetShowLoading(false)));
        return;
      }
    },
    handleCloseModalNotification: () => {
      dispatch(SetCloseModalIsNotification());
    },
    // check printer
    HandlePrint: async () => {
      dispatch(SetShowLoading(true));
      dispatch(SetCloseModalIsNotification());
      const itemProductCheck = TableItem.listitem;
      const itemCheckValid = itemProductCheck.filter(item => item.check == 1);
      const statusProductEvery = itemCheckValid.length > 0  ? 
      itemCheckValid.every(item => {    return item.status == 2 && item.qtyStatus <= 0; }) :
      itemProductCheck.every(item => { return item.status == 2 && item.qtyStatus <= 0;})
      const ItemMapData =itemCheckValid.length > 0 ? itemCheckValid : itemProductCheck;
      let totalitem = 0;
      if (Platform.OS == 'android') {
        const datamapitem = ItemMapData.map(item => {
          if (statusProductEvery) {
            totalitem += item.quantity;
            return (
              `[L]<b>${removeVietnameseaccents(item.title.toString())}</b>[R]${
                item.quantity
              }\n` +
              '[L]\n' +
              `[C]------------------------------------------------\n`
            );
          } else {
            if (item.qtyStatus > 0 && item.check == 1) {
              totalitem += item.qtyStatus;
              return (
                `[L]<b>${removeVietnameseaccents(
                  item.title.toString(),
                )}</b>[R]${item.qtyStatus}\n` +
                '[L]\n' +
                `[C]------------------------------------------------\n`
              );
            }
            if (item.qtyStatus > 0) {
              totalitem += item.qtyStatus + item.quantity;
              return (
                `[L]<b>${removeVietnameseaccents(
                  item.title.toString(),
                )}</b>[R]${item.qtyStatus + item.quantity}\n` +
                '[L]\n' +
                `[C]------------------------------------------------\n`
              );
            }
            if (item.qtyStatus <= 0 && item.status == 1) {
              totalitem += item.quantity;
              return (
                `[L]<b>${removeVietnameseaccents(
                  item.title.toString(),
                )}</b>[R]${item.quantity}\n` +
                '[L]\n' +
                `[C]------------------------------------------------\n`
              );
            }
            if (item.qtyStatus <= 0 && item.status == 2) {
              totalitem += item.quantity;
              return (
                `[L]<b>${removeVietnameseaccents(
                  item.title.toString(),
                )}</b>[R]${item.quantity}\n` +
                '[L]\n' +
                `[C]------------------------------------------------\n`
              );
            }
          }
        });
        const dataConfig = datamapitem.filter(item => item !== undefined);
        const text =
          "[C]<font size='big'>   PHIEU IN BEP</font>\n" +
          '[L]\n' +
          `[L]<font size='small'>Ngay    : ${getDateTime(date)}</font>\n` +
          `[L]<font size='small'>${'Ban     : ' + TableItem.tableName
          }</font>\n` +
          `[L]<font size='small'>NV      : ${removeVietnameseaccents(
            UserName.name !== undefined ? UserName.name.toString() : '',
          )}</font>\n` +
          `[L]<font size='small'>${
            noteValue !== ''
              ? 'Ghi Chu : ' + removeVietnameseaccents(noteValue)
              : ''
          }</font>\n` +
          '[C]================================================\n' +
          '[L]\n' +
          '[L]<b>TEN MON</b>[R]SL\n' +
          '[L]\n' +
          '[C]------------------------------------------------\n' +
          '[L]\n' +
          dataConfig.join('') +
          '[L]\n' +
          `[L]<b>Tong SL : ${totalitem}</b>\n` +
          '[C]------------------------------------------------\n' +
          '[C]     Thank You\n' +
          '[L]\n';
        PingModule.PingAddress(DataSetting.PrintIp, (value: string) => {
          if (value == 'Success') {
              handleUpdatePrinted(text, dataConfig, totalitem);
            return;
          }
          if (value == 'Failure') {
            dispatch(SetShowLoading(false));
            dispatch(
              SetShowModalIsNotification({
                description: CantConnectIP,
                showValue: true,
                functionConfirm: '',
                functionCancel: 'handleCloseModalNotification',
                valueBtnConfirm: '',
                valueBtnCancel: '',
              }),
            );
            return;
          }
        });
      }
      else {
        const datamapitem = ItemMapData.map(item => {
          if (statusProductEvery) {
            totalitem += item.quantity;
            return [
              `${removeVietnameseaccents(item.title.toString())}`,
              `${item.quantity}`,
              `${item.price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',')}`,
            ];
          } else {
            if (item.qtyStatus > 0 && item.check == 1) {
              totalitem += item.qtyStatus;
              return [
                `${removeVietnameseaccents(item.title.toString())}`,
                `${item.qtyStatus}`,
                `${item.price
                  .toString()
                  .replace(/\B(?=(\d{3})+(?!\d))/g, ',')}`,
              ];
            }
            if (item.qtyStatus > 0) {
              totalitem += item.qtyStatus + item.quantity;
              return [
                `${removeVietnameseaccents(item.title.toString())}`,
                `${item.qtyStatus + item.quantity}`,
                `${item.price
                  .toString()
                  .replace(/\B(?=(\d{3})+(?!\d))/g, ',')}`,
              ];
            }
            if (item.qtyStatus <= 0 && item.status == 1) {
              totalitem += item.quantity;
              return [
                `${removeVietnameseaccents(item.title.toString())}`,
                `${item.quantity}`,
                `${item.price
                  .toString()
                  .replace(/\B(?=(\d{3})+(?!\d))/g, ',')}`,
              ];
            }
            if (item.qtyStatus <= 0 && item.status == 2) {
              totalitem += item.quantity;
              return [
                `${removeVietnameseaccents(item.title.toString())}`,
                `${item.quantity}`,
                `${item.price
                  .toString()
                  .replace(/\B(?=(\d{3})+(?!\d))/g, ',')}`,
              ];
            }
          }
        });
        const dataConfig = datamapitem.filter(item => item !== undefined);
        NetPrinter.init()
          .then(() => {
            NetPrinter.connectPrinter(`${DataSetting.PrintIp}`, 9100)
              .then(() => {
                  handleUpdatePrinted('', dataConfig, totalitem);
              })
              .catch(e => {
                dispatch(
                  SetShowModalIsNotification({
                    description: CantConnectIP,
                    showValue: true,
                    functionConfirm: '',
                    functionCancel: 'handleCloseModalNotification',
                    valueBtnConfirm: '',
                    valueBtnCancel: '',
                  }),
                );
                dispatch(SetShowLoading(false));
              });
          })
          .catch(e => {
            dispatch(SetShowLoading(false));
            dispatch(
              SetShowModalIsNotification({
                description: CantConnectIP,
                showValue: true,
                functionConfirm: '',
                functionCancel: 'handleCloseModalNotification',
                valueBtnConfirm: '',
                valueBtnCancel: '',
              }),
            );
          });
      }
    },
  };
  // call function modal
  const handlePress = (StringValue: string, valueBtn: string) => {
    valueBtn !== ''
      ? functions[StringValue](valueBtn)
      : functions[StringValue]();
  };
  //Show Error
  const handleError = (e: any) => {
    dispatch(SetShowLoading(false));
    dispatch(
      SetShowModalIsNotification({
        description: errorNotification,
        showValue: true,
        functionConfirm: '',
        functionCancel: 'handleCloseModalNotification',
        valueBtnConfirm: '',
        valueBtnCancel: '',
      }),
    );
  };
  // in bàn trên thiết bị android
  const handlePrinterTableAndroid = async (
    TextPrinter: string,
    itemMap: ProductItem[],
  ) => {
    try {
      await ThermalPrinterModule.printTcp({
        ip: DataSetting.PrintIp,
        payload: TextPrinter,
        port: 9100,
        timeout: 3000,
      });
      dispatch(SetPrintTableDetail(itemMap));
      dispatch(SetShowLoading(false));
      dispatch(
        SetShowModalIsNotification({
          description: PrintSuccess,
          showValue: true,
          functionConfirm: '',
          functionCancel: 'handleCloseModalNotification',
          valueBtnConfirm: '',
          valueBtnCancel: '',
        }),
      );
    } catch (err) {
      dispatch(
        SetShowModalIsNotification({
          description: CantConnectIP,
          showValue: true,
          functionConfirm: '',
          functionCancel: 'handleCloseModalNotification',
          valueBtnConfirm: '',
          valueBtnCancel: '',
        }),
      );
      dispatch(SetShowLoading(false));
    }
  };
  //in bàn trên thiết bị Ios
  const handlePrinterTableIOS = (
    dataArray: any[],
    totalitem: number,
    itemMap: ProductItem[],
  ) => {
    NetPrinter.printText(`${CENTER}${BOLD_ON}PHIEU IN BEP${BOLD_OFF}\n`);
    NetPrinter.printText(`${LEFT}Ngay CT : ${getDateTime(date)}${OFF_CENTER}`);
    NetPrinter.printText(`${LEFT}${`Ban     : ${TableItem.tableName}`}${OFF_CENTER}`,);
    NetPrinter.printText(`${LEFT}NV      : ${removeVietnameseaccents(UserName.name !== undefined ? UserName.name.toString() : '',)}${OFF_CENTER}`);
    NetPrinter.printText(`${LEFT}${noteValue !== ''? 'Ghi Chu : ' + removeVietnameseaccents(noteValue): ''}${OFF_CENTER}`);
    NetPrinter.printText(`${CENTER}${COMMANDS.HORIZONTAL_LINE.HR_80MM}${CENTER}`);
    let orderList: any = dataArray;
    let columnAliment = [
      ColumnAlignment.LEFT,
      ColumnAlignment.CENTER,
      ColumnAlignment.RIGHT,
    ];
    let columnWidth = [46 - (7 + 12), 7, 12];
    const header = ['TEN MON', 'SL', 'GIA'];
    NetPrinter.printColumnsText(header, columnWidth, columnAliment, [
      `${BOLD_ON}`,
      '',
      '',
    ]);
    NetPrinter.printText(`${CENTER}${COMMANDS.HORIZONTAL_LINE.HR3_80MM}${CENTER}`,);
    for (let i in orderList) {
      NetPrinter.printColumnsText(orderList[i], columnWidth, columnAliment, [
        `${BOLD_OFF}`,
        '',
        '',
      ]);
      NetPrinter.printText(`${CENTER}${COMMANDS.HORIZONTAL_LINE.HR_80MM}${CENTER}`);
    }
    NetPrinter.printText(`${LEFT}TONG SL : ${totalitem}${OFF_CENTER}`);
    NetPrinter.printText(`${CENTER}${COMMANDS.HORIZONTAL_LINE.HR_80MM}${CENTER}`);
    NetPrinter.printBill2(`${CENTER}Thank you\n`, function () {}, {
      beep: false,
    });
    dispatch(SetPrintTableDetail(itemMap));
    dispatch(SetShowLoading(false));
    dispatch(
      SetShowModalIsNotification({
        description: PrintSuccess,
        showValue: true,
        functionConfirm: '',
        functionCancel: 'handleCloseModalNotification',
        valueBtnConfirm: '',
        valueBtnCancel: '',
      }),
    );
  };
  //update table printer
  const handleUpdatePrinted = (
    textPrinterAndroid: string,
    dataArray: any[],
    totalItem: number,
  ) => {
    const indexUserOrderOld = TableItem.userordered !== undefined ? TableItem.userordered.findIndex(item => item.id == UserName.id): -1;
    const ValidItemCheck = TableItem.listitem.find(item => item.check == 1);
    const itemMap = TableItem.listitem.map(item => {
      if (ValidItemCheck) {
        if (item.check == 1) {
          return {
            id: item.id,
            image: item.image,
            price: item.price,
            quantity: item.quantity + item.qtyStatus,
            status: 2,
            title: item.title,
            vat: item.vat,
            qtyStatus: 0,
            check: item.check,
            category_id: item.category_id,
          };
        } else {
          return {
            id: item.id,
            image: item.image,
            price: item.price,
            quantity: item.quantity,
            status: item.status == 1 ? 1 : 2,
            title: item.title,
            vat: item.vat,
            qtyStatus: item.qtyStatus,
            check: item.check,
            category_id: item.category_id,
          };
        }
      }
      else {
        return {
          id: item.id,
          image: item.image,
          price: item.price,
          quantity: item.quantity + item.qtyStatus,
          status: 2,
          title: item.title,
          vat: item.vat,
          qtyStatus: 0,
          check: item.check,
          category_id: item.category_id,
        };
      }
    });
    UpdateTable({
      token: UserName.token,
      data: {
        id: TableItem.id,
        listitem: JSON.stringify(itemMap),
        user_id: UserName.id,
        status: 0,
        userordered: indexUserOrderOld !== -1  ? JSON.stringify(TableItem.userordered) : JSON.stringify(TableItem.userordered !== undefined? [  ...TableItem.userordered,  {Name: UserName.name, id: UserName.id}]: '[]'),
      },
    })
      .then(data => {
        if (AuthenticationError(data.message , data.status_code)) {
          signOut(LoginAgain);
          return;
        }
        if (data.status_code !== Success && Platform.OS == 'ios') {
          disconnectPrinterIos();
          return;
        }
        if (data.message == ResponsiveMessage.TableDontExist) {
          dispatch(SetModalPosition(''));
          dispatch(SetRemoveTableNotExist(TableItem.id))
          dispatch(SetShowLoading(false));
          dispatch(
            SetShowModalIsNotification({
              description: TableStandingNotExist,
              showValue: true,
              functionConfirm: '',
              functionCancel: 'handleCloseModalNotification',
              valueBtnConfirm: '',
              valueBtnCancel: '',
            }),
          );
          return;
        }
        if (data.status_code == Success) {
          if (Platform.OS == 'android') {
            handlePrinterTableAndroid(textPrinterAndroid, itemMap);
            return;
          }
          if (Platform.OS == 'ios') {
            handlePrinterTableIOS(dataArray, totalItem, itemMap);
            return;
          }
        }
        else {
          dispatch(SetShowLoading(false));
          dispatch(
            SetShowModalIsNotification({
              description: data.message,
              showValue: true,
              functionConfirm: '',
              functionCancel: 'handleCloseModalNotification',
              valueBtnConfirm: '',
              valueBtnCancel: '',
            }),
          );
        }
      })
      .catch(e => {
        if (Platform.OS == 'ios') {
          disconnectPrinterIos();
        }
        handleError(e);
      });
  };
  // disconnet Printer ios
  const disconnectPrinterIos = () => {
    NetPrinter.closeConn();
  };
  const handleChangeNoteValue = (value: string) => {
    setNoteValue(value);
  };
  //đóng modal
  return (
    <>
      {initialValueModal.description !== '' ? (
        <KeyboardAvoidingView
          behavior={Platform.OS === 'ios' ? 'padding' : 'padding'}
          style={ModalNotificationCss.ModalOpitionContainer}>
          <View
            style={
              initialValueModal.description == CreateKitchenVouchers
                ? ModalNotificationCss.ModalOpitionPrint
                : ModalNotificationCss.ModalOpition
            }>
            {initialValueModal.description !== '' ? (
              <View style={{flex: 1}}>
                <View style={{flex: 1, alignItems: 'center', paddingTop: 10}}>
                  <Text style={ModalNotificationCss.textStyleModal}>
                    {initialValueModal.description}
                  </Text>
                  {initialValueModal.description == CreateKitchenVouchers ? (
                    <View style={{width: '100%', flex: 1}}>
                      <TextInput
                        placeholder="Nhập ghi chú"
                        style={{
                          width: '100%',
                          paddingHorizontal: 10,
                          fontSize: widthScreen * 0.04,
                          height: widthScreen * 0.14,
                        }}
                        onChangeText={e => handleChangeNoteValue(e)}
                      />
                    </View>
                  ) : (
                    ''
                  )}
                </View>
                <View style={ModalNotificationCss.ModalOpitionBtnTable}>
                  {initialValueModal.functionConfirm !== '' ? (
                    <TouchableOpacity
                      onPress={() =>handlePress(initialValueModal.functionConfirm,initialValueModal.valueBtnConfirm)}
                      style={ModalNotificationCss.ModalOpitionBtn}>
                      <Text style={ModalNotificationCss.ModalOpitionBtnText}>
                        Đồng ý
                      </Text>
                    </TouchableOpacity>
                  ) : (
                    <TouchableOpacity></TouchableOpacity>
                  )}
                  <TouchableOpacity
                    onPress={() =>handlePress( initialValueModal.functionCancel, initialValueModal.valueBtnCancel)}
                    style={ModalNotificationCss.ModalOpitionBtn}>
                    <Text style={ModalNotificationCss.ModalOpitionBtnText}>
                      Huỷ
                    </Text>
                  </TouchableOpacity>
                </View>
              </View>
            ) : (
              ''
            )}
          </View>
        </KeyboardAvoidingView>
      ) : (
        ''
      )}
    </>
  );
};
export default React.memo(ModalNotification);
