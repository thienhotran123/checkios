import {
  ActivityIndicator,
  ScrollView,
  Text,
  TouchableOpacity,
  View,
  KeyboardAvoidingView,
  LogBox,
  Platform,
} from 'react-native';
import React from 'react';
import {ListTableMoveCss} from './ListTableMoveCss';
import {useState} from 'react';
import {
  SetModalPosition,
  SetRemoveTableNotExist,
  SetShowLoading,
  SetShowMOdalListTable,
  SetShowModalIsNotification,
  SetUpdateTableChange,
} from '../../../Redux/Slide';
import {useDispatch, useSelector} from 'react-redux';
import {useEffect} from 'react';
import {
  ChangeTable,
  CheckOutTable,
  UpdateTable,
  getTable,
} from '../../../Utils/GetApi';
import {RootState} from '../../../Redux/Storage';
import {ValueOpiton} from '../../../Utils/interface/interfaceListTableEmpty/interfaceListTableEmpty';
import {AuthContext} from '../../../DefaultLayout/DefaultLayoutScreen';
import {ProductItem} from '../../../Utils/interface/interface';
import {Success} from '../../../Utils/Enums';
import {ResponsiveMessage} from '../../../Utils/MessageApi';
import {
  LoginAgain,
  TableMoveNotExist,
  TableStandingNotExist,
  errorNotification,
} from '../../../Utils/MessageNotification';
import {TableIsUse} from '../../../Utils/MessageNotification';
import { AuthenticationError } from '../../../Utils/Helper';
LogBox.ignoreLogs(['Warning: ...']);
LogBox.ignoreAllLogs();
const ListTableMove = () => {
  const {signOut} = React.useContext(AuthContext);
  const dispatch = useDispatch();
  const UserName = useSelector((state: RootState) => state.counter.UserName);
  const TableItem = useSelector((state: RootState) => state.counter.TableItem);
  const NameModal = useSelector((state: RootState) => state.counter.NameModal);
  const [ValueOpiton, SetValueOpition] = useState<ValueOpiton>({});
  const [DataTableStatus, SetDataTableStatus] = useState<ValueOpiton[]>([]);
  const [isLoading, setIsLoading] = useState<boolean>(true);
  useEffect(() => {
    getTable({
      token: UserName.token,
    })
      .then(data => {
        setIsLoading(true);
        if (AuthenticationError(data.message , data.status_code))
        {
          signOut(LoginAgain)
          return
        } 
        if (data.status_code == Success) {
          const DataMap = data.listtable.filter(item => item.status == 1);
          SetDataTableStatus(DataMap);
          setIsLoading(false);
          return
        }
        else {
          SetDataTableStatus([]);
          setIsLoading(false);
        }
      })
      .catch((e) => {
        setIsLoading(false);
      });
  }, []);
  const handleGetValueOpiton = (value: ValueOpiton) => {
    SetValueOpition(value);
  };
  const handleCloseMoveTable = () => {
    dispatch(SetShowMOdalListTable({show: false, NameModal: ''}));
  };
  const handleMoveTable = () => {
    dispatch(SetShowMOdalListTable({show: false, NameModal: ''}));
    dispatch(SetShowLoading(true));
    const SelectedDish = TableItem.listitem.filter(item => {
      return item.check == 1;
    });
    const NotSelect = TableItem.listitem.filter(item => {
      return item.check !== 1;
    });
    const listProduct: ProductItem[] = JSON.parse(
      ValueOpiton.listitem ? ValueOpiton.listitem : '[]',
    );
    let ListTableUpdate: ProductItem[] = [];
    const ListProduct: ProductItem[] = [];
    if (
      SelectedDish.length == TableItem.listitem.length ||
      SelectedDish.length <= 0
    ) {
      ListTableUpdate = [...listProduct, ...TableItem.listitem];
    } else {
      ListTableUpdate = [...SelectedDish, ...listProduct];
    }
    let ChangeValue =
      SelectedDish.length == TableItem.listitem.length ||
      SelectedDish.length <= 0 ? 'All': 'NotAll';
    for (let i = 0; i < ListTableUpdate.length; i++) {
      const indexdaListMenu = ListProduct.findIndex(item => item.id == ListTableUpdate[i].id && item.status == 1);
      if (indexdaListMenu !== -1) {
        const productQty: ProductItem = {
          vat: ListProduct[indexdaListMenu].vat,
          id: ListProduct[indexdaListMenu].id,
          image: ListProduct[indexdaListMenu].image,
          price: ListProduct[indexdaListMenu].price,
          qtyStatus: ListProduct[indexdaListMenu].qtyStatus,
          quantity: ListProduct[indexdaListMenu].quantity + ListTableUpdate[i].quantity,
          status: ListProduct[indexdaListMenu].status,
          title: ListProduct[indexdaListMenu].title,
          check: 0,
          category_id: ListProduct[indexdaListMenu].category_id,
        };
        ListProduct.splice(indexdaListMenu, 1, productQty);
      } else {
        const dataClone: ProductItem = {
          vat: ListTableUpdate[i].vat,
          id: ListTableUpdate[i].id,
          image: ListTableUpdate[i].image,
          price: ListTableUpdate[i].price,
          qtyStatus: ListTableUpdate[i].qtyStatus,
          quantity: ListTableUpdate[i].quantity,
          status: ListTableUpdate[i].status,
          title: ListTableUpdate[i].title,
          check: 0,
          category_id: ListTableUpdate[i].category_id,
        };
        ListProduct.push(dataClone);
      }
    }
    //update old table
    UpdateTable({
      token: UserName.token,
      data: {
        id: TableItem.id,
        listitem: TableItem.listitem,
        user_id: UserName.id,
        status: 0,
        userordered: JSON.stringify(TableItem.userordered),
      },
    })
      .then(dataUpdateTable => {
        if (dataUpdateTable.status_code == Success) {
          ChangeTable({
            token: UserName.token,
            data: {
              old_id_table: TableItem.id,
              new_id_table: ValueOpiton.id,
              listitem_old: ChangeValue == 'All'? JSON.stringify([]) : JSON.stringify(NotSelect),
              listitem_new: JSON.stringify(ListProduct),
            },
          })
            .then(dataChangeTable => {
              if (dataChangeTable.status_code == Success) {
                if (ChangeValue == 'All') {
                  CheckOutTable({
                    token: UserName.token,
                    data: {
                      id: TableItem.id,
                      user_id: UserName.id,
                    },
                  })
                    .then(data => {
                      if (data.status_code == Success) {
                        dispatch(SetModalPosition(''));
                        dispatch(
                          SetUpdateTableChange({
                            ChangeValue: ChangeValue,
                            ArrayTableStanDing: [],
                            ArrayTableMove: JSON.parse(
                              dataChangeTable.new_table.listitem,
                            ),
                            IDTableMove: dataChangeTable.new_table.id,
                            IDTableStanDing: dataChangeTable.old_table.id,
                          }),
                        );
                        return;
                      }
                      else {
                        dispatch(
                          SetUpdateTableChange({
                            ChangeValue: 'NotAll',
                            ArrayTableStanDing: [],
                            ArrayTableMove: JSON.parse(dataChangeTable.new_table.listitem,),
                            IDTableMove: dataChangeTable.new_table.id,
                            IDTableStanDing: dataChangeTable.old_table.id,
                          }),
                        );
                      }
                    })
                    .catch(err => {
                      dispatch(
                        SetUpdateTableChange({
                          ChangeValue: 'NotAll',
                          ArrayTableStanDing: [],
                          ArrayTableMove: JSON.parse(
                            dataChangeTable.new_table.listitem,
                          ),
                          IDTableMove: dataChangeTable.new_table.id,
                          IDTableStanDing: dataChangeTable.old_table.id,
                        }),
                      );
                    })
                    .finally(() => {
                      dispatch(SetShowLoading(false));
                    });
                  return;
                }
                if (ChangeValue == 'NotAll') {
                  dispatch(
                    SetUpdateTableChange({
                      ChangeValue: ChangeValue,
                      ArrayTableStanDing: JSON.parse(
                        dataChangeTable.old_table.listitem,
                      ),
                      ArrayTableMove: JSON.parse(
                        dataChangeTable.new_table.listitem,
                      ),
                      IDTableMove: dataChangeTable.new_table.id,
                      IDTableStanDing: dataChangeTable.old_table.id,
                    }),
                  );
                }
                return;
              }
              if (AuthenticationError(dataChangeTable.message , dataChangeTable.status_code)) {
                signOut(LoginAgain);
                return;
              }
              if (
                dataChangeTable.message == ResponsiveMessage.TableOldNotExist
              ) {
                dispatch(SetModalPosition(''));
                dispatch(SetRemoveTableNotExist(TableItem.id));
                dispatch(
                  SetShowModalIsNotification({
                    description: TableStandingNotExist,
                    showValue: true,
                    functionConfirm: '',
                    functionCancel: 'handleCloseModalNotification',
                    valueBtnConfirm: '',
                    valueBtnCancel: '',
                  }),
                );
                return;
              }
              if (
                dataChangeTable.message == ResponsiveMessage.TableNewNotExist
              ) {
                dispatch(SetRemoveTableNotExist(ValueOpiton.id));
                dispatch(
                  SetShowModalIsNotification({
                    description: TableMoveNotExist,
                    showValue: true,
                    functionConfirm: '',
                    functionCancel: 'handleCloseModalNotification',
                    valueBtnConfirm: '',
                    valueBtnCancel: '',
                  }),
                );
                return;
              }
              if (dataChangeTable.message == ResponsiveMessage.TableBusy) {
                dispatch(
                  SetShowModalIsNotification({
                    description: TableIsUse,
                    showValue: true,
                    functionConfirm: '',
                    functionCancel: 'handleCloseModalNotification',
                    valueBtnConfirm: '',
                    valueBtnCancel: '',
                  }),
                );
                return;
              }
              else {
                dispatch(
                  SetShowModalIsNotification({
                    description: dataChangeTable.message,
                    showValue: true,
                    functionConfirm: '',
                    functionCancel: 'handleCloseModalNotification',
                    valueBtnConfirm: '',
                    valueBtnCancel: '',
                  }),
                );
              }
            })
            .catch(err => {
              dispatch(
                SetShowModalIsNotification({
                  description: errorNotification,
                  showValue: true,
                  functionConfirm: '',
                  functionCancel: 'handleCloseModalNotification',
                  valueBtnConfirm: '',
                  valueBtnCancel: '',
                }),
              );
            })
            .finally(() => dispatch(SetShowLoading(false)));
          return;
        }
        if (AuthenticationError(dataUpdateTable.message , dataUpdateTable.status_code)) {
          signOut(LoginAgain);
          return;
        }
        if (dataUpdateTable.message == ResponsiveMessage.TableDontExist) {
          dispatch(SetModalPosition(''));
          dispatch(SetRemoveTableNotExist(TableItem.id));
          dispatch(
            SetShowModalIsNotification({
              description: TableStandingNotExist,
              showValue: true,
              functionConfirm: '',
              functionCancel: 'handleCloseModalNotification',
              valueBtnConfirm: '',
              valueBtnCancel: '',
            }),
          );
          return;
        }
        else {
          dispatch(
            SetShowModalIsNotification({
              description: dataUpdateTable.message,
              showValue: true,
              functionConfirm: '',
              functionCancel: 'handleCloseModalNotification',
              valueBtnConfirm: '',
              valueBtnCancel: '',
            }),
          );
        }
      })
      .catch(err => {
        dispatch(
          SetShowModalIsNotification({
            description: errorNotification,
            showValue: true,
            functionConfirm: '',
            functionCancel: 'handleCloseModalNotification',
            valueBtnConfirm: '',
            valueBtnCancel: '',
          }),
        );
      })
      .finally(() => dispatch(SetShowLoading(false)));
  };
  return (
    <KeyboardAvoidingView
      behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
      style={ListTableMoveCss.ModalOpiton}>
      <View style={ListTableMoveCss.Modal}>
        {NameModal == 'TableItemDetail' ? (
          <View style={ListTableMoveCss.ModalBodyInput}>
            <ScrollView
              style={ListTableMoveCss.ScrollContainer}
              showsVerticalScrollIndicator={false}>
              <View style={ListTableMoveCss.containerScrollChildren}>
                {isLoading ? (
                  <View style={ListTableMoveCss.Loading}>
                    <ActivityIndicator size={42} color="#0099FF" />
                  </View>
                ) : (
                  DataTableStatus.map(item => (
                    <TouchableOpacity
                      key={item.id}
                      onPress={() => handleGetValueOpiton(item)}
                      style={
                        ValueOpiton.id == item.id
                          ? ListTableMoveCss.ListTableCssValid
                          : ListTableMoveCss.ListTableCss
                      }>
                      <Text style={ListTableMoveCss.TextTableName}>
                        {item.tablename}
                      </Text>
                    </TouchableOpacity>
                  ))
                )}
              </View>
            </ScrollView>
            <View style={ListTableMoveCss.BtnModalS}>
              <TouchableOpacity
                disabled={ValueOpiton.id !== undefined ? false : true}
                onPress={handleMoveTable}
                style={
                  ValueOpiton.id !== undefined
                    ? ListTableMoveCss.BtnModalEdit
                    : ListTableMoveCss.BtnModalEditNotValid
                }>
                <Text
                  style={
                    ValueOpiton.id !== undefined
                      ? ListTableMoveCss.BtnModalEditText
                      : ListTableMoveCss.BtnModalEditTextNotValid
                  }>
                  Chuyển
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={handleCloseMoveTable}
                style={ListTableMoveCss.BtnModalEdit}>
                <Text style={ListTableMoveCss.BtnModalEditText}>Đóng</Text>
              </TouchableOpacity>
            </View>
          </View>
        ) : (
          ''
        )}
      </View>
    </KeyboardAvoidingView>
  );
};
export default ListTableMove;
