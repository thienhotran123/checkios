import {  StyleSheet } from 'react-native'
import { colors, widthScreen } from '../../../Utils/Styles'
export const ListTableMoveCss = StyleSheet.create({
    ModalOpiton: {
        position: 'absolute',
        backgroundColor: 'rgba(0,0,0,0.6)',
        flex: 1,
        justifyContent: 'center',
        alignContent: 'center',
        top: 0,
        bottom: 0,
        right: 0,
        left: 0,
        zIndex: 12,
    },
    BtnModalCartHD: {
        flexDirection: 'row',
        alignItems: 'flex-end',
        justifyContent: 'flex-end',
        marginBottom: 10,
    },
    OrderCart: {
        backgroundColor: '#fff',
        borderColor: colors.btnColor,
        marginRight: 10,
        width: widthScreen * 0.3,
        height: widthScreen * 0.1,
        alignItems: 'center',
        justifyContent: 'center',
        borderWidth: widthScreen >= 600 ? 2 : 1,
        borderRadius: 6
    },
    OrderCartTextValid: {
        color: '#fff',
        fontSize: widthScreen * 0.04,
    },
    OrderCartValid: {
        backgroundColor: colors.btnColor,
        marginRight: 10,
        width: widthScreen * 0.3,
        height: widthScreen * 0.1,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 6
    },
    OrderCartText: {
        color: colors.Textcolor,
        fontSize: widthScreen * 0.04,
    },

    Modal: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        padding: 20
    },
    ModalBodyInput: {
        backgroundColor: '#fff',
        padding: 20,
        borderRadius: 12,
        width: '100%',
        height: '60%'
    },
    ModalBodyInputMV: {
        backgroundColor: '#fff',
        padding: 20,
        borderRadius: 12,
        width: '100%',
    },
    INPUTeDIT: {
        marginVertical: 6,
        width: '100%',
        height: widthScreen * 0.14,
        fontSize: 12,
        backgroundColor: '#fff',
        borderRadius: 5,
        borderColor: '#333',
        borderWidth: widthScreen >= 600 ? 2 : 1,
    },
    BtnModalS: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent:'center',
        marginTop: 20,
        height: '10%'

    },
    BtnModalSMV: {
        flexDirection: 'row',
        alignItems: 'flex-end',
        justifyContent: 'flex-end',
        marginTop: 20,

    },
    TextModalIput: {
        flex: 1,
        fontSize: widthScreen * 0.04,
        paddingHorizontal:10,
        color:'#333'
    },

    BtnModalEdit: {
        backgroundColor: '#fff',
        borderColor: colors.btnColor,
        marginHorizontal:5,
        width: widthScreen * 0.3,
        height: widthScreen * 0.1,
        alignItems: 'center',
        justifyContent: 'center',
        borderWidth: widthScreen >= 600 ? 2 : 1,
        borderRadius: 6
    },
    BtnModalEditNotValid: {
        marginHorizontal:5,
        backgroundColor: 'rgba(0,0,0,0.2)',
        borderColor: 'rgba(0,0,0,0.2)',
        width: widthScreen * 0.3,
        height: widthScreen * 0.1,
        alignItems: 'center',
        justifyContent: 'center',
        borderWidth: widthScreen >= 600 ? 2 : 1,
        borderRadius: 6
    },
    BtnModalEditText: {
        color: colors.Textcolor,
        fontSize: widthScreen * 0.04,
        fontWeight:'700'
    },
    BtnModalEditTextNotValid: {
        color: '#fff',
        fontSize: widthScreen * 0.04,
        fontWeight:'700'
    },
    ListTableCss: {
        borderColor: '#AAAAAA',
        borderWidth: widthScreen >= 600 ? 2 : 2,
        width: widthScreen >= 600 ?'28%' :'20%',
        height:widthScreen >= 600 ? 80: 60,
        borderRadius: 6,
        marginLeft: 5,
        marginRight: 5,
        marginBottom: 10,
        alignItems: 'center',
        justifyContent: 'center'
    },
    ListTableCssValid: {
        borderColor: '#3366CC',
        borderWidth: widthScreen >= 600 ? 2 : 2,
        width: widthScreen >= 600 ?'28%' :'20%',
        height:widthScreen >= 600 ? 80: 60,
        borderRadius: 6,
        marginLeft: 5,
        marginRight: 5,
        marginBottom: 10,
        alignItems: 'center',
        justifyContent: 'center'
    } ,
    ScrollContainer:{width: '100%', flex: 1},
    containerScrollChildren :{
        flex: 1,
        flexDirection: 'row',
        flexWrap: 'wrap',
        alignItems: 'center',
        justifyContent: 'center',
      },
      Loading:{
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
      },
      TextTableName:{
        fontSize: widthScreen * 0.04,
        fontWeight:'700',
        color: '#333',
      }
})