import {StyleSheet} from 'react-native';
import {colors, widthScreen} from '../../../Utils/Styles';
import {verticalScale} from '../../../Utils/Reponsive';
export const ItemTableCss = StyleSheet.create({
  validTable: {
    borderColor: '#00FF00',
    borderWidth: widthScreen >= 600 ? 2 : 1,
    width: '32%',
    borderRadius: 8,
    paddingHorizontal: 8,
    marginRight: 4,
    marginBottom: 12,
  },
  ContainerValidOrder: {
    position: 'relative',
    backgroundColor: '#F9D066',
    borderColor: '#FBDC63',
    borderWidth: widthScreen >= 600 ? 2 : 1,
    marginBottom: 18,
    borderRadius: 16,
  },
  ContainerValid: {
    position: 'relative',
    backgroundColor: '#EBF5FF',
    borderColor: '#2C81D8',
    borderWidth: widthScreen >= 600 ? 2 : 1,
    marginBottom: 18,
    borderRadius: 16,
  },
  Container: {
    position: 'relative',
    backgroundColor: '#fff',
    borderColor: '#BBBBBB',
    borderWidth: widthScreen >= 600 ? 2 : 1,
    marginBottom: 18,
    borderRadius: 16,
  },
  bodyTableProductTextValid: {
    fontSize: widthScreen * 0.042,
    color: '#272A2E',
    fontWeight: '700',
  },
  bodyTableProductText: {
    fontSize: widthScreen * 0.042,
    color: '#272A2E',
    fontWeight: '700',
  },
  bodyTableProductTextOrder: {
    fontSize: widthScreen * 0.042,
    color: '#272A2E',
    fontWeight: '700',
  },
  containerView: {
    paddingHorizontal: 12,
    paddingVertical: 12,
    width: '100%',
    height: '100%',
  },
  containerCheck: {justifyContent: 'center', flex: 1},
  containerCheckChildren: {
    display: 'flex',
    alignItems: 'center',
    flexDirection: 'row',
  },
  TotalQtyProductTable: {
    color: colors.Textcolor,
    fontWeight: '700',
    fontSize: widthScreen * 0.042,
  },
  stylesIcon: {marginRight: 6},
  TotalProductTable: {
    flex:1,
    fontSize: widthScreen * 0.042,
    color: colors.Textcolor,
    fontWeight: '700',
  },
});
