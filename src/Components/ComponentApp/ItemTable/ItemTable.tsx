import {Text, TouchableOpacity, LogBox, View} from 'react-native';
import React, {useMemo} from 'react';
import {ItemTableCss} from './ItemTableCss';
import {useDispatch, useSelector} from 'react-redux';
import {
  SetCloseModalIsNotification,
  SetModalPosition,
  SetRemoveTableNotExist,
  SetShowLoading,
  SetShowModalIsNotification,
  SetShowTableDetail,
  SetStatusTable,
} from '../../../Redux/Slide';
import {CheckInTable} from '../../../Utils/GetApi';
import {ResponsiveMessage} from '../../../Utils/MessageApi';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import {widthScreen} from '../../../Utils/Styles';
import {RootState} from '../../../Redux/Storage';
import {AuthenticationError, xmlChangeText} from '../../../Utils/Helper';
import {ProductItem} from '../../../Utils/interface/interface';
import {AuthContext} from '../../../DefaultLayout/DefaultLayoutScreen';
import {Success} from '../../../Utils/Enums';
import {
  LoginAgain,
  TableIsUse,
  TableStandingNotExist,
  errorNotification,
} from '../../../Utils/MessageNotification';
import {RoundOff, TotalMoney, TotalQuantity} from '../../../Services/Services';
import {verticalScale} from '../../../Utils/Reponsive';
LogBox.ignoreLogs(['Warning: ...']);
LogBox.ignoreAllLogs();
const ItemTable = ({index, item, navigation}) => {
  const {signOut} = React.useContext(AuthContext);
  const UserName = useSelector((state: RootState) => state.counter.UserName);
  const XmlProperties = useSelector((state: RootState) => state.item.XmlProperties);
  const TabletIemXml = XmlProperties !== null ? XmlProperties[2] : null;
  const dispatch = useDispatch();
  const handleShowDetaiLOrder = item => {
    dispatch(SetCloseModalIsNotification());
    dispatch(SetShowLoading(true));
    CheckInTable({
      token: UserName.token,
      data: {
        id: item.id,
        user_id: UserName.id,
      },
    })
      .then(data => handleCheckIntableData(data))
      .catch(e => handleCatchErrorData(e))
      .finally(() => dispatch(SetShowLoading(false)));
  };
  const handleCheckIntableData = async data => {
    if (AuthenticationError(data.message, data.status_code)) {
      signOut(LoginAgain);
      return;
    }
    if (data.message == ResponsiveMessage.TableDontExist) {
      dispatch(SetRemoveTableNotExist(item.id));
      dispatch(
        SetShowModalIsNotification({
          description: TableStandingNotExist,
          showValue: true,
          functionConfirm: '',
          functionCancel: 'handleCloseModalNotification',
          valueBtnConfirm: '',
          valueBtnCancel: '',
        }),
      );
      return;
    }
    if (
      data.message == ResponsiveMessage.TableIsUse &&
      data.user_id == UserName.id
    ) {
      if (item.listitem.length <= 0) {
        dispatch(SetModalPosition('TableAddProduct'));
        navigation.navigate('ProductScreen', {
          nameOrder: 'TableBill',
          TakeOutBillID: undefined,
          TableID: item.id,
          tableName: item.tableName,
        });
      }
      if (item.listitem.length > 0) {
        dispatch(SetModalPosition('TableDetail'));
      }
      dispatch(
        SetShowTableDetail({
          dataDetailTable: {
            token: UserName.token,
            id: item.id,
            listitem: item.listitem,
            status: item.status,
            tableName: item.tableName,
            user_id: UserName.id,
            userordered: item.userordered,
            name_user: UserName.name,
          },
          dataListProductTable: item.listitem,
        }),
      );
      return;
    }
    if (
      data.message == ResponsiveMessage.TableIsUse &&
      data.user_id !== UserName.id
    ) {
      dispatch(
        SetShowModalIsNotification({
          description: TableIsUse,
          showValue: true,
          functionConfirm: '',
          functionCancel: 'handleCloseModalNotification',
          valueBtnConfirm: '',
          valueBtnCancel: '',
        }),
      );
      dispatch(SetStatusTable({index: index, status: 0}));
      return;
    }
    if (data.status_code == Success) {
      const ParseProduct =
        data.table.listitem !== null ? JSON.parse(data.table.listitem) : [];
      if (ParseProduct.length <= 0) {
        navigation.navigate('ProductScreen', {
          nameOrder: 'TableBill',
          TakeOutBillID: undefined,
          TableID: item.id,
          tableName: item.tableName,
        });
        dispatch(SetModalPosition('TableAddProduct'));
      }
      if (ParseProduct.length > 0) {
        dispatch(SetModalPosition('TableDetail'));
      }
      const mapData = {
        token: UserName.token,
        id: data.table.id,
        listitem: ParseProduct,
        status: data.table.status,
        tableName: data.table.tablename,
        user_id: UserName.id,
        userordered:
          data.table.userordered !== null
            ? JSON.parse(data.table.userordered)
            : [],
        name_user: UserName.name,
      };
      dispatch(
        SetShowTableDetail({
          dataDetailTable: mapData,
          dataListProductTable: ParseProduct,
        }),
      );
    } else {
      dispatch(
        SetShowModalIsNotification({
          description: data.message,
          showValue: true,
          functionConfirm: '',
          functionCancel: 'handleCloseModalNotification',
          valueBtnConfirm: '',
          valueBtnCancel: '',
        }),
      );
    }
  };
  const handleCatchErrorData = (e: any) => {
    dispatch(
      SetShowModalIsNotification({
        description: errorNotification,
        showValue: true,
        functionConfirm: '',
        functionCancel: 'handleCloseModalNotification',
        valueBtnConfirm: '',
        valueBtnCancel: '',
      }),
    );
  };
  const CheckPrint = item.listitem.find(
    (item: ProductItem) => item.status == 2,
  );
  const TotalTableItem = useMemo(() => {
    return item.listitem !== null &&
      item.listitem !== undefined &&
      item.listitem.length > 0
      ? TotalMoney(item.listitem)
      : 0;
  }, [item]);
  const TotalQty = useMemo(() => {
    return TotalQuantity(item.listitem);
  }, [item.listitem]);
  return (
    <TouchableOpacity
      onPress={() => handleShowDetaiLOrder(item)}
      style={[
        item.status == 0
          ? ItemTableCss.ContainerValidOrder
          : item.listitem.length > 0
          ? ItemTableCss.ContainerValid
          : ItemTableCss.Container,
        {
          width: xmlChangeText(TabletIemXml, 'ItemTable.Width', '48%'),
          height: verticalScale(
            xmlChangeText(TabletIemXml, 'ItemTable.Height', 160),
          ),
        },
      ]}>
      <View style={ItemTableCss.containerView}>
        <Text
          numberOfLines={1}
          style={
            item.status == 0
              ? ItemTableCss.bodyTableProductTextOrder
              : item.listitem.length > 0
              ? ItemTableCss.bodyTableProductTextValid
              : ItemTableCss.bodyTableProductText
          }>
          {item.status == 0
            ? `Bàn ${item.tableName} ${item.name_user} Đang Order`
            : `Bàn ${item.tableName}`}
        </Text>
        {item.listitem.length > 0 ? (
          <View style={ItemTableCss.containerCheck}>
            <View style={ItemTableCss.containerCheckChildren}>
              <MaterialIcons
                name={xmlChangeText(
                  TabletIemXml,
                  'IconMoney',
                  'monetization-on',
                )}
                size={widthScreen * 0.06}
                color={xmlChangeText(TabletIemXml, 'IconMoneyColor', '#FF9900')}
                style={ItemTableCss.stylesIcon}
              />
              {CheckPrint ? (
                <MaterialIcons
                  name={xmlChangeText(TabletIemXml, 'IconPrinter', 'print')}
                  size={widthScreen * 0.06}
                  color={xmlChangeText(
                    TabletIemXml,
                    'IconPrinterColor',
                    '#33CC33',
                  )}
                />
              ) : (
                ''
              )}
            </View>
          </View>
        ) : (
          ''
        )}
        {item.listitem !== null &&
        item.listitem !== undefined &&
        item.listitem.length > 0 ? (
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'space-between',
            }}>
            <Text numberOfLines={1} style={ItemTableCss.TotalProductTable}>
              {RoundOff(TotalTableItem)}
            </Text>
            <View style={{flexDirection: 'row', alignItems: 'center'}}>
              <MaterialIcons
                name={xmlChangeText(
                  TabletIemXml,
                  'IconReceipt',
                  'receipt-long',
                )}
                size={widthScreen * 0.05}
                color="#0066CC"
              />
              <Text numberOfLines={1} style={ItemTableCss.TotalQtyProductTable}>
                {TotalQty}
              </Text>
            </View>
          </View>
        ) : (
          ''
        )}
      </View>
    </TouchableOpacity>
  );
};
export default React.memo(ItemTable);
