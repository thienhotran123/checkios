import {ScrollView, View} from 'react-native';
import React from 'react';
import {CustomerModalCss} from './CustomerModalCss';
const CustomerModal = ({DataFilterItemCustomer}) => {
  return (
    <View style={CustomerModalCss.scrollCustomerContainer}>
      <ScrollView
        showsVerticalScrollIndicator={false}
        style={{width: '100%', height: '100%'}}
        nestedScrollEnabled={true}>
        <View style={{width: '100%'}}>
          {DataFilterItemCustomer.map((item) => item)}
        </View>
      </ScrollView>
    </View>
  );
};
export default React.memo(CustomerModal);
