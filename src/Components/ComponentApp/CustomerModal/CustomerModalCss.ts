import { StyleSheet } from 'react-native'
import { scale } from '../../../Utils/Reponsive'

export const CustomerModalCss = StyleSheet.create({
    scrollCustomerContainer: {
        position: 'absolute',
        width: '60%',
        paddingHorizontal: 10,
        height: scale(200),
        backgroundColor:'#333' ,
        zIndex:999,
        elevation:3, 
        borderRadius: 8,
        top:'104%',
      },
})