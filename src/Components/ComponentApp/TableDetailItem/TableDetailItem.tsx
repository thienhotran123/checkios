import {Image, Text, TouchableOpacity, View, LogBox} from 'react-native';
import React from 'react';
import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import {useDispatch, useSelector} from 'react-redux';
import {
  SetDataTableCheckIitem,
  SetItemTableQty,
  SetShowModalIsNotification,
} from '../../../Redux/Slide';
import {TableDetailItemCSS} from './TableDetailItemCSS';
import {colors, widthScreen} from '../../../Utils/Styles';
import {DeleteProductItem} from '../../../Utils/MessageNotification';
import {CommaSeparatedValues} from '../../../Services/Services';
import {RootState} from '../../../Redux/Storage';
import {scale} from '../../../Utils/Reponsive';
import {xmlChangeText} from '../../../Utils/Helper';
LogBox.ignoreLogs(['Warning: ...']);
LogBox.ignoreAllLogs();
const TableDetailItem = ({item, index}) => {
  const dispatch = useDispatch();
  const XmlProperties = useSelector(
    (state: RootState) => state.item.XmlProperties,
  );
  const TableDetailItemXml = XmlProperties !== null ? XmlProperties[2] : null;
  const handleInCreReduceItem = (value: string) => {
    dispatch(SetItemTableQty({value: value, index: index}));
    if (item.status !== 2 && item.quantity == 1 && value == 'reduce') {
      dispatch(
        SetShowModalIsNotification({
          description: DeleteProductItem,
          showValue: true,
          functionConfirm: 'handleDeleteItemListMenu',
          functionCancel: 'handleCloseModalNotification',
          valueBtnConfirm: '',
          valueBtnCancel: '',
        }),
      );
    }
  };
  const handelSetDataTableItemCheck = () => {
    dispatch(
      SetDataTableCheckIitem({
        ItemProductTable: item,
        IndexProductTable: index,
      }),
    );
  };
  return (
    <View
      style={
        item.status == 2
          ? TableDetailItemCSS.ItemCartOrderValid
          : TableDetailItemCSS.ItemCartOrder
      }>
      <Image
        source={{uri: item.image}}
        style={[
          TableDetailItemCSS.ItemCartOrderImg,
          {
            width: scale(
              xmlChangeText(
                TableDetailItemXml,
                'ItemTable.TableDetail.WidthImageProduct',
                60,
              ),
            ),
            height: scale(xmlChangeText(TableDetailItemXml, 'ItemTable.TableDetail.HeightImageProduct', 60)),
          },
        ]}
      />
      <View style={TableDetailItemCSS.InfoProduct}>
        <View style={TableDetailItemCSS.ItemCartOrderName}>
          <Text
            numberOfLines={2}
            style={TableDetailItemCSS.ItemCartOrderNameText}>
            {item.title}
          </Text>
        </View>
        <View style={TableDetailItemCSS.ItemCartOrderPrice}>
          <Text
            numberOfLines={1}
            style={TableDetailItemCSS.ItemCartOrderPriceText}>
            {CommaSeparatedValues(item.price)} VND
          </Text>
        </View>
      </View>
      <View style={TableDetailItemCSS.ItemCartOrderBtnUpAndDown}>
        <View style={TableDetailItemCSS.ItemCartOrderBtnUpAndDownMain}>
          <TouchableOpacity
            disabled={item.status == 2 && item.qtyStatus <= 0 ? true : false}
            onPress={() => handleInCreReduceItem('reduce')}
            style={TableDetailItemCSS.ItemCartOrderBtnDown}>
            <Ionicons
              name="remove-circle"
              size={widthScreen * 0.07}
              style={
                item.status == 2 && item.qtyStatus <= 0
                  ? TableDetailItemCSS.ItemCartOrderBtnDownTextPrintOld
                  : TableDetailItemCSS.ItemCartOrderBtnDownText
              }
            />
          </TouchableOpacity>
          <View style={TableDetailItemCSS.ItemCartOrderBtnTextQty}>
            <Text
              numberOfLines={1}
              style={TableDetailItemCSS.ItemCartOrderBtnText}>
              {item.quantity + item.qtyStatus}
            </Text>
          </View>
          <TouchableOpacity
            onPress={() => handleInCreReduceItem('increase')}
            style={TableDetailItemCSS.ItemCartOrderBtnUp}>
            <Ionicons
              name="add-circle"
              size={widthScreen * 0.07}
              style={TableDetailItemCSS.ItemCartOrderBtnUpText}
            />
          </TouchableOpacity>
        </View>
      </View>
      <TouchableOpacity
        onPress={handelSetDataTableItemCheck}
        style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
        {item.check == 1 ? (
          <MaterialIcons
            size={widthScreen * 0.055}
            name="check-box"
            style={{color: colors.IconColor}}
          />
        ) : (
          <MaterialIcons
            size={widthScreen * 0.055}
            style={{color: '#888888'}}
            name="check-box-outline-blank"
          />
        )}
      </TouchableOpacity>
    </View>
  );
};

export default React.memo(TableDetailItem);
