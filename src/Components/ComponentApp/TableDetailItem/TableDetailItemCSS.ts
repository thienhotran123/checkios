import {StyleSheet} from 'react-native';
import {colors, widthScreen} from '../../../Utils/Styles';
import {scale} from '../../../Utils/Reponsive';
export const TableDetailItemCSS = StyleSheet.create({
  ItemCartOrder: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 10,
    borderRadius: 4,
    paddingVertical: 10,
    backgroundColor: '#FFFFFF',
    borderBottomColor: '#EEEEEE',
    borderBottomWidth: 3,
  },
  ItemCartOrderValid: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 10,
    paddingVertical: 10,
    backgroundColor: 'rgba(60, 179, 113,0.4)',
    borderRadius: 4,
    borderBottomColor: '#EEEEEE',
    borderBottomWidth: 3,
  },
  ItemCartOrderImg: {
    borderRadius: 6,
  },
  ItemCartOrderName: {
    width: '100%',
  },
  ItemCartOrderNameText: {
    fontSize: widthScreen * 0.042,
    fontWeight: '700',
    color: '#0066CC',
  },
  ItemCartOrderPrice: {
    width: '100%',
  },
  ItemCartOrderPriceText: {
    fontSize: widthScreen * 0.034,
    fontWeight: '600',
    color: '#333',
  },
  ItemCartOrderBtnUpAndDown: {
    width: '22%',
  },
  ItemCartOrderBtnUpAndDownMain: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    width: '100%',
  },
  ItemCartOrderBtnUp: {},

  ItemCartOrderBtnUpText: {
    color: colors.IconColor,
    fontWeight: '600',
  },
  ItemCartOrderBtnTextQty: {
    fontSize: 18,
    alignItems: 'center',
    flex: 1,
  },
  ItemCartOrderBtnText: {
    fontSize: widthScreen * 0.04,
    color: '#333',
    fontWeight: '600',
  },
  ItemCartOrderBtnDown: {},
  ItemCartOrderBtnDownText: {
    color: colors.IconColor,
    fontWeight: '600',
  },
  ItemCartOrderBtnDownTextPrintOld: {
    color: colors.gray,
    fontWeight: '600',
  },
  InfoProduct: {
    paddingHorizontal:10 ,
    width:'48%'
  },
});
