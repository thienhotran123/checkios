import {ScrollView, Text, TouchableOpacity, View, LogBox} from 'react-native';
import React from 'react';
import {TableDetailItem} from '../../Components';
import {TableDetailMainCss} from './TableDetailCss';
import {useSelector, useDispatch} from 'react-redux';
import {
  SetChooseAllData,
  SetCloseDetailTable,
  SetCloseModalIsNotification,
  SetModalPosition,
  SetRemoveTableNotExist,
  SetShowLoading,
  SetShowMOdalListTable,
  SetShowModalIsNotification,
} from '../../../Redux/Slide';
import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import {useMemo} from 'react';
import {UpdateTable, CheckOutTable} from '../../../Utils/GetApi';
import {colors, widthScreen} from '../../../Utils/Styles';
import {ResponsiveMessage} from '../../../Utils/MessageApi';
import {RootState} from '../../../Redux/Storage';
import {AuthenticationError, xmlChangeText} from '../../../Utils/Helper';
import {ProductItem} from '../../../Utils/interface/interface';
import {AuthContext} from '../../../DefaultLayout/DefaultLayoutScreen';
import {Success} from '../../../Utils/Enums';
import {
  CreateKitchenVouchers,
  DeleteProductAll,
  LoginAgain,
  TableStandingNotExist,
  errorNotification,
} from '../../../Utils/MessageNotification';
import {
  RoundOff,
  TotalMoney,
  TotalQuantity,
  CheckProductItemPrinted,
} from '../../../Services/Services';
import {verticalScale} from '../../../Utils/Reponsive';
LogBox.ignoreLogs(['Warning: ...']);
LogBox.ignoreAllLogs();
const TableDetail = ({navigation}) => {
  const {signOut} = React.useContext(AuthContext);
  const TableItem = useSelector((state: RootState) => state.counter.TableItem);
  const UserName = useSelector((state: RootState) => state.counter.UserName);
  const XmlProperties = useSelector((state: RootState) => state.item.XmlProperties);
  const TableDetailXml = XmlProperties !== null ? XmlProperties[2] : null;
  const dispatch = useDispatch();
  const AllCheckData = useMemo(() => {
    return TableItem.listitem.every((item: ProductItem) => item.check == 1);
  }, [TableItem.listitem]);
  const handleShowDetailTable = async () => {
    dispatch(SetCloseModalIsNotification());
    dispatch(SetShowLoading(true));
    if (TableItem.listitem.length <= 0) {
      CheckOutTable({
        token: UserName.token,
        data: {id: TableItem.id, user_id: UserName.id},
      })
        .then(data => {
          if (AuthenticationError(data.message, data.status_code)) {
            signOut(LoginAgain);
            return;
          }
          if (data.message == ResponsiveMessage.TableDontExist) {
            dispatch(SetRemoveTableNotExist(TableItem.id));
            dispatch(SetModalPosition(''));
            dispatch(
              SetShowModalIsNotification({
                description: TableStandingNotExist,
                showValue: true,
                functionConfirm: '',
                functionCancel: 'handleCloseModalNotification',
                valueBtnConfirm: '',
                valueBtnCancel: '',
              }),
            );
            return;
          }
          if (data.status_code == Success) {
            dispatch(SetModalPosition(''));
            dispatch(SetCloseDetailTable());
          } else {
            dispatch(
              SetShowModalIsNotification({
                description: data.message,
                showValue: true,
                functionConfirm: '',
                functionCancel: 'handleCloseModalNotification',
                valueBtnConfirm: '',
                valueBtnCancel: '',
              }),
            );
          }
        })
        .catch(e => handleCatchTableCheckLogout(e))
        .finally(() => dispatch(SetShowLoading(false)));
      return;
    }
    if (TableItem.listitem.length > 0) {
      const indexUserOrderOld =
        TableItem.userordered !== undefined
          ? TableItem.userordered.findIndex(item => item.id == UserName.id)
          : -1;
      UpdateTable({
        token: UserName.token,
        data: {
          id: TableItem.id,
          listitem: JSON.stringify(TableItem.listitem),
          user_id: UserName.id,
          status: 1,
          userordered:
            indexUserOrderOld !== -1
              ? JSON.stringify(TableItem.userordered)
              : JSON.stringify(
                  TableItem.userordered !== undefined
                    ? [
                        ...TableItem.userordered,
                        {Name: UserName.name, id: UserName.id},
                      ]
                    : '[]',
                ),
        },
      })
        .then(data => {
          if (AuthenticationError(data.message, data.status_code)) {
            signOut(LoginAgain);
            return;
          }
          if (data.message == ResponsiveMessage.TableDontExist) {
            dispatch(SetRemoveTableNotExist(TableItem.id));
            dispatch(SetModalPosition(''));
            dispatch(
              SetShowModalIsNotification({
                description: TableStandingNotExist,
                showValue: true,
                functionConfirm: '',
                functionCancel: 'handleCloseModalNotification',
                valueBtnConfirm: '',
                valueBtnCancel: '',
              }),
            );
            return;
          }
          if (data.status_code == Success) {
            dispatch(SetModalPosition(''));
            dispatch(SetCloseDetailTable());
          } else {
            dispatch(
              SetShowModalIsNotification({
                description: data.message,
                showValue: true,
                functionConfirm: '',
                functionCancel: 'handleCloseModalNotification',
                valueBtnConfirm: '',
                valueBtnCancel: '',
              }),
            );
          }
        })
        .catch(e => handleCatchTableCheckLogout(e))
        .finally(() => dispatch(SetShowLoading(false)));
      return;
    }
  };
  const handleCatchTableCheckLogout = (e: any) => {
    dispatch(
      SetShowModalIsNotification({
        description: errorNotification,
        showValue: true,
        functionConfirm: '',
        functionCancel: 'handleCloseModalNotification',
        valueBtnConfirm: '',
        valueBtnCancel: '',
      }),
    );
  };
  const total = useMemo(() => {
    return TotalMoney(TableItem.listitem);
  }, [TableItem]);
  const handleClearDataTableItem = () => {
    const CheckStatus = TableItem.listitem.find(
      item => item.status == 2 && item.check == 1,
    );
    if (CheckStatus) {
      const listData = TableItem.listitem.filter(item => item.check == 1);
      const NotCheck = TableItem.listitem.filter(item => item.check !== 1);
      navigation.navigate('CancelInvoicePrintedOrder', {
        nameOption: 'Xoá Món',
        screenName: 'Bàn',
        valueName: 'deleteItemProductTable',
        data: listData,
        NotCheck: NotCheck,
      });
      return;
    }
    dispatch(
      SetShowModalIsNotification({
        description: DeleteProductAll,
        showValue: true,
        functionConfirm: 'handleClearDataTable',
        functionCancel: 'handleCloseModalNotification',
        valueBtnConfirm: '',
        valueBtnCancel: '',
      }),
    );
  };
  const handleCheckAllData = () => {
    if (TableItem.listitem.length > 0) {
      dispatch(SetChooseAllData({ChooseScreen: 'Table'}));
    }
  };
  const handleAddItemOrder = () => {
    dispatch(SetCloseModalIsNotification());
    navigation.navigate('ProductScreen', {
      nameOrder: 'TableBill',
      TakeOutBillID: undefined,
      TableID: TableItem.id,
      tableName: TableItem.tableName,
    });
  };
  const handlePaymentTable = () => {
    dispatch(SetCloseModalIsNotification());
    const ProductPaymentItem = TableItem.listitem.filter(item => item.check == 1);
    navigation.navigate('PaymentScreen', {
      item: ProductPaymentItem.length > 0 ? ProductPaymentItem : TableItem.listitem,
      CheckValue:  ProductPaymentItem.length == TableItem.listitem.length || ProductPaymentItem.length <= 0? 'All': 'NotAll',
    });
  };
  const handlePrinter = () => {
    dispatch(
      SetShowModalIsNotification({
        description: CreateKitchenVouchers,
        showValue: true,
        functionConfirm: 'HandlePrint',
        functionCancel: 'handleCloseModalNotification',
        valueBtnConfirm: '',
        valueBtnCancel: '',
      }),
    );
  };
  const quantityItem = useMemo(() => {
    return TotalQuantity(TableItem.listitem);
  }, [TableItem]);
  const handleMoveTable = () => {
    dispatch(SetShowMOdalListTable({show: true, NameModal: 'TableItemDetail'}));
  };
  const handleCancelTable = () => {
    dispatch(SetCloseModalIsNotification());
    navigation.navigate('CancelInvoicePrintedOrder', {
      nameOption: 'Huỷ Hoá Đơn',
      screenName: 'Hoá Đơn Bàn',
      valueName: 'CancelTable',
      data: TableItem.listitem,
      NotCheck: [],
    });
  };
  const CheckData = useMemo(() => {
    return TableItem.listitem.find((item: ProductItem) => item.check == 1);
  }, [TableItem]);
  const CheckItemPrinted = useMemo(() => {
    return CheckProductItemPrinted(TableItem.listitem);
  }, [TableItem.listitem]);
  return (
    <View style={TableDetailMainCss.centeredView}>
      <View style={{flex: 1}}>
        <View style={TableDetailMainCss.ModalHeader}>
          <TouchableOpacity
            onPress={handleShowDetailTable}
            style={TableDetailMainCss.ModalHeaderBtn}>
            <Ionicons
              name={xmlChangeText(
                TableDetailXml,
                'ItemTable.TableDetail.IconCloseDetail',
                'close-outline',
              )}
              size={widthScreen * 0.12}
              color={xmlChangeText(
                TableDetailXml,
                'ItemTable.TableDetail.ColorIconCloseDetail',
                '#0066CC',
              )}
            />
          </TouchableOpacity>
          <View style={TableDetailMainCss.headerRightContainer}>
            <Text
              style={TableDetailMainCss.ModalHeaderTextTb}
              numberOfLines={1}>
              {TableItem.tableName !== undefined
                ? `Bàn ${TableItem.tableName}`
                : 'Chi Tiết'}
            </Text>
            {TableItem.listitem.length > 0 ? (
              <TouchableOpacity
                onPress={handleCheckAllData}
                style={{marginRight: 16}}>
                {AllCheckData ? (
                  <MaterialIcons
                    size={widthScreen * 0.055}
                    name="check-box"
                    style={{color: colors.IconColor}}
                  />
                ) : (
                  <MaterialIcons
                    size={widthScreen * 0.055}
                    name="check-box-outline-blank"
                    style={{color: '#888888'}}
                  />
                )}
              </TouchableOpacity>
            ) : (
              ''
            )}
            <TouchableOpacity
              disabled={
                TableItem.listitem.length <= 0 || CheckData == undefined
                  ? true
                  : false
              }
              onPress={handleClearDataTableItem}
              style={
                TableItem.listitem.length <= 0 || CheckData == undefined
                  ? TableDetailMainCss.ModalHeaderBtnClearNotOrder
                  : TableDetailMainCss.ModalHeaderBtnClear
              }>
              <Text style={TableDetailMainCss.TextBtn}>Xoá</Text>
            </TouchableOpacity>
          </View>
        </View>
        <ScrollView
          style={TableDetailMainCss.ScrollContainer}
          showsVerticalScrollIndicator={false}>
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'space-between',
              paddingVertical: 4,
            }}>
            <View></View>
            {TableItem.listitem.length > 0 ? (
              <TouchableOpacity
                onPress={handlePrinter}
                style={{paddingHorizontal: 10}}>
                <Ionicons
                  name="print-outline"
                  size={widthScreen * 0.11}
                  color={colors.IconColor}
                />
              </TouchableOpacity>
            ) : (
              ''
            )}
          </View>
          <View>
            {TableItem.listitem.map((item, index) => (
              <TableDetailItem item={item} index={index} key={index} />
            ))}
          </View>
        </ScrollView>
      </View>
      {TableItem.listitem.length <= 0 ? (
        <TouchableOpacity
          onPress={handleAddItemOrder}
          style={{position: 'absolute', bottom: 12, right: 12}}>
          <MaterialIcons
            name={xmlChangeText(
              TableDetailXml,
              'ItemTable.TableDetail.IconAddProduct',
              'add-circle',
            )}
            size={widthScreen * 0.14}
            color={xmlChangeText(
              TableDetailXml,
              'ItemTable.TableDetail.ColorIconAddProduct',
              '#0066CC',
            )}
          />
        </TouchableOpacity>
      ) : (
        ''
      )}
      {TableItem.listitem.length > 0 ? (
        <View style={TableDetailMainCss.footer}>
          <View style={TableDetailMainCss.footerTotal}>
            <TouchableOpacity onPress={handleAddItemOrder}>
              <MaterialIcons
                name={xmlChangeText(
                  TableDetailXml,
                  'ItemTable.TableDetail.IconAddProduct',
                  'add-circle',
                )}
                size={widthScreen * 0.14}
                color={xmlChangeText(
                  TableDetailXml,
                  'ItemTable.TableDetail.ColorIconAddProduct',
                  '#0066CC',
                )}
              />
            </TouchableOpacity>
            <View style={TableDetailMainCss.ContainerFooterTotalPrice}>
              <View style={{flexDirection: 'row', alignItems: 'center'}}>
                <Text
                  numberOfLines={1}
                  style={[
                    TableDetailMainCss.footerTotalText,
                    {marginRight: 10},
                  ]}>
                  Tổng
                </Text>
                <Text
                  numberOfLines={1}
                  style={TableDetailMainCss.footerTotalText}>
                  {quantityItem}
                </Text>
              </View>
              <Text
                numberOfLines={1}
                style={TableDetailMainCss.footerTotalText}>
                {RoundOff(total)} VND
              </Text>
              <Text
                numberOfLines={1}
                style={TableDetailMainCss.footerTotalTextVat}>
                (Chưa bao gồm thuế)
              </Text>
            </View>
          </View>
          <View style={TableDetailMainCss.ContainerBtnFooter}>
            <View
              style={{
                width: xmlChangeText(
                  TableDetailXml,
                  'ItemTable.TableDetail.ButtonFooter.WidthBtn',
                  '32%',
                ),
              }}>
              <TouchableOpacity
                disabled={CheckItemPrinted ? false : true}
                onPress={handleCancelTable}
                style={[
                  CheckItemPrinted
                    ? TableDetailMainCss.btnCancelOrder
                    : TableDetailMainCss.btnCancelOrderNot,
                  {
                    height: verticalScale(
                      xmlChangeText(
                        TableDetailXml,
                        'ItemTable.TableDetail.ButtonFooter.HeightBtn',
                        50,
                      ),
                    ),
                  },
                ]}>
                <Text style={TableDetailMainCss.TextBtn}>Huỷ đơn</Text>
              </TouchableOpacity>
            </View>
            <View
              style={{
                width: xmlChangeText(
                  TableDetailXml,
                  'ItemTable.TableDetail.ButtonFooter.WidthBtn',
                  '32%',
                ),
              }}>
              <TouchableOpacity
                onPress={handleMoveTable}
                style={[
                  TableDetailMainCss.btnMoveTable,
                  {
                    height: verticalScale(
                      xmlChangeText(
                        TableDetailXml,
                        'ItemTable.TableDetail.ButtonFooter.HeightBtn',
                        50,
                      ),
                    ),
                  },
                ]}>
                <Text style={TableDetailMainCss.TextBtn}>Chuyển bàn</Text>
              </TouchableOpacity>
            </View>
            <View
              style={{
                width: xmlChangeText(
                  TableDetailXml,
                  'ItemTable.TableDetail.ButtonFooter.WidthBtn',
                  '32%',
                ),
              }}>
              <TouchableOpacity
                onPress={handlePaymentTable}
                style={[
                  TableDetailMainCss.btnPayment,
                  {
                    height: verticalScale(
                      xmlChangeText(
                        TableDetailXml,
                        'ItemTable.TableDetail.ButtonFooter.HeightBtn',
                        50,
                      ),
                    ),
                  },
                ]}>
                <Text style={TableDetailMainCss.TextBtn}>Thanh toán</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      ) : (
        ''
      )}
    </View>
  );
};

export default React.memo(TableDetail);
