const TokenExpired = 406
const TokenEmpty = 407
const Success = 200
const ServerError = 500
export {
    TokenExpired,
    TokenEmpty,
    Success ,
    ServerError
}