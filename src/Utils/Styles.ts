import {Appearance, Dimensions, Platform} from 'react-native';
const colors = {
  primaryColor: '#0066CC',
  btnColor:'#0066CC',
  Textcolor:'#0066CC',
  AlanColor:'#0066CC',
  IconColor:'#0066CC',
  btnColorPayment :'#0066CC',
  AlanFooterColor:'#0066CC',
  ColorUserName :'#444444',
  ColorIconOrDer:'#777777',
  secondaryColor: '#446c0c',
  white: '#ffffff',
  blue: 'blue',
  gray: 'gray',
  black: 'black',
  green: 'green',
  btnDisable: '#808080',
};
const styleShadow = {
  elevation: 1,
  shadowColor: '#171717',
  shadowOpacity: 0.2,
  shadowRadius: 3,
}
const widthScreen = Dimensions.get('screen').width;
const heightWindown = Dimensions.get('screen').height;
const Mode = Appearance.getColorScheme();
export {widthScreen, heightWindown, Mode, colors ,styleShadow};
