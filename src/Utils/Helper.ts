import {TokenExpired, TokenEmpty} from './Enums';
import {ResponsiveMessage} from './MessageApi';
import {ProductItem} from './interface/interface';
let RegexReason = /^\s*$/; // contains only whitespace
let RegexString = /^[—-]+$/; //contains only "--" characters
let RegexStringEqual = /^=*$/; // contains only "=" characters
const regexNumber = /(03|028|05|07|08|09|01[2|6|8|9])+([0-9]{8})\b/; // check for numbers
const regexString = /^\D+$/;
const RegexNotNumber = /\D/g //search not number


const ChangeString = (stringPayload: string, arrayData: ProductItem[]) => {
  if (stringPayload == '') {
    return arrayData;
  }
  const itemProduct = arrayData.filter(item => {
    let itemString = item.title;
    let StringActions = stringPayload;
    itemString = itemString.toLowerCase();
    itemString = itemString.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, 'a');
    itemString = itemString.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, 'e');
    itemString = itemString.replace(/ì|í|ị|ỉ|ĩ/g, 'i');
    itemString = itemString.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, 'o');
    itemString = itemString.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, 'u');
    itemString = itemString.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, 'y');
    itemString = itemString.replace(/đ/g, 'd');
    StringActions = StringActions.toLowerCase();
    StringActions = StringActions.replace(
      /à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g,
      'a',
    );
    StringActions = StringActions.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, 'e');
    StringActions = StringActions.replace(/ì|í|ị|ỉ|ĩ/g, 'i');
    StringActions = StringActions.replace(
      /ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g,
      'o',
    );
    StringActions = StringActions.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, 'u');
    StringActions = StringActions.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, 'y');
    StringActions = StringActions.replace(/đ/g, 'd');
    return itemString.toLowerCase().includes(StringActions.toLowerCase());
  });
  return itemProduct;
};
const removeVietnameseaccents = (str: string) => {
  str = str.toLowerCase();
  str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, 'a');
  str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, 'e');
  str = str.replace(/ì|í|ị|ỉ|ĩ/g, 'i');
  str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, 'o');
  str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, 'u');
  str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, 'y');
  str = str.replace(/đ/g, 'd');
  return str;
};
const getDay = (date: any) => {
  let day = date.getDate();
  if (day < 10) day = `0${day}`;
  return day;
};
const getMonth = (date: any) => {
  let Mounth = date.getMonth() + 1;
  if (Mounth < 10) Mounth = `0${Mounth}`;
  return Mounth;
};
const getYear = (date: any) => {
  return date.getFullYear();
};
const getMilliseconds = (date: any) => {
  return date.getMilliseconds();
};
const getMinutes = (date: any) => {
  return date.getMinutes();
};
const getHours = (date: any) => {
  return date.getHours();
};
const getDateTime = (date: any) => {
  return `${getDay(date)}-${getMonth(date)}-${date.getFullYear()} ${date.getHours()}:${getMinutes(date)}`;
};
const getTimeConverter = (Time: string) =>{
  const date = new Date(Time)
  return `${date.getFullYear()}/${getMonth(date)}/${getDay(date)}`
}
const getTimeClock = (Time:string)=>{
  const date = new Date(Time)
  return `${date.getHours()}:${date.getMinutes()}:${date.getSeconds()}`
}
const getYearReduce = (date: any, day: number) => {
  return date.getFullYear() - day;
};
const xmlChangeText = (LoginXmlParser: any, Properties: string,Text: string | number) => {
  if (LoginXmlParser !== null && LoginXmlParser !== undefined) {
    const ConvertArray = Properties.split('.');
    let Ojp = LoginXmlParser[ConvertArray[0]];
    for (let i = 1; i < ConvertArray.length; i++) {
      if (Ojp !== undefined) {
        Ojp = Ojp[ConvertArray[i]];
      }
    }
    if (Properties == 'FooterSupportNumberPhone') {
      return Ojp !== undefined ? JSON.parse(Ojp) : Text;
    }
    return Ojp !== undefined ? Ojp : Text;
  } else {
    return Text;
  }
};
const xmlChangeCss = ( LoginXmlParser: any, Properties: string, CssDefault: any,) => {
  if (LoginXmlParser !== null && LoginXmlParser !== undefined) {
    const ConvertArray = Properties.split('.');
    let Ojp = LoginXmlParser[ConvertArray[0]];
    for (let i = 1; i < ConvertArray.length; i++) {
      if (Ojp !== undefined) {
        Ojp = Ojp[i];
      }
    }
    return Ojp !== undefined ? Ojp : CssDefault;
  } else {
    return CssDefault;
  }
};
const AuthenticationError = (message: string, statusCode: number) => {
  if (
    message == ResponsiveMessage.UserHasDelete 
    || message == ResponsiveMessage.StaffDontExist 
    ||message == ResponsiveMessage.StaffDontExistDeleted 
    || statusCode == TokenEmpty 
    || statusCode == TokenExpired) {
    return true;
  } else {
    return false;
  }
};
export {
  ChangeString,
  RegexReason,
  RegexString,
  RegexStringEqual,
  regexNumber,
  regexString,
  RegexNotNumber,
  removeVietnameseaccents,
  getDay,
  getMonth,
  getYear,
  getMilliseconds,
  getMinutes,
  getHours,
  getDateTime,
  getYearReduce,
  xmlChangeText,
  xmlChangeCss,
  AuthenticationError,
  getTimeConverter ,
  getTimeClock
};
