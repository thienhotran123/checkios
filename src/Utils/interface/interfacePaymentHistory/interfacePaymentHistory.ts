import {ProductItem} from '../interfaceProduct/interfaceProduct'
interface DataPayment {
    item: ProductItem[];
    CheckValue:string; 
    Reason:string ,
    DiscountPayment : number ,
    valueNumber:{name: string, phone: string, point: string, valid: boolean}
  }
  interface DetailItemPaymentOld {
    item: ProductItem[];
    tableName: number | string;
    valuetotal: number;
    discountPayment:number
  }
  export{
    DataPayment,
    DetailItemPaymentOld
  }