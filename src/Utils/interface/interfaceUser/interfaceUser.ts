interface dataUser {
    address: string;
    addresscompany: string;
    admin_id: number;
    created_at: string;
    id: number;
    logo: string | null;
    name: string;
    storename: string | null;
    phone: string;
    status: number;
    token: string;
    updated_at: string;
  }
  interface userName {
    token?: string;
    name?: string;
    id?: number;
    phone?: number;
    address?: string;
    status?: number;
    pass?: string;
    storename?: string | null;
    logo?: string | null;
    addresscompany?: string | null;
    phonenumbercompany?: string ;
  }
  interface loginData {
    address: string;
    addresscompany: string;
    admin_id: number;
    created_at:string;
    id: number;
    logo: string | null;
    name:string;
    storename:string| null;
    phone: string;
    status: number;
    token:string;
    updated_at: string;
  }
export {dataUser ,userName ,loginData}