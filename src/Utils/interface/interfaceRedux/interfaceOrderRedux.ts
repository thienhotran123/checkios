interface ValueInitialStateOrder {
  showBtnActions: boolean;
  DataSetting: DataSetting;
  DataFooter: footerBill;
  XmlProperties: any;
  checkCallGetXml: boolean;
  ConnectNetwork: {show: boolean; connected: boolean};
}
interface DataSetting {
  PrintIp: string;
}
interface footerBill {
  content: string;
  image: null | string;
}
export {DataSetting, footerBill, ValueInitialStateOrder};
