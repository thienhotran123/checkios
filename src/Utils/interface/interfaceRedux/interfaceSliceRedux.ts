 import {userName ,Table  ,TableItem ,DataPayment  ,DetailItemPaymentOld,CategoryItem ,ProductItem} from '../interface'
 interface ValueInitialState {
    showTable: boolean;
    ShowTableDetail: boolean;
    ShowCreateModal:boolean;
    isNotification: boolean;
    ShowDetailPaymentHistory: boolean;
    ShowMOdalListTable: boolean;
    isLoading: boolean;
    renderTable: boolean;
    ShowOrderTamDetail:boolean,
    indexItem:   number;
    NameModal: string;
    dataDeleteItem: {indexDelete: number, indexParent: number},
    UserName: userName;
    Table: Table[];
    TableItem: TableItem;
    DataPayment: DataPayment;
    ModalPosition: string;
    DetailItemPaymentOld: DetailItemPaymentOld;
    productItem: ProductItem[];
    CategoryItem: CategoryItem[];
    initialValueModal:{description : string , showValue : boolean , functionConfirm : string ,functionCancel :string , valueBtnConfirm :string , valueBtnCancel :string}
  }
  export {ValueInitialState };
  