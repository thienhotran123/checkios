interface ValueOpiton {
    admin_id?: number;
    created_at?: string;
    id?: number;
    listitem?: string;
    status?: number;
    tablename?: string;
    updated_at?: string;
    user_id?: null | number;
    userordered?: string;
  }

export {
    ValueOpiton
}