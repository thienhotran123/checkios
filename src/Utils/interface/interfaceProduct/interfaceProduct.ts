interface ProductItem {
    vat: number;
    category_id: string;
    check: number;
    id: string;
    image: string | null;
    price: number;
    qtyStatus: number;
    quantity: number;
    status: number;
    title: string;
  }
  interface CategoryItem {
    admin_id: number;
    category_name: string;
    created_at: string;
    id: number;
    status: number;
    updated_at: string;
  }
  interface dataProduct {
    message: string;
    product: ProductItem[];
    status: boolean;
    status_code: number;
  }
export {ProductItem ,CategoryItem ,dataProduct}