import {ProductItem} from '../interfaceProduct/interfaceProduct'
interface Table {
    id?: number;
    listitem: ProductItem[];
    name_user?: string;
    status?: number;
    status_code?:number;
    tableName?: number | string;
    user_id?: number | string;
    userordered?: userOrderTable[];
  }

  interface userOrderTable {
    Name: string;
    id: number;
  }
  interface TableItem {
    id?: number;
    listitem: ProductItem[];
    name_user?: string;
    status?: number;
    tableName?: number | string;
    token?: string;
    user_id?: number;
    userordered?: userOrderTable[];
  }
  export {userOrderTable,Table ,TableItem}