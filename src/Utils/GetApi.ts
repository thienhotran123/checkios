const urLink = 'https://api-pos-test.vietvang.net/api/';
import axios from 'axios';
export const Login = async params => {
  const request = await axios({
    method: 'post',
    url: `${urLink}user/user_login`,
    timeout: 10000,
    headers: {'Content-Type': 'application/json'},
    data: params,
  });
  return request.data;
};
export const LogOut = async params => {
  const request = await axios({
    method: 'post',
    url: `${urLink}user/logout_user`,
    timeout: 10000,
    headers: {
      'Content-Type': 'application/json',
      "Authorization": `Bearer ${params.token}`,
    },
  });
  return request.data;
};
export const getProduct = async params => {
  const request = await axios({
    method: 'post',
    url: `${urLink}user/product/get_product_list`,
    timeout: 10000,
    headers: {
      'Content-Type': 'application/json',
      "Authorization": `Bearer ${params.token}`,
    },
  });
  return request.data;
};
export const getTable = async params => {
  const request = await axios({
    method: 'post',
    url: `${urLink}user/table/get_all_table_user`,
    timeout: 10000,
    headers: {
      'Content-Type': 'application/json',
      "Authorization": `Bearer ${params.token}`,
    },
  });
  return request.data;
};
export const CheckInTable = async params => {
  const request = await axios({
    method: 'post',
    url: `${urLink}user/table/check_in_table`,
    timeout: 10000,
    headers: {
      'Content-Type': 'application/json',
      "Authorization": `Bearer ${params.token}`,
    },
    data: params.data,
  });
  return request.data;
};
export const CheckOutTable = async params => {
  const request = await axios({
    method: 'post',
    url: `${urLink}user/table/check_out_table`,
    timeout: 10000,
    headers: {
      'Content-Type': 'application/json',
      "Authorization": `Bearer ${params.token}`,
    },
    data: params.data,
  });
  return request.data;
};
export const UpdateTable = async params => {
  const request = await axios({
    method: 'post',
    url: `${urLink}user/table/update_table`,
    timeout: 10000,
    headers: {
      'Content-Type': 'application/json',
      "Authorization": `Bearer ${params.token}`,
    },
    data: params.data,
  });
  return request.data;
};
export const CreatePayment = async params => {
  const request = await axios({
    method: 'post',
    url: `${urLink}user/payment/create_payment`,
    timeout: 10000,
    headers: {
      'Content-Type': 'application/json',
      "Authorization": `Bearer ${params.token}`,
    },
    data: params.data,
  });
  return request.data;
};
export const ChangeTable = async params => {
  const request = await axios({
    method: 'post',
    url: `${urLink}user/table/change_table`,
    timeout: 10000,
    headers: {
      'Content-Type': 'application/json',
      "Authorization": `Bearer ${params.token}`,
    },
    data: params.data,
  });
  return request.data;
};
export const GetCategory = async params => {
  const request = await axios({
    method: 'post',
    url: `${urLink}user/category/get_category`,
    timeout: 10000,
    headers: {
      'Content-Type': 'application/json',
      "Authorization": `Bearer ${params.token}`,
    },
  });
  return request.data;
};
export const CreateContact = async params => {
  const request = await axios({
    method: 'post',
    url: `${urLink}create_contact`,
    timeout: 10000,
    headers: {
      'Content-Type': 'application/json',
    },
    data: params,
  });
  return request.data;
};
export const GetFooterBill = async params => {
  const request = await axios({
    method: 'post',
    url: `${urLink}user/footer/get_footer`,
    timeout: 10000,
    headers: {
      'Content-Type': 'application/json',
      Authorization: `Bearer ${params.token}`,
    },
  });
  return request.data;
};
export const GetAllCustomer = async params => {
  const request = await axios({
    method: 'post',
    url: `${urLink}user/customer/get_all_customer`,
    timeout: 10000,
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${params.token}`,
    },
  });
  return request.data;
};
export const UpdateCustomerReward = async params => {
  const request = await axios({
    method: 'post',
    url: `${urLink}user/customer/update_customer_reward`,
    timeout: 10000,
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${params.token}`,
    },
    data: params.data,
  });
  return request.data;
};
export const CreateCustomerReward = async params => {
  const request = await axios({
    method: 'post',
    url: `${urLink}user/customer/create_customer`,
    timeout: 10000,
    headers: {
      'Content-Type': 'application/json',
    },
    data: params,
  });
  return request.data;
};
export const GetAllPaymentUser = async params => {
  const request = await axios({
    method: 'post',
    url: `${urLink}user/payment/get_all_payment_for_user`,
    timeout: 6000,
    headers: {
      'Content-Type': 'application/json',
      Authorization: `Bearer ${params.token}`,
    },
    data: params.data,
  });
  return request.data;
};
export const getXmlStaff = async () => {
  const request = await axios({
    method: 'get',
    url: `https://api-pos-test.vietvang.net/storage/staff_public_vi.xml`,
    timeout: 6000,
  });
  return request.data;
};
